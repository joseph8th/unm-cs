# Test SPQR config file.

ALGORITHM = other
FIRST_SEED = 120
INPUT_ORDER = random
INPUTS_PER_REMOVAL = 0
LOOPS = 5
N_MAX = 10000
N_MIN = 
N_MULTIPLIER = 150
OUTPUT_LABEL = # %a %o %r %l %s (%n)

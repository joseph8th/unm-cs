#include <time.h>
#include <sys/time.h>      /* for struct timeval, etc */
#include <sys/resource.h>  /* for getrusage */

#include "SPQR.h"

#define AUTHOR_NAME "Joseph Edwards"

/**
 * SPQR implementation
 * CS241 UNM Fall 2011
 * Author: Joseph Edwards <jee8th@unm.edu>
 */

typedef struct perround * Round;
typedef struct runSPQR * Run;

struct perround {
  int N;
  float usecs;
  float stdev;
  uintptr_t * data;
};

struct runSPQR {
  int cur_seed;
  Round * rounds;
  size_t num_rounds;
};

static Config c;

/*
 * Function declarations
 */
static char * getLabel(char * format);

static void printConfig(Config c) {
  printf("%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n%s\n", 
         c->algorithm, c->first_seed, c->input_order, c->inputs_per_removal, 
         c->loops, c->n_max, c->n_min, c->n_multiplier, c->output_label);
}

static void * myMalloc(size_t size) {
  void * v = (void *) malloc(size);
  if (v == NULL)
    die("Out of memory", 3);
  return v;
}

static Round * initRounds(size_t num) {
  int i;
  Round * rds = (Round *) myMalloc(num * sizeof(Round)); 
  for (i = 0; i < num; ++i) {
    rds[i] = (Round) myMalloc(sizeof(struct perround));
    rds[i]->N = (int) floor((double) c->n_min * pow(((double) c->n_multiplier / 100), i));
    rds[i]->usecs = 0.0;
    rds[i]->stdev = 0.0;
    rds[i]->data = NULL;
  }
  return rds;
}

static Run initOnce() {
  char * output_label = getLabel(c->output_label);
  Run run = (Run) myMalloc(sizeof(struct runSPQR));
  int num_rounds = (uintptr_t) floor( (log((double) c->n_max) - log((double) c->n_min)) / log(((double) c->n_multiplier) / 100) ) + 1;

  run->cur_seed = c->first_seed;
  run->rounds = initRounds(num_rounds);
  run->num_rounds = num_rounds;

  if (*output_label != '\0')
    fprintf(stdout, "%s\n", output_label);
  free(output_label);

  return run;
}

static uintptr_t * loadData(size_t cur_N) {
  uintptr_t i;
  uintptr_t * data = (uintptr_t *) myMalloc(cur_N * sizeof(uintptr_t));

  switch (c->input_order) {
  case ASCENDING:
    for (i = 0; i < cur_N; ++i)
      data[i] = i + 1;
    break;
  case DESCENDING:
    for (i = 0; i < cur_N; ++i)
      data[i] = cur_N - i;
    break;
  case ALTERNATING:
    for (i = 0; i < cur_N; i += 2) {
      data[i] = i/2 + 1; 
      if (i+1 < cur_N)
        data[i+1] = cur_N - i/2;
    }
    break;
  case RANDOM:
    for (i = 0; i < cur_N; ++i)
      data[i] = (rand() % cur_N) + 1;
    break;
  }
  return data;
}

static void initPerRun(Run run, int ix, size_t cur_N) {
  srand(run->cur_seed++);
  run->rounds[ix]->data = loadData(cur_N);
}

void sarqRound(Round round) {
  int i;
  uintptr_t n;
  int ipr = c->inputs_per_removal;
  Sarq * s = SarqMalloc(NULL);

  for (i = 0; i < round->N; ++i) {
    if (ipr > 0) {
      if (i % ipr == 0)
        n = (uintptr_t) SarqRemove(s);
    }
    SarqInsert(s, (void *) round->data[i]); 
  }
  for (i = 0; i < SarqSize(s); ++i) {
    n = (uintptr_t) SarqRemove(s);
  }
  SarqFree(s);
}

void saryRound(Round round) {
  int i, n;
  int ipr = c->inputs_per_removal;
  SAry s = SAryMalloc(round->N);

  for (i = 0; i < round->N; ++i) {
    if (ipr > 0) {
      if (i % ipr == 0)
        n = SAryRemove(s);
    }
    SAryInsert(s, (int) round->data[i]); 
  }
  for (i = 0; i < SArySize(s); ++i) {
    n = SAryRemove(s);
  }
  SAryFree(s);
}

static void finPerRun(Round round, uint32_t * usAry) {
  int i;
  float stdev = 0.0;
  free(round->data);
  round->data = NULL;
  round->usecs /= c->loops;
  for (i = 0; i < c->loops; ++i) {
    stdev += pow(usAry[i]-round->usecs, 2);
  }
  stdev /= c->loops;
  round->stdev = sqrt(stdev);
}

static Run runAll(Run r) {
  volatile uint32_t i, j;
  uint32_t * usecsAry = (uint32_t *) myMalloc(c->loops * sizeof(uint32_t));
  struct rusage startUsage, stopUsage;

  for (i = 0; i < r->num_rounds; ++i) {
    initPerRun(r, i, r->rounds[i]->N);

    for (j = 0; j < c->loops; ++j) {

      if (getrusage(RUSAGE_SELF, &startUsage))
        exit(1);

      switch (c->algorithm) {
      case SARQ:
        sarqRound(r->rounds[i]);
        break;
      case OTHER:
      case ARRAY:
        saryRound(r->rounds[i]);
        break;
      default:
        break;
      }
      
      if (getrusage(RUSAGE_SELF, &stopUsage))
        exit(1);
      
      { /* Cut 'n pasted - from Prof. Ackley's supplied file */
        uint32_t secs = stopUsage.ru_utime.tv_sec - startUsage.ru_utime.tv_sec;
        uint32_t usecs = stopUsage.ru_utime.tv_usec - startUsage.ru_utime.tv_usec;
        usecs += 1000000*secs;
        r->rounds[i]->usecs += usecs;
        usecsAry[j] = usecs;
      }
    }
    finPerRun(r->rounds[i], usecsAry); 
  }
  free(usecsAry);
  return r;
}

static void freeRun(Run r) {
  int i;
  for (i = 0; i < r->num_rounds; ++i) {
    free(r->rounds[i]);
    r->rounds[i] = NULL;
  }
  free(r->rounds);
  free(r);
}

static void printRun(Run r) {
  int i;
  for (i = 0; i < r->num_rounds; ++i) {
    fprintf(stdout, "%d\t%f\t%f\n", r->rounds[i]->N, r->rounds[i]->usecs, r->rounds[i]->stdev);
  }
}

/**
 * SPQR benchmarking implementation
 */
int main() {

  Run run;
  
  c = configSPQR(stdin);
  if (c->errors) {
    freeSPQR(c);
    die("Sorry! Problem(s) with your configuration file.", 1);
  }

  /*  printConfig(c); 
      printf("\n"); */

  run = runAll(initOnce(c));
  printRun(run);

  freeRun(run);
  freeSPQR(c);

  return 0;
}

static char * getLabel(char * format) {
  int i = 0, j, k = 0;
  char ch1, ch2;
  size_t l = strlen(format);
  char * tmp = mallocZStr();
  char * label = mallocZStr();

  while (k < l) {
    ch1 = format[k++];

    if (ch1 != '%') {
      label[i++] = ch1;
    }
    else {
      ch2 = format[k++];

      switch (ch2) {
      case '%': 
        label[i++] = ch2;
        break;

      case 'a':
        switch (c->algorithm) {
        case SARQ:
          strcpy(tmp, "sarq");
          break;
        case OTHER:
        case ARRAY:
          strcpy(tmp, "array");
          break;
        }
        for (j = 0; j < strlen(tmp); ++j)
          label[i+j] = tmp[j];
        i += j;
        break;

      case 'l':
        sprintf(tmp, "%d", c->loops);
        for (j = 0; j < strlen(tmp); ++j)
          label[i+j] = tmp[j];
        i += j;
        break;

      case 'n':
        strcpy(tmp, AUTHOR_NAME);
        for (j = 0; j < strlen(tmp); ++j)
          label[i+j] = tmp[j];
        i += j;
        break;

      case 'o':
        switch (c->input_order) {
        case ASCENDING:
          strcpy(tmp, "ascending");
          break;
        case DESCENDING:
          strcpy(tmp, "descending");
          break;
        case ALTERNATING:
          strcpy(tmp, "alternating");
          break;
        case RANDOM:
          strcpy(tmp, "random");
          break;
        }
        for (j = 0; j < strlen(tmp); ++j)
          label[i+j] = tmp[j];
        i += j;
        break;

      case 'r':
        sprintf(tmp, "%d", c->inputs_per_removal);
        for (j = 0; j < strlen(tmp); ++j)
          label[i+j] = tmp[j];
        i += j;
        break;

      case 's':
        sprintf(tmp, "%d", c->first_seed);
        for (j = 0; j < strlen(tmp); ++j)
          label[i+j] = tmp[j];
        i += j;
        break;

      case '\0':
        label[i++] = ch1;
        break;

      default:
        label[i++] = ch1;
        label[i++] = ch2;
        break;
      }
    }
  }
  label[i] = '\0';
  free(tmp);
  return label;
}

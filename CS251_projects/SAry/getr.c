#include <stdlib.h>        /* for exit */
#include <stdio.h>         /* for printf */
#include <inttypes.h>      /* for uint32_t */
#include <sys/time.h>      /* for struct timeval, etc */
#include <sys/resource.h>  /* for getrusage */

int main() {
  volatile uint32_t i;
  struct rusage startUsage, stopUsage;
  if (getrusage(RUSAGE_SELF, &startUsage))  /* get initial usage */
    exit(1);                                 /* Couldn't?? */

  for (i = 0; i < 1000000000; ) { ++i; }    /* Count to a billion */

  if (getrusage(RUSAGE_SELF, &stopUsage))  /* get final usage */
    exit(1);                                /* Couldn't?? */

  {
    uint32_t secs = stopUsage.ru_utime.tv_sec-startUsage.ru_utime.tv_sec;
    uint32_t usecs = stopUsage.ru_utime.tv_usec-startUsage.ru_utime.tv_usec;
    usecs += 1000000*secs;
    printf("runtime %d usec\n",usecs);
  }
  return 0;
}

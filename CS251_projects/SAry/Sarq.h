#ifndef SARQ_H
#define SARQ_H

/*
 * Sarq.h - Self-Adjusting Randomized Queue
 *
 * A simple priority queue algorithm with decent amortized complexity,
 * based on the self-adjusting heaps of Sleator and Tarjan, with
 * randomization by Littman.
 *
 * David Ackley for UNM CS241 Fall 2011
 *
 * IMPORTANT NOTES:                                                    
 *                                                           
 * (0) This file must be used AS-IS, UNALTERED in any way.  Your code
 * is placed in Sarq.c, which will be compiled and tested against an
 * unaltered Sarq.h; if your Sarq.c doesn't work correctly under those
 * conditions, IT DOESN'T WORK.
 *                                                           
 * (1) See the Sarq(3) man page for complete documentation!  Your Sarq.c
 * implementation MUST be in full accordance with the man page
 * documentation, NOT MERELY the brief comments in this header!
 *
 * (2) Your code's not clean until it's valgrind clean!
 */

#include <stdio.h>  /* for FILE */

/* The exact definition of 'struct sarq' is implementation-specific --
 * up to you, and defined by you in your Sarq.c.  
 *
 * (Note that unlike other libraries such as 'Xstr.h', in this case
 * the Sarq typedef is an alias for the 'struct sarq' itself, NOT for
 * a pointer to that struct.)
 */
typedef struct sarq Sarq;

/** 
 * Function pointers used by SarqType:
 *
 * To enhance flexibility, the Sarq routines manipulate the priority
 * queue 'items' using void *'s rather than any particular fixed type.
 * However, the Sarq routines need to perform three operations on
 * items: Comparison, printing, and freeing.  To do this, function
 * pointer types are defined for the necessary operations...
 */
typedef int (*SarqCompareFunction)(void * left, void * right) ;
typedef void (*SarqPrintFunction)(FILE * stream, void * item) ;
typedef void (*SarqFreeFunction)(void * item) ;

/**
 * ...and a 'SarqType' struct is declared to provide a set of function
 * pointers to a SarqMalloc call:
 */
typedef struct sarqtype {
  SarqCompareFunction compare;
  SarqPrintFunction print;
  SarqFreeFunction free;
} SarqType;

/**
 * Create a new empty Sarq designed to hold items that are operated on
 * using the function pointers in the given 'info'.  If 'info' is
 * NULL, a default set of operations is assumed (see the man page).
 * 
 * The SarqType pointed to by info remains the property of the caller
 * of SarqMalloc; the Sarq library does _not_ take ownership of the
 * SarqType.
 */
extern Sarq * SarqMalloc(SarqType * info) ;

/**
 * Deallocate an existing Sarq and reclaim all storage associated with
 * it, including any items still present in the queue, which will be
 * freed using calls to the appropriate SarqFreeFunction. 
 */
extern void SarqFree(Sarq * s) ;

/**
 * Add 'item' to Sarq 's' using the 'self-adjusting randomized queue'
 * algorithm, consequently increasing SarqSize(s) by one.  
 */
extern void SarqInsert(Sarq * s, void * item) ;

/**
 * If SarqSize(s)==0, return 0.  Otherwise, remove and return the
 * highest priority item currently stored in 's', consequently
 * decreasing SarqSize(s) by one.
 */
extern void * SarqRemove(Sarq * s) ;

/**
 * Print a textual representation of 's' to the given 'stream'.  See
 * the man page.
 */
extern void SarqPrint(FILE * stream, Sarq * s) ;

/**
 * Return the current number of items stored in 's'.
 */
extern size_t SarqSize(Sarq * s) ;

/**
 * Return the current depth of 's' -- the number of items on 
 * any longest path from the root to any leaf.
 */
extern size_t SarqDepth(Sarq * s) ;

#endif /* SARQ_H */

#ifndef CONFIGSPQR_H
#define CONFIGSPQR_H

typedef struct cmapnode * CMapNode;
typedef struct configmap * CMap;
typedef struct spqrConfig * Config;

enum algrthm_t { SARQ, ARRAY, OTHER };
enum inorder_t { ASCENDING, DESCENDING, ALTERNATING, RANDOM };

/* Defines a config struct for SPQR */ 
struct spqrConfig {
  enum algrthm_t algorithm;
  int first_seed;
  enum inorder_t input_order;
  int inputs_per_removal;
  int loops;
  int n_max;
  int n_min;
  int n_multiplier;
  char * output_label;
  int errors;
};

extern char * mallocZStr();
extern Config configSPQR(FILE * file);
extern void freeSPQR(Config c);
extern void die(char * msg, int err);

#endif /* CONFIGSPQR_H */

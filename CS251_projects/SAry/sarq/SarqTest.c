#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <time.h>
#include "SarqTest.h"

#define N 10000
#define N_z N/100

static int compareFunc(void * left, void * right) { /* matches SarqCompareFunction */
  return strcmp((char*) left, (char*) right);
}
static void printFunc(FILE * stream, void * item) { /* matches SarqPrintFunction */
  fprintf(stream, "%s", (char *) item);
}
static void freeFunc(void * item) { /* ignore */ } /* matches SarqFreeFunction */

static void seedRand() {
  srand(time(0));
}

static int getRandLen(int M) {
  return (int) rand() % M;
}

int main(void) {
  uintptr_t i, j, q;
  char * zstr;
  char * zary[N_z];

  Sarq * s1 = SarqMalloc(NULL);    /* Using default SarqType */
  SarqType sarqInfo = { compareFunc, printFunc, freeFunc };
  Sarq * s2 = SarqMalloc(&sarqInfo);

  /* s1 default SarqType tests */
  for (i = 0; i < N; i += 10) {
    SarqInsert(s1, (void *) i);      /* insert N times */
  }
  printf("\n=========================\nsize: %d\n", (int) SarqSize(s1));
  SarqPrint(stdout, s1);
  for (i = 0; i < N/5; i += 10) {        /* remove N/3 items */
    q = (uintptr_t) SarqRemove(s1);
  }
  printf("\n=========================\nsize: %d\n", (int) SarqSize(s1));
  printf("depth: %d\n", (int) SarqDepth(s1));
  SarqPrint(stdout, s1);
  printf("\n=========================\n");

  seedRand();
  for (i = 0; i < N_z; ++i) {
    q = getRandLen(31) + 1;
    zstr = malloc(32*sizeof(char));
    for (j = 0; j < q; ++j)
      zstr[j] = 'A' + getRandLen(26);
    zstr[q] = '\0';
    zary[i] = zstr;
    SarqInsert(s2, (void*) zstr);
  }
  printf("\n=========================\nsize: %d\n", (int) SarqSize(s2));
  printf("depth: %d\n", (int) SarqDepth(s2));
  SarqPrint(stdout, s2);
  printf("\n");

  for (i = 0; i < N_z/3; ++i) {
    printf("/%s/ : ", (char *) SarqRemove(s2));
  }
  printf("\n=========================\nsize: %d\n", (int) SarqSize(s2));
  printf("depth: %d\n", (int) SarqDepth(s2));

  for (i = 0; i < N_z; ++i)
    free(zary[i]);
  
  printf("\n");

  SarqFree(s1);
  SarqFree(s2);

  return 0;

}

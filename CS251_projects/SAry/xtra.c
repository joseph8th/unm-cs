
static char * nextOption(char * line) {
  int n;
  char * opt, * bar;
  if (line != NULL)
    bar = strchr(line, '|');
  if (bar != NULL) {
    n = bar - line;
    opt = mallocZStr();
    strncpy(opt, line, n);
    line = line + (++n);
    return opt;
  }
}

static void loadOptions() {
  int i;
  char * line;
  size_t zarySize;
  char * val;
  FILE * file = fopen(DEFAULT_OPTIONS_FILENAME, "r");
  if (file == NULL)
    die("Unable to read default options.", 1);

  line = mallocZStr();
  while (fgets(line, 257, file)) {
    val = nextOption(line);
    zarySize = (size_t) atoi(val);
    *valAry = mallocZAry(zarySize);
    *valAry[0] = val;
    for (i = 1; i < zarySize; ++i) {
      strcpy(*valAry[i], nextOption(line));
    } 
    ++(*valAry);
  }
  fclose(file);
  free(line);
}

static void freeOptions() {
  int i, j, l;
  for (i = 0; i < ARYLEN; ++i) {
    l = *valAry[i][0];
    for (j = 0; j < l; ++j)
      free(valAry[i][j]);
    free(valAry[i]);
  }
}


static int addOption(CMapNode cnode) {
  int i;
  for (i = 0; i < ARYLEN; ++i)
    if (!strcmp(configAry[i].keyword, cnode->keyword)) {
      strcpy(configAry[i].value, cnode->value);
      return 1;
    }
  return 0;
}


static char ** mallocZAry(size_t size) {
  int i;
  char ** zary = (char **) malloc(size * sizeof(char *));
  if (zary == NULL)
    die("Out of memory.", 1);
  for (i = 0; i < size; ++i)
    zary[i] = mallocZStr();
  return zary;
}


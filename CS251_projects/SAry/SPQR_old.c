#include "SPQR.h"

/**
 * SPQR implementation
 * CS241 UNM Fall 2011
 * Author: Joseph Edwards <jee8th@unm.edu>
 */

#define ARYLEN 9
#define DEFAULT_OPTIONS_FILENAME "./defaultconfig.txt"

struct configmap {
  CMapNode root;
  size_t size;
};

struct cmapnode {
  char * keyword;
  char * value;
  struct cmapnode * next;
};

static struct spqrConfig {
  char * algorithm;
  int first_seed;
  char * input_order;
  int inputs_per_removal;
  int loops;
  int n_max;
  int n_min;
  int n_multiplier;
  char * output_label;
} SPQRConfig;

static struct {
  char * keyword;
  char * value;
} config[] = {
  { "ALGORITHM", NULL },
  { "FIRST_SEED", NULL },
  { "INPUT_ORDER", NULL },
  { "INPUTS_PER_REMOVAL", NULL },
  { "LOOPS", NULL },
  { "N_MAX", NULL },
  { "N_MIN", NULL },
  { "N_MULTIPLIER", NULL },
  { "OUTPUT_LABEL", NULL }
};

static CMap defCMap;

/*
static char * kwAry[] =
  { "ALGORITHM", "FIRST_SEED", "INPUT_ORDER", "INPUTS_PER_REMOVAL", 
    "LOOPS", "N_MAX", "N_MIN", "N_MULTIPLIER", "OUTPUT_LABEL" };

    static char * valAry[ARYLEN]; */

/**
vv * Error handling functions
 */
static int configError = 0;

static void die(char * msg, int err) {
  fprintf(stderr, "Error: %s\n", msg);
  exit(err);
}

static void printErr(char * msg, char * txt, int err) {
  fprintf(stderr, "Error: %s[%s]\n", msg, txt);
  ++configError;
}

/**
 * Config file handling functions
 */
static char * mallocZStr() {
   char * zstr = (char *) malloc(sizeof(char)*257);
   if (zstr == NULL)
     die("Out of memory.", 1);
   *zstr = '\0';
   return zstr;
}

static char ** mallocZAry(size_t size) {
  int i;
  char ** zary = (char **) malloc(size * sizeof(char *));
  if (zary == NULL)
    die("Out of memory.", 1);
  for (i = 0; i < size; ++i)
    zary[i] = mallocZStr();
  return zary;
}

static CMapNode mallocCNode() {
  CMapNode cnode = (CMapNode) malloc(sizeof(struct cmapnode));
  if (cnode == NULL)
    die("Out of memory.", 1);
  cnode->next = NULL;
  cnode->keyword = NULL;
  cnode->value = NULL;
  return cnode;
}

static CMap mallocCMap() {
  CMap cmap = (CMap) malloc(sizeof(struct configmap));
  if (cmap == NULL)
    die("Out of memory.", 1);
  return cmap;
}

static int getKeyPair(char * line) {
  int eq = 0;
  char ch;
  while ((ch = line[eq]) != '\0') {
    if (ch == '=') break;
    ++eq;
  }
  return eq;
}

static int isComment(char ch) {
  switch (ch) {
  case '!':
  case '#':
  case ';':
  case '\0':
    return 1;
  default:
    return 0;
  }
}

static char * rmLeadSpace(char * zstr) {
  char * tmp;
  /*  if (*zstr == '\0') return zstr; */
  while (isspace(*zstr)) {
    tmp = ++zstr;
    zstr = rmLeadSpace(tmp);
  }
  return zstr;
}

static char * rmTailSpace(char * zstr, int end) {
  /*  if (*zstr == '\0') return zstr; */
  if (end) {
    while (isspace(zstr[end])) {
      zstr[end] = '\0';
      zstr = rmTailSpace(zstr, end);
      --end;
    }
  }
  return zstr;
}

static char * rmSpace(char * zstr) {
  char * tmp;
  int end;
  if (*zstr == '\0') return zstr;
  end = strlen(zstr) - 1;
  tmp = rmLeadSpace(rmTailSpace(zstr, end));
  memmove(zstr, tmp, strlen(tmp)+1);
  return zstr;
}

static int rmDQuotes(char * val) {
  char * tmp;
  int end = strlen(val) - 1;
  if (*val == '\0') return 1;
  if ((val[0] == '"') && (val[end] != '"'))
    return 0;
  if ((val[0] != '"') && (val[end] == '"'))
    return 0;
  if (val[0] == '"') {
    tmp = mallocZStr();
    val[end] = '\0';
    strcpy(tmp, &val[1]);
    strcpy(val, tmp);
    free(tmp);
  }
  return 1;
}

static int isKWChar(char ch) {
  if ((ch >= '0') && (ch <= '_')) { /*T*/
    if ((ch > '9') && (ch < 'A')) return 0; /*F*/
    if ((ch > 'Z') && (ch != '_')) return 0; /*F*/
    return 1;
  }
  return 0;
}

static int kwParse(char * kw) {
  kw = rmSpace(kw);
  while (*kw != '\0') {
    if (!isKWChar(*kw)) return 0;
    ++kw;
  }
  return 1;
}

static char * getKeyword(char * line, int eq) {
  char * kw = mallocZStr();
  strncpy(kw, line, eq);
  kw[eq] = '\0';
  if (!kwParse(kw))
    printErr("Invalid parameter keyword in line: ", line, 1);
  return kw;
}

static int valParse(char * val) {
  return rmDQuotes(rmSpace(val));
}

static char * getValue(char * line, int eq) {
  char * tmp;
  char * val = mallocZStr();
  if (eq < 254) {
    tmp  = &line[eq + 1];
    strcpy(val, tmp);
    if (!valParse(val))
      printErr("Invalid parameter value in line: ", line, 2);
  }
  return val;
}

static CMapNode nextCNode(char * line) {
  CMapNode cnode;
  int eqSign = getKeyPair(line);
  if (!eqSign) printErr("Keyword required in line: ", line, 4);
  if (eqSign < strlen(line)) {
    cnode = mallocCNode();
    cnode->keyword = getKeyword(line, eqSign);
    cnode->value = getValue(line, eqSign);
    return cnode;
  }
  return NULL;
}

static void pushCNode(CMapNode cnode, CMap cmap) {
  cnode->next = cmap->root;
  cmap->root = cnode;
  ++(cmap->size);
}

static CMapNode popCNode(CMap cmap) {
  CMapNode cnode = cmap->root;
  if (cnode != NULL) {
    cmap->root = cnode->next;
    --(cmap->size);
    cnode->next = NULL;
  }
  return cnode;
}

static void freeCNode(CMapNode cnode) {
    free(cnode->keyword);
    cnode->keyword = NULL;
    free(cnode->value);
    cnode->value = NULL;
    free(cnode);
    cnode = NULL;
}

static void freeCMap(CMap cmap) {
  while (cmap->size) {
    freeCNode(popCNode(cmap));
  }
  free(cmap);
  cmap = NULL;
}

static CMap getCMap(FILE * file) {
  CMapNode cnode;
  CMap cmap = mallocCMap();
  char * line = mallocZStr(); 
  cmap->size = 0;

  while (fgets(line, 257, stdin)) {
    if (strlen(line) > 255) {
      freeCMap(cmap);
      free(line);
      line = NULL;
      die("Parameter line exceeds maximum line length", 3);
    }

    line = rmSpace(line);

    if ((!isComment(line[0])) && (line[0] != '\0')) {
      if ((cnode = nextCNode(line)) != NULL)
        pushCNode(cnode, cmap);
    }
  }
  fclose(file);
  free(line);
  line = NULL;
  return cmap;
}

static char * searchCMap(CMapNode cnode, char * keyword) {
  char * val = NULL;
  if (cnode == NULL) return val;

  if (!strcmp(cnode->keyword, keyword)) {
    val = mallocZStr();
    strcpy(val, cnode->value);
    return val;
  }
  return searchCMap(cnode->next, keyword);
}

static char * getCValue(CMap cmap, char * keyword) {
  char * val;
  if (defCMap == NULL) return NULL;        /* Must load default config 1st */
  if ((val = searchCMap(cmap->root, keyword)) == NULL) 
    val = searchCMap(defCMap->root, keyword);
  return val;
}

static int setConfigValue(int ix, char * val) {
  int n;

  switch (ix) {
  case 0:
    if (!strcmp(val, "sarq") || !strcmp(val, "array") || !strcmp(val, "other")) {
      SPQRConfig.algorithm = val;
      return 1;
    }
    return 0;
  case 1:
    if ((n = atoi(val)) > -1) {
      SPQRConfig.first_seed = n;
      return 1;
    }
    return 0;
  case 2:
    if (!strcmp(val, "ascending") || !strcmp(val, "descending") || !strcmp(val, "alternating") || !strcmp(val, "random")) {
      SPQRConfig.input_order = val;
      return 1;
    }
    return 0;
  case 3:
    if ((n = atoi(val)) > -1) {
      SPQRConfig.inputs_per_removal = n;
      return 1;
    }
    return 0;
  case 4:
    if ((n = atoi(val)) > -1) {
      SPQRConfig.loops = n;
      return 1;
    }
    return 0;
  case 5:
    if ((n = atoi(val)) > -1) {
      SPQRConfig.n_max = n;
      return 1;
    }
    return 0;
  case 6:
    if ((n = atoi(val)) > -1) {
      SPQRConfig.n_min = n;
      return 1;
    }
    return 0;
  case 7:
    if ((n = atoi(val)) > -1) {
      SPQRConfig.n_multiplier = n;
      return 1;
    }
    return 0;
  case 8:
    if (*val == '#') {
      SPQRConfig.output_label = val;
      return 1;
    }
    return 0;

  default:
    return 0;
  }
}

static void initConfig(CMap cmap) {
  int i = 0;
  FILE * file = fopen(DEFAULT_OPTIONS_FILENAME, "r");

  defCMap = getCMap(file);                     /* Load default config */

  for (i = 0; i < ARYLEN; ++i) {
    config[i].value = getCValue(cmap, config[i].keyword);    /* get config values */

    printf("%s=%s\n", config[i].keyword, config[i].value);

    if (!setConfigValue(i, config[i].value))                    /* check & set configuration */
      printErr("Invalid parameter value: ", "initConfig()", 1);    /* ... or log an error */
  }
  freeCMap(defCMap);
  freeCMap(cmap);
}

static int configSPQR(CMap cmap) {
  initConfig(cmap);
  if (configError) return 0;  
  return 1;
}

static void freeSPQR() {
  int i;
  for (i = 0; i < ARYLEN; ++i)
    free(config[i].value);
}

/**
 * Benchmark SARQ implementation
 */
int main() {

  if (!configSPQR(getCMap(stdin))) {
    printErr("Problem with your configuration file: ", "main()", 1);
    freeSPQR();
    return 1;
  }


  freeSPQR();
  return 0;
}

#include <stdio.h>
#include <inttypes.h>   /* for uintptr_t */
#include <stdlib.h>     /* for srand(), rand() */
#include <time.h>

#include "Sarq.h"       /* SARQ_H header */

/** 
 * SARQ (Self-Adjusting Randomized Queue) library implementation
 * By Joseph Edwards <jee8th@unm.edu>
 * CS241 F11 UNM - Program 3
 */

#define HEADS coinToss()   /* returns boolean 1 == Heads, 0 == Tails */

/**
 * Sarq - struct sarq type declared in Sarq.h
 * SarqNode - ptr-to-struct sarqNode type
 */
typedef struct sarqNode * SarqNode;

/* struct sarq type for the Sarq itself*/
struct sarq {
  SarqNode root;
  SarqType * type;
  size_t size;
};

/* struct sarqNode type for Sarq nodes */
struct sarqNode {
  void * item;
  SarqNode left;
  SarqNode right;
};

/* Random number globals and prototypes */
static int randSeeded = 0;
static int bits[31];
static int bitCount = 0;
static void seedRand(void) ;
static void get31bits() ;
static int coinToss(void) ;

/* Default SarqType functions */
static int defCompareFunc(void * left, void * right) {
  return (int) ((uintptr_t) left - (uintptr_t) right);
} 
static void defPrintFunc(FILE * stream, void * item) {
  fprintf(stream, "%ld", (uintptr_t) item);
}
static void defFreeFunc(void * item) { }
static SarqType defSarqType = { defCompareFunc, defPrintFunc, defFreeFunc };


/**
 * Utility functions
 */

/* Function to exit with error 101 */
static void die(void) {
  fprintf(stderr, "Out of Memory Error.\n");
  exit(101);
}

/* Generic function to allocate memory or die */
static void * mallocFunc(size_t size) {
  void * obj = malloc(size);
  if (obj == NULL) die();
  return obj;
}


/**
 * Static SarqNode alloc & destroy, access, mutate functions
 */

/* Malloc a SarqNode function */
static SarqNode mallocSNode(void * item) {
  SarqNode snode = (SarqNode) mallocFunc(sizeof(struct sarqNode));
  snode->item = item;
  snode->left = NULL;
  snode->right = NULL;
  return snode;
}

/* Free an SNode function */
static void freeSNode(SarqNode snode) {
  if (snode) {
    free(snode);
    snode = NULL;
  }
}

/* Free all SarqNode's of a given root SarqNode */
static void freeAllSNode(SarqNode root, SarqType * type) {
  SarqNode thisNode = root;
  if (thisNode == NULL) return;

  type->free(thisNode->item);
  thisNode->item = NULL;
  freeAllSNode(thisNode->left, type);
  freeAllSNode(thisNode->right, type);
  freeSNode(thisNode);
}

/* Print all SNodes in a Sarq */
static void printNodes(FILE * stream, SarqNode root, SarqType * type) {
  if ((root) == NULL) {
    fprintf(stream, "-");
    return;
  }
  fprintf(stream, " (");
  type->print(stream, root->item);
  fprintf(stream, " ");
  printNodes(stream, root->left, type);
  fprintf(stream, " ");
  printNodes(stream, root->right, type);
  fprintf(stream, ") ");
}

/* Merge two SNodes given SarqType, then return result */
static SarqNode mergeSNodes(SarqNode this, SarqNode that, SarqType * type) {
  SarqNode hi = NULL;
  SarqNode lo = NULL;

  if (this == NULL) return that;
  if (that == NULL) return this;
  if ( type->compare(this->item, that->item) <= 0 ) {
    hi = this;
    lo = that;
    } else {
    hi = that;
    lo = this;
    }
    if (HEADS) 
      hi->left = mergeSNodes(hi->left, lo, type);
    else 
      hi->right = mergeSNodes(hi->right, lo, type);
 
  return hi;
}

/* Pop off & return the root SarqNode, & fix Sarq */
static SarqNode popSarqRoot(Sarq * s) {
  SarqNode sl = s->root->left;
  SarqNode sr = s->root->right;
  SarqNode root = s->root;
  s->root = mergeSNodes(sl, sr, s->type);
  return root;
}

/* Pop off root, get item, free root, return item */
static void * removeRoot(Sarq * s) {
  SarqNode root;
  void * item;
  root = popSarqRoot(s);
  item = root->item; 
  freeSNode(root);
  return item;
}

/* Recursive depth-finder function */
static size_t getDepth(SarqNode snode, size_t count) { 
  size_t dl, dr;
  if (snode == NULL) return 0;
  dl = getDepth(snode->left, count);
  dr = getDepth(snode->right, count);
  count = (dl > dr) ? dl + 1 : dr + 1 ;
  return count;
}


/**
 * Make a Sarq
 */
Sarq * SarqMalloc(SarqType * info) {
  Sarq * s = (Sarq *) malloc(sizeof(struct sarq));
  if (!info) info = &defSarqType;
  s->type = (SarqType *) malloc(sizeof(SarqType));
  s->type->compare = info->compare;
  s->type->print = info->print;
  s->type->free = info->free;
  s->root = NULL;
  s->size = 0;
  return s;
}

/**
 * Free a Sarq
 */
void SarqFree(Sarq * s) {
  if (s) {
    freeAllSNode(s->root, s->type);
    free(s->type);
    free(s);
  }
}

/**
 * Insert an item into a Sarq.
 */
void SarqInsert(Sarq * s, void * item) {
  SarqNode newRoot = mallocSNode(item);
  s->root = mergeSNodes(s->root, newRoot, s->type);
  ++s->size;
}

/**
 * If SarqSize(s)==0, return 0.  Otherwise, remove and return the
 * highest priority item currently stored in 's', consequently
 * decreasing SarqSize(s) by one.
 */
void * SarqRemove(Sarq * s) {
  void * item;
  if (!SarqSize(s)) return NULL;
  item = removeRoot(s);
  --s->size;
  return item;
}

/**
 * Function to print a Sarq 
 */
void SarqPrint(FILE * stream, Sarq * s) {
  printNodes(stream, s->root, s->type);
}

/**
 * Get Size of a Sarq 
 */
size_t SarqSize(Sarq * s) {
  return s->size;
}

/**
 * Get depth of a Sarq
 */
size_t SarqDepth(Sarq * s) {
  size_t dl = 0, dr = 0;
  if (s->root == NULL) return 0;
  dl = getDepth(s->root->left, dl);
  dr = getDepth(s->root->right, dr);
  return (dl > dr) ? dl + 1 : dr + 1;
}

/*
 * Random number functions
 */
static void seedRand(void) {
  srand(time(0));
  randSeeded = 1;
  return;
}
static void get31bits() {
  int i;
  uint32_t num = rand();
  for (i = 30; i >= 0; --i) {
    bits[i] = num & 0x1;
    num = num >> 1;
  }
  bitCount = 31;
  return;
}
static int coinToss(void) {
  if (!randSeeded) seedRand();
  if (!bitCount) get31bits();  
  return (int) bits[--bitCount];
}

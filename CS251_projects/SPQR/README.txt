SPQR - Final Project Part II

Joseph Edwards <jee8th@unm.edu>
UNM CS241 Fall 2011

DESCRIPTION:

       SPQR is a program for measuring the  performance  of  various  priority
       queue   implementations  including  Sarq's  (Self-Adjusting  Randomized
       Queues) and brute-force implementations.  SPQR is  highly  configurable
       with respects to various benchmarking parameters, and produces lightly-
       reduced data outputs that can be supplied to various data analysis  and
       graphing programs such as gnuplot.

       The  behavior  of  SPQR   is controlled by a configuration file which is
       read from standard input.  The configuration file format is essentially
       'KEYWORD=VALUE'    pairs  presented one pair per line, with various rules
       for comments and whitespace; see the file SPQRconfig.txt (or a  similar
       filename  including  version information) for a detailed description of
       the configuration file format, and  see the man page in SPQR.txt for 
       complete information about the OPTIONS. 

       NOTE: This implementation is dependent on a "defaultconfig.txt" file that
       must remain in the same folder as the executable SPQR program. If moved,
       the included "runSPQR" script described below can be used to generate a
       new "defaultconfig.txt" file.

MAKEFILE:

        Type "make" to compile SPQR from source.

CONFIG, RUN AND PLOT SCRIPT:

        "runSPQR" is a script included with SPQR that provides an easier
        way to create config files, run tests, and generate plots from the 
        command line, without editing configuration files. Type "runSPQR -h" 
        for usage. The option flags correspond to the tags used by SPQR.

        Default execution of runSPQR creates (2) files: configuration file
        "autospqr.txt" and "autospqr.dat" results of the SPQR run. Users
        may specify a filename using the "-f FILENAME" option.

        To generate a new "defaultconfig.txt" file with OUTPUT_LABEL, type:

        $ runSPQR -L -f defaultconfig

        If the flag "-P" is used, "runSPQR" also attempts to use gnuplot to
        generate a plot of the data, stored in "autospqr.ps" (or as specified).

REPORT TEMPLATE:

       The "report" folder contains a simple LaTeX template file and dependent figures
       to create comparisons between Sarq and SAry benchmark results.

BUG REPORT:

    * Out of memory errors have not been valgrind'd (valground?) while happening.

    * My debug-config files aren't thorough, but SPQR was valgrind clean up to 100K
      queries (N) in both PQs implemented (Sarq & SAry).

    * My Sarq was buggy, to begin with, but I think I (mostly) fixed it.

    * This version was written and tested on a 64-bit Intel MacBook running Linux 2.6
      (Mint Edition). I have no idea if is ILP-32 safe, or how safe.

    * The source code isn't heavily annotated. Related code is grouped under headings.

    * Random OPTION doesn't shuffle the input data. Instead a random number in the
      N domain is inserted, sequentially. Duplicates may (read: will) occur.

#ifndef SARY_H
#define SARY_H

typedef struct strawary * SAry;

struct strawary {
  int * array;
  size_t size;
};

extern SAry SAryMalloc(size_t size) ;
extern void SAryInsert(SAry sary, int k) ;
extern int SAryRemove(SAry sary) ;
extern void SAryFree(SAry sary) ;
extern size_t SArySize(SAry sary);

#endif /* SARY_H */

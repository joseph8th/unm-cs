SPQR configuration File Format  
Version 1.0: Tue Nov 29 14:29:48 2011 
Version 0.9.1: Thu Nov 17 15:51:50 2011

The SPQR config file format is a lightly customized version of
standard config file formats.  All code for parsing SPQR config files
must be written from scratch by the implementor; no external libraries
may be used.

(F.1) General format issues
(F.1.1) File contents is ASCII text and line-based, with one
        KEYWORD/VALUE pair per line. 
(F.1.2) Each 'line' is terminated by the first '\n' encountered, as
        returned by fgets.

(F.2) Basic line interpretation guidelines
(F.2.1) It is an input error if a line is longer than 256 chars including
        the terminating '\n'.  

  (F.2.1.1) If an overlength line is encountered, an error is printed
        to stderr, that line is ignored, and parsing of the rest of
        configuration file _continues_.  However, after the entire
        configuration file has been parsed, SPQR execution then stops
        if any parsing errors were encountered.  (In other words, a
        configuration file may produce many error reports in a
        single SPQR run, but if any errors are detected, execution
        stops after configuration finishes.)

(F.2.2) On each line, all leading and trailing whitespace is ignored.
  (F.2.2.1) In this spec, 'whitespace' is defined to mean all chars for which
           'isspace()' returns non-zero.

(F.2.3) Any line that is empty, or ends up empty after (F.2.2), is
        ignored

(F.2.4) COMMENTS: Any line beginning with ';', '#', or '!' after
        whitespace removal are interpreted as comments and are
        ignored.

(F.3) Keyword value pairs

(F.3.1) A line that isn't ignored under the rules of (F.2) is of the form
     KEYWORD = VALUE  

(F.3.2) Any amount of optional whitespace may appear on either side
        of the '='.  

(F.3.2) The KEYWORD consists of one or more alphanumeric characters
        plus '_'.

(F.3.3) The VALUE consists of zero or more characters of any kind,
        except that any leading and trailing whitespace is removed
        from VALUE.

        (F.3.3.1) Also, after the processing of (F.3.3), if VALUE
        both begins and ends with '"'s, they are both removed.

(F.3.4) The maximum size of KEYWORDs and VALUEs are is limited only by
        the overall line length bounds.

(F.4) Legal configuration file examples

(F.4.1) This section contains some examples of syntactically-legal
configuration files.  Each example delimited by '---start---' and
'---end---' markers that are not part of the file.  Note, however,
that the specific keywords and values in these examples are just
examples, and do not correspond to actual SPQR usage.

(F.4.1.1)
---start---
---end---
(F.4.1.1.1) An empty file is a legal configuration file

(F.4.1.2)
---start---
# This is a comment
   ; So is this.
	! This too

# Note that the previous line, which is empty, is legal
    	
# Note that the previous line, which is empty except for whitespace,
# is legal too
---end---
(F.4.1.2.1) A file containing only comments and empty lines is file is
a legal configuration file

(F.4.1.3)
---start---
# Define the FOO variable:
   FOO =12
! Ditto the BAR variable:
BAR=13.579

# And GAH as well
		GAH		= foonly
  ; Note that although a KEYWORD cannot be empty, the VALUE can be:

EMPTY=

# any combination of alphanumerics and '_' is
; legal for a keyword name:

 99_percent_aRISE=  whitespace, etc, in the value is legal, but leading and trailing w/s gets stripped
---end---
(F.4.1.3.1) Legal keyword / value lines can have whitespace in a
variety of places, or not.

(F.4.1.4)
---start---
# VALUEs can have anything in them, including '='s:
  FOO====this value begins with three '='s and ends with two==   
BAR  = ===this value too!==   
GAH  = === "     "    ", plus it has embedded double quotes!==   

! VALUEs can be quoted to preserve leading/trailing whitespace:
HOP= " this value begins with a space and ends with two spaces  "      
POP=" this value too!  "      

# Quoted values can even include embedded "'s:
COP=" this "value" begins with a space, ends with a '.' and has three '"'s."
YIKES=""this value begins with a double quote and ends with a single quote'"
---end---
(F.4.1.4.1) Values can have almost anything in them, except NULL bytes
and newlines, and can be quoted with double quotes, for example to
preserve leading and trailing whitespace.

(F.5) Illegal configuration file examples

(F.5.1) This section contains some examples of syntactically-illegal
configuration files.  Each example delimited by '---start---' and
'---end---' markers that are not part of the file.  Note, however,
that the specific keywords and values in these examples are just
examples, and do not correspond to actual SPQR usage.

(F.5.1.1)
---start---
 FOO BAR=GAH
 FOO-BAR=GAH
 FOO/BAR=GAH
 FOO:BAR=GAH
---end---
(F.5.1.1.1) A keyword cannot contain whitespace, punctuation, or
anything but alphanumerics and '_'

(F.5.1.2)
---start---
  =GAH
---end---
(F.5.1.2.1) A keyword cannot be empty.

(F.5.1.3)
---start---
  FOO="a double quoted value must end with a double quote!
  BAR=a double quoted value must start with a double quote!"
---end---
(F.5.1.3.1) The double quote must appear at the beginning and the
ending, or in neither position.

(F.6) This section intentionally left blank.

(F.7) This section intentionally left blank.

(F.8) This section intentionally left blank.

(F.9) Revision history
(F.9.1) Version 0.9.1: Thu Nov 17 15:51:50 2011: Add (F4)-(F9)
(F.9.2) Version 1.0: Tue Nov 29 14:29:48 2011 Fix misc typoes

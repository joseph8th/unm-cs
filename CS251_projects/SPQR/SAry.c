#include <stdio.h>
#include <stdlib.h>
#include "SAry.h"

static void die(char * msg, int err) {
  fprintf(stderr, "Error: %s\n", msg);
  exit(err);
}

SAry SAryMalloc(size_t size) {
  SAry sary;
  int * ary = malloc(size*sizeof(int));
  if (ary == NULL)
    die("Out of memory.", 3);
  sary = malloc(sizeof(struct strawary));
  if (sary == NULL)
    die("Out of memory.", 3);
  sary->array = ary;
  sary->size = 0;
  return sary;
}

void SAryInsert(SAry sary, int k) {
  sary->array[sary->size] = k;
  ++sary->size;
}

int SAryRemove(SAry sary) {
  int i = 0, j;
  int m = sary->array[0];
  for (j = 1; j < sary->size; ++j) {
    if (sary->array[j] < m) {
      m = sary->array[j];
      i = j;
    }
  }
  for (j = i; j < sary->size - 1; ++j) {
    sary->array[j] = sary->array[j+1];
  }
  --sary->size;
  return m;
}

void SAryFree(SAry sary) {
  free(sary->array);
  free(sary);
}

size_t SArySize(SAry sary) {
  return sary->size;
}

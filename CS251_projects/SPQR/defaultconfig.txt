# Default SPQR config file.
# Defines legal keywords and default options.

ALGORITHM = sarq
FIRST_SEED = 100
INPUT_ORDER = random
INPUTS_PER_REMOVAL = 0
LOOPS = 5
N_MAX = 10000
N_MIN = 10
N_MULTIPLIER = 200
OUTPUT_LABEL = # %a %o %r %l %s (%n)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "configSPQR.h"

#define ARYLEN 9
#define DEFAULT_OPTIONS_FILENAME "./defaultconfig.txt"

struct cmapnode {
  char * keyword;
  char * value;
  CMapNode next;
};

struct configmap {
  struct cmapnode * root;
  size_t size;
};

/* CMap to store kw/vals from default config file */
static CMap defCMap;

/* A struct array for reading config file
 * Defines legal keywords for this implementation. 
 * Stores config values until config struct set. */
static struct {
  char * keyword;
  char * value;
} config[] = {
  { "ALGORITHM", NULL },
  { "FIRST_SEED", NULL },
  { "INPUT_ORDER", NULL },
  { "INPUTS_PER_REMOVAL", NULL },
  { "LOOPS", NULL },
  { "N_MAX", NULL },
  { "N_MIN", NULL },
  { "N_MULTIPLIER", NULL },
  { "OUTPUT_LABEL", NULL }
};

/* Declaration for a long fcn to be at bottom */
static int setConfigValue(Config c, int ix, char * val) ;

/*
 * Error handling stuff
 */
static int configError = 0;

/* SPQR.c uses this extern die fcn, too */
void die(char * msg, int err) {
  fprintf(stderr, "Fatal Error: %s\n", msg);
  exit(err);
}

static void printErr(char * msg, char * txt) {
  fprintf(stderr, "%s: Config Error (%d): %s\n", txt, configError, msg);
  ++configError;
}

/*
 * Creation and destruction functions
 */

char * mallocZStr() {
   char * zstr = (char *) malloc(sizeof(char)*257);
   if (zstr == NULL)
     die("Out of memory.", 1);
   *zstr = '\0';
   return zstr;
}

static CMapNode mallocCNode() {
  CMapNode cnode = (CMapNode) malloc(sizeof(struct cmapnode));
  if (cnode == NULL)
    die("Out of memory.", 1);
  cnode->next = NULL;
  cnode->keyword = '\0';
  cnode->value = '\0';
  return cnode;
}

static CMap mallocCMap() {
  CMap cmap = (CMap) malloc(sizeof(struct configmap));
  if (cmap == NULL)
    die("Out of memory.", 1);
  cmap->root = NULL;
  cmap->size = 0;
  return cmap;
}

static Config mallocConfig() {
  Config c = (Config) malloc(sizeof(struct spqrConfig));
  if (c == NULL)
    die("Out of memory.", 1);
  return c;
}

/*
 * Functions to get & parse kw/val pairs
 */

static int getKeyPair(char * line) {
  int eq = 0;
  char ch;

  while ((ch = line[eq]) != '\0') {
    if (ch == '=') break;
    ++eq;
  }
  return eq;
}

static int isComment(char ch) {
  switch (ch) {
  case '!':
  case '#':
  case ';':
  case '\0':
    return 1;

  default:
    return 0;
  }
}

static char * rmLeadSpace(char * zstr) {
  char * tmp;

  while (isspace(*zstr)) {
    tmp = ++zstr;
    zstr = rmLeadSpace(tmp);
  }
  return zstr;
}

static char * rmTailSpace(char * zstr, int end) {
  if (end) {
    while (isspace(zstr[end])) {
      zstr[end] = '\0';
      zstr = rmTailSpace(zstr, end);
      --end;
    }
  }
  return zstr;
}

static char * rmSpace(char * zstr) {
  char * tmp;
  int end;

  if (*zstr == '\0') return zstr;
  end = strlen(zstr) - 1;
  tmp = rmLeadSpace(rmTailSpace(zstr, end));
  memmove(zstr, tmp, strlen(tmp)+1);

  return zstr;
}

static int rmDQuotes(char * val) {
  char * tmp;
  int end = strlen(val) - 1;

  if (*val == '\0') return 1;
  if ((val[0] == '"') && (val[end] != '"'))
    return 0;
  if ((val[0] != '"') && (val[end] == '"'))
    return 0;
  if (val[0] == '"') {
    tmp = mallocZStr();
    val[end] = '\0';
    strcpy(tmp, &val[1]);
    strcpy(val, tmp);
    free(tmp);
  }
  return 1;
}

static int isKWChar(char ch) {
  if ((ch >= '0') && (ch <= '_')) { /*T*/

    if ((ch > '9') && (ch < 'A')) 
      return 0; /*F*/
    if ((ch > 'Z') && (ch != '_')) 
      return 0; /*F*/

    return 1;
  }
  return 0;
}

static int kwParse(char * kw) {
  kw = rmSpace(kw);
  while (*kw != '\0') {
    if (!isKWChar(*kw)) 
      return 0;
    ++kw;
  }
  return 1;
}

static char * getKeyword(char * line, int eq) {
  char * kw = mallocZStr();

  strncpy(kw, line, eq);
  kw[eq] = '\0';
  if (!kwParse(kw))
    printErr("Invalid parameter keyword.", line);

  return kw;
}

static int valParse(char * val) {
  return rmDQuotes(rmSpace(val));
}

static char * getValue(char * line, int eq) {
  char * tmp;
  char * val = mallocZStr();

  if (eq < 254) {
    tmp  = &line[eq + 1];
    strcpy(val, tmp);
    if (!valParse(val))
      printErr("Invalid parameter value.", line);
  }
  return val;
}


/*
 * Functions to handle config map structure 
 */

static CMapNode nextCNode(char * line) {
  CMapNode cnode;

  int eqSign = getKeyPair(line);
  if (!eqSign) 
    printErr("Keyword required in line: ", line);

  if (eqSign < strlen(line)) {
    cnode = mallocCNode();
    cnode->keyword = getKeyword(line, eqSign);
    cnode->value = getValue(line, eqSign);
    return cnode;
  }
  return NULL;
}

static void pushCNode(CMapNode cnode, CMap cmap) {
  cnode->next = cmap->root;
  cmap->root = cnode;
  ++(cmap->size);
}

static CMapNode popCNode(CMap cmap) {
  CMapNode cnode = cmap->root;

  if (cnode != NULL) {
    cmap->root = cnode->next;
    --(cmap->size);
    cnode->next = NULL;
  }
  return cnode;
}

static void freeCNode(CMapNode cnode) {
    free(cnode->keyword);
    cnode->keyword = NULL;
    free(cnode->value);
    cnode->value = NULL;
    free(cnode);
    cnode = NULL;
}

static void freeCMap(CMap cmap) {
  while (cmap->size) {
    freeCNode(popCNode(cmap));
  }
  free(cmap);
  cmap = NULL;
}

static char * getCValue(CMap cmap, char * keyword) {
  CMapNode cnode;

  if (cmap->size > 0) {
    cnode = cmap->root;
    while (cnode != NULL) {
      if (!strcmp(cnode->keyword, keyword)) {
        return cnode->value;
      }
      cnode = cnode->next;
    }
  }
  return NULL;
}

/*
 * Function to create CMap from config file
 */

static CMap getCMap(FILE * file) {
  CMapNode cnode;
  CMap cmap = mallocCMap();
  char * line = mallocZStr();

  while (fgets(line, 257, file)) {
    if (strlen(line) > 255) {
      freeCMap(cmap);
      free(line);
      line = NULL;
      die("Parameter line exceeds maximum line length", 1);
    }
    line = rmSpace(line);

    if ((!isComment(*line)) && (*line != '\0'))
      if ((cnode = nextCNode(line)) != NULL)
        pushCNode(cnode, cmap);
  }
  free(line);
  line = NULL;

  if (cmap->size > ARYLEN) {
    freeCMap(cmap);
    die("Duplicate keyword/value pairs illegal", 1);
  }
  return cmap;
}

/*
 * Function to initialize global config struct 
 */
static Config initConfig(CMap cmap) {
  char * val;
  int i = 0;
  Config c;

  FILE * file = fopen(DEFAULT_OPTIONS_FILENAME, "r");
  if (file == NULL) {
    freeCMap(cmap);
    die(DEFAULT_OPTIONS_FILENAME " -- Default configuration file not found.", 2);
  }
  defCMap = getCMap(file);                     /* Load default config map from default file */

  for (i = 0; i < ARYLEN; ++i) {                 /* Load default values into config ary 1st */
    config[i].value = getCValue(defCMap, config[i].keyword);
  }

  c = mallocConfig();                            /* Init the config struct to be used by SPQR */

  for (i = 0; i < ARYLEN; ++i) {
    val = getCValue(cmap, config[i].keyword);                      /* get user config value */

    if (val != NULL) {
      if (!setConfigValue(c, i, val))                                   /* check vals & set config struct */
        printErr("Invalid parameter value: ", "initConfig");    /* ... else log an error */
    }
    else if (!setConfigValue(c, i, config[i].value))                    /* load default vals if not user spec'd */
      printErr("Error in default config file: ", "initConfig");
  }
  fclose(file);
  return c;
}

/**
 * Extern declared functions to call config & free vals
 */

/* Configure global config struct and return error if any */
Config configSPQR(FILE * file) {

  CMap cmap = getCMap(file);
  Config c = initConfig(cmap);

  c->errors = configError;

  freeCMap(defCMap);
  freeCMap(cmap);

  return c;
}

/* Free config resources */
void freeSPQR(Config c) {
  free(c->output_label);
  free(c);
}

/* Long fcn to check options & set config struct */
static int setConfigValue(Config c, int ix, char * val) {
  int n;

  switch (ix) {
  case ALGORITHM:
    if (!strcmp(val, "sarq")) {
      c->algorithm = SARQ;
      return 1;
    }
    if (!strcmp(val, "array")) {
      c->algorithm = ARRAY;
      return 1;
    }
    if (!strcmp(val, "other")) {
      c->algorithm = OTHER;
      return 1;
    }
    break;
  case FIRST_SEED:
    if ((n = atoi(val)) > -1) {
      c->first_seed = (n) ? n : time(0); 
      return 1;
    }
    break;
  case INPUT_ORDER:
   if (!strcmp(val, "ascending")) {
     c->input_order = ASCENDING;
     return 1;
   } 
   if (!strcmp(val, "descending")) {
     c->input_order = DESCENDING;
     return 1;
   } 
   if (!strcmp(val, "alternating")) {
     c->input_order = ALTERNATING;
     return 1;
   } 
   if (!strcmp(val, "random")) {
     c->input_order = RANDOM;
     return 1;
   }
   break;
  case INPUTS_PER_REMOVAL:
    if ((n = atoi(val)) > -1) {
      c->inputs_per_removal = n;
      return 1;
    }
    break;
  case LOOPS:
    if ((n = atoi(val)) > 1) {
      c->loops = n;
      return 1;
    }
    break;
  case N_MAX:
    if ((n = atoi(val)) > 1) {
      c->n_max = n;
      return 1;
    }
    break;
  case N_MIN:
    if ((n = atoi(val)) > 0) {
      c->n_min = n;
      return 1;
    }
    break;
  case N_MULTIPLIER:
    if ((n = atoi(val)) > 100) {
      c->n_multiplier = n;
      return 1;
    }
    break;
  case OUTPUT_LABEL:
    c->output_label = mallocZStr();
    strcpy(c->output_label, val);
    return 1;
  }
  return 0;
}


# Test SPQR config file.

ALGORITHM = array
#FIRST_SEED = 0
INPUT_ORDER = alternating
INPUTS_PER_REMOVAL = 10
LOOPS = 5
N_MAX = 100000
N_MIN = 10000
N_MULTIPLIER = 150
OUTPUT_LABEL = # %a %o %r %l %s (%n)

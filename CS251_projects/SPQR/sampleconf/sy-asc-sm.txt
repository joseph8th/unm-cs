# Test SPQR config file.

ALGORITHM = array
FIRST_SEED = 0
INPUT_ORDER = ascending
INPUTS_PER_REMOVAL = 0
LOOPS = 5
N_MAX = 10000
N_MIN = 1000
N_MULTIPLIER = 150
OUTPUT_LABEL = # %a %o %r %l %s (%n)

#include <stdio.h>
#include <string.h>

/* Joseph Edwards, jee8th@unm.edu 
   CS241 F11 University of New Mexico

   Stepup 1: Find palindromes in command line arguments */;

#define maxchars 50

int main(int argc, char **argv) {

  int argcount = 1;
  int len, first, last;
  char revword[maxchars];

  if (argc==1) {    // exit if no arguments passed
    printf("\n Oops! Forgot some arguments?\n");
    return 0;
  }
  else {    // print palindromes
    printf("\n> ");
    for (argcount; argcount < argc; ++argcount) {   // loop thru argv's

   /* Function to reverse word moved to main(). Pointers are tricky! */

      len = strlen(argv[argcount]);
      last = len;
      revword[last] = '\0'; 
      --last;
      for (first=0; first<len; ++first, --last) {    // loop thru chars
	revword[last] = argv[argcount][first];       // reverse char order
      }

      if (strcmp(argv[argcount], revword) == 0)    // check if word=revword
	printf("%s ", revword);
    }
    printf("\n");
    return 0;
  }
}

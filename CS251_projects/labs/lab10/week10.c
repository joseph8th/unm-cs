#include <stdio.h>
#include <stdlib.h>

#define BRANCH "|---"
#define SPACE "    "
#define LIMB "|   "

typedef struct tree {
	struct tree * left;
	struct tree * right;
	int data;
} *tree;

tree makeTree( int d ) {
	tree t = malloc( sizeof( struct tree ) );
	t->left = 0;
	t->right = 0;
	t->data = d;
	return t;
}

static int nodeLevel = 0;

/**
 * implement me.
 *
 * Feel free to change the signature to add
 * arguments that make it work.
 *
 * You might add something like an indentation or
 * a string to prepend the next print call.
 *
 * Note that there is no one method that's better
 * than all others. Try to find a balance between
 * the elegance of your code and the elegance of
 * what appears on the screen.
 */
void printTreeVertical( tree t ) {
  int i = 0;
  if (t->right) {
    ++nodeLevel;
    printTreeVertical( t->right );
    --nodeLevel;
  }

  for (i = 0; i < nodeLevel; ++i) {
    if (i == nodeLevel - 1)
      printf(BRANCH);
    else if (i > 0)
      printf(LIMB);
    else
      printf(SPACE);
  }
  printf("%d\n", t->data);

  if (t->left) {
    ++nodeLevel;
    printTreeVertical( t->left );
    --nodeLevel;
  }
}

int main( ) {
	tree t = makeTree( 1 );
	t->left = makeTree( 2 );
	t->left->left = makeTree( 3 );
	t->left->right = makeTree( 4 );
	t->right = makeTree( 3 );
	t->right->right = makeTree( 5 );
	t->right->left = makeTree( 4 );
	t->left->right->right = makeTree( 8 );
	t->left->right->right->left = makeTree( 9 );


	printTreeVertical( t );
	
	return 0;

}

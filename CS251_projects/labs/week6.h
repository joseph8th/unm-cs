#ifndef WEEK6_H
#define WEEK6_H

typedef unsigned int (*BinaryFunc)(unsigned int, unsigned int);

extern int and(unsigned int* args );
extern int  or(unsigned int* args );
extern int xor(unsigned int* args );

#endif /* WEEK6_H */

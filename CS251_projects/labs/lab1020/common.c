#include "common.h"
#include <time.h>

static clock_t timeStart, timeStop;

void timerInit( ) {
	timeStart = (clock_t)NULL;
	timeStop  = (clock_t)NULL;
}

void timerStart( ) {
	timeStart = clock( );
}

long int timerStop( ) {
	if( !timeStart ) return (long int)NULL;

	timeStop = clock( );

	return getElapsed( );
}

long int getElapsed( ) {
	if( !timeStart || !timeStop ) return (long int)NULL;
	return (timeStop - timeStart);
}

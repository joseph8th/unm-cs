#include <stdio.h>
#include "main.h"
#include "lib.h"
#include <unistd.h>

void func( ) {
	sleep(1);
}

void nullFunc( ) {
	return;
}


int main ( ) {
	printf( "clock ticks in empty function: %ld\n", timeOp( &nullFunc ) );
	printf( "clock ticks in one second wait: %ld\n", timeOp( &func ) );
	printf( "clock ticks in colwise 10x10 write: %ld\n", timeOp( &writeColwise10x10 ) );
	printf( "clock ticks in colwise 10x10 write: %ld\n", timeOp( &writeColwise10x10 ) );
	printf( "clock ticks in colwise 100x100 write: %ld\n", timeOp( &writeColwise100x100 ) );
	printf( "clock ticks in rowwise 100x100 write: %ld\n", timeOp( &writeRowwise100x100 ) );
	printf( "clock ticks in colwise 1000x1000 write: %ld\n", timeOp( &writeColwise1000x1000 ) );
	printf( "clock ticks in rowwise 1000x1000 write: %ld\n", timeOp( &writeRowwise1000x1000 ) );
	
	return 0;
}

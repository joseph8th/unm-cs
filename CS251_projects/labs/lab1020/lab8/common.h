#ifndef COMMON_H
#define COMMON_H

void timerInit( );
void timerStart( );
long int timerStop( );
long int getElapsed( );

#endif

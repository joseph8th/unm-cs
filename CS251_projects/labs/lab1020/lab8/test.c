#include "test.h"
#include "main.h"
#include "lib.h"
#include <stdio.h>

void tester( void (*func1)(void), void (*func2)(void), char * msg ) {
	if( timeOp(func1) < timeOp(func2) )
		printf("+colwise faster for %s!\n",msg);
	else
		printf("-colwise slower for %s? freak computer\n",msg);
}

void test10( ) {
	tester( &writeColwise10x10, &writeRowwise10x10, "10x10" );
}

void test100( ) {
	tester( &writeColwise100x100, &writeRowwise100x100, "100x100" );
}

void test1000( ) {
	tester( &writeColwise1000x1000, &writeRowwise1000x1000, "1000x1000" );
}


int main( ) {
	test10( );
	test100( );
	test1000( );
	
	return 0;
}

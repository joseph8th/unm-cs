#ifndef LIB_H
#define LIB_H

#include "common.h"

long int timeOp( void (*ptr)(void) ); 

void writeColwise10x10( );
void writeRowwise10x10( );
void writeColwise100x100( );
void writeRowwise100x100( );
void writeColwise1000x1000( );
void writeRowwise1000x1000( );

#endif

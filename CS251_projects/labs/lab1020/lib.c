#include "lib.h"

long int timeOp( void (*func)(void) ) {
	timerInit( );
	timerStart( );
	func( );
	return timerStop( );
}

static void writeColwise( int rows, int cols ) {
	double mat[rows][cols];
	int row=0,col=0;

	for(; row<rows; row++)
		for(; col<cols; col++)
			mat[row][col] = 0;
}

static void writeRowwise( int rows, int cols ) {
	double mat[rows][cols];
	int row=0,col=0;

	for(; col<cols; col++)
		for(; row<rows; row++)
			mat[row][col] = 0;
}

void writeColwise10x10( ) {
	writeColwise(10,10);
}

void writeRowwise10x10( ) {
	writeRowwise(10,10);
}

void writeColwise100x100( ) {
	writeColwise(100,100);
}

void writeRowwise100x100( ) {
	writeRowwise(100,100);
}

void writeColwise1000x1000( ) {
	writeColwise(1000,1000);
}

void writeRowwise1000x1000( ) {
	writeRowwise(1000,1000);
}

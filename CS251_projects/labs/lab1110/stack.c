#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <inttypes.h>
#include "stack.h"

#define SUB1( x ) ( x > 0 ? x - 1 : x )


struct frame {
	size_t varCount;
	void **  vars;
};

struct stack {
	size_t frameCount;
	FramePrintFunc print;
	Frame  frames[50];
};


static void defaultPrint( size_t len, void ** vars ) {
	int count;
	int varNum;
	
	count = (int)len;
	printf( "( " );
	if( !count ) {
		printf( ")\n" );
		return;
	}
	for( varNum = 0; varNum < count; varNum++ )
		printf( "%ld ", (uintptr_t)vars[varNum] );
	printf( ")\n" );
}


Stack * makeStack( FramePrintFunc p ) {
	Stack * s = malloc( sizeof( Stack ) );
	if( !p )
		s->print = &defaultPrint;
	else
		s->print = p;
	
	s->frameCount = 0;
	return s;
}

size_t stackSize( Stack * s ) {
	return s->frameCount;
}

void stackPush( Stack * s, size_t len, ... ) {
	int i;
	Frame * f;
	va_list ap;
	
	if( s->frameCount >= 50 ) {
		fprintf( stderr, "stack full. Frame dropped\n" );
		return;
	}

	f = &(s->frames[s->frameCount]);
	f->varCount = len;
	f->vars = malloc( len * sizeof( void * ) );
	
	va_start( ap, len );
	for( i=0; i < len; i++ )
		f->vars[i] = va_arg( ap, void * );
	va_end( ap );

	s->frameCount++;
}

void ** stackPop( Stack * s ) {
	if( !s->frameCount ) {
		fprintf( stderr, "stack empty. Returning NULL\n" );
		return NULL;
	}
	
	return s->frames[--s->frameCount].vars;
}

void stackPrint( Stack * s ) {
	int frameNum = 0;

	printf( "-----stack start-----\n" );
	
	for( ; frameNum < s->frameCount; frameNum++ ) {
		s->print( s->frames[frameNum].varCount, s->frames[frameNum].vars );
	}

	printf( "-----stack  end------\n" );
}

void stackFreeFrames( Stack * s ) {
	int i = 0;
	while( i++ < s->frameCount )
		free( s->frames[s->frameCount].vars );
	s->frameCount = 0;
}

void stackFree( Stack * s ) {
	stackFreeFrames( s );
 	free( s ); 
} 

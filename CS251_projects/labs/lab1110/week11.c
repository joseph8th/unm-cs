#include <stdio.h>
#include <time.h>
#include <inttypes.h>
#include <stdlib.h>
#include "stack.h"
#include "week11.h"

#define CHILD( t, dir ) ( dir ? t->right : t->left )
#define max( x, y ) ( x > y ? x : y )

/* Tree stuff comes first */

struct tree {
	struct tree * left;
	struct tree * right;
	int data;
};

/* printing stuff */

void printTreeVertical1( Tree * t, int indent ) {
	static int increment = 4;
	int i = 0;
	if( !t ) return;

	printTreeVertical1( t->right, indent + increment );
	
	while( (i++) < indent-increment ) printf( " " );
	if( indent > 0 ) printf("|");
	while( (i++) < indent ) printf( "-" );

	printf( "%d\n", t->data );
		
	printTreeVertical1( t->left, indent + increment );	
}

void printTreeVertical( Tree * t, int indent ) {
	static int increment = 4;
	int i = 0;
	if( !t ) return;

	printTreeVertical( t->right, indent + increment );
	while( (i++) < indent ) printf( " " );
	printf( "%d\n", t->data );
	printTreeVertical( t->left, indent + increment );
	
}

/* Used to match the signature required by the stack */
void printTreeVerticalMask( size_t len, void ** vars ) {
	printTreeVertical1( (Tree *)vars[2], 0 );
	printf("**********\n");
}

Tree * makeTree( ) {
	Tree * t = malloc( sizeof( struct tree ) );
	t->left = 0;
	t->right = 0;
	t->data = rand( ) % 100;
	return t;
}

void freeTree( Tree * t ) {
	if( !t ) return;
	
	freeTree( t->left );
	freeTree( t->right );
	free( t );
}

void showMeDepth( int n ) {
	Stack * s = makeStack( &printTreeVerticalMask );
	Tree * t;
	Tree * tmpNode;
	int i;
	char dir;
	void ** args;
	
	/* Populate random tree of size n */
	srand( time( 0 ) );
	srand( rand( ) );
	tmpNode = t = makeTree( );

	for( i=1; i<n; i++ ) {
		while( ( CHILD( tmpNode,
										/*Choose a random direction at every branch*/
										( dir = rand() % 2 ) ) ) )
			tmpNode = CHILD( tmpNode, dir );

		if( dir )
			tmpNode->right = makeTree( );
		else
			tmpNode->left  = makeTree( );
		
		tmpNode = t;
	}

	/* End tree generator */

	
	tmpNode = t;
	while( 1 ) {
		/* We've reached a leaf */
		if( !tmpNode ) {
			/* Popped last element(head) off the stack */
			if( !stackSize( s ) ) break;


			/* Print it for us(move this or add more for better resolution */
			stackPrint( s );
			printf( "\n" );
			/* Get back to parent */
			args = stackPop( s );

			switch ((uintptr_t)args[0]) {
			case 0: /* Been down left side. Visiting right */
				stackPush( s, 3, 1, i, args[2] );
				tmpNode = ((Tree *)args[2])->right;
				break;
			case 1: /* Done with left and right. finish up */
				/* i is whatever depth was for right side. left depth
					 is stored in args[1] */
				i = 1+ max( (uintptr_t)args[1], i );
				tmpNode = NULL;
				break;
			}
			
			free( args );
			continue;
		}

		/* If we're still going down a side of the tree, push this
			 node on the stack and continue onward. Worry about depth later */
		stackPush( s, 3, 0, 0, tmpNode );
		tmpNode = tmpNode->left;
		/* i is our temp counter for depth. Since we haven't reached
			 the bottom, don't start counting just yet. */
		i = 0;
	}

	freeTree( t );

	printf( "depth: %d\n", i );
}

void showMeFact( int n ) {
	Stack * s = makeStack( NULL );
	int i = n;
	int fact = 1;
	void ** frameVars;

	/*
	 * Push all frames onto the stack, emulating the process
	 * that would take place during recursion.
	 */
	for( ; i>=0; i-- ) {
		/*
		 * The next line is included to make clear the role of the base
		 * case test in this iterative implementation.
		 */
		if( i == 0 ) break;
		
		stackPush( s, 1, (uintptr_t)i );
	}

	/*
	 * Print thet stack after we hit our base cose to see what it
	 * actually looks like.
	 *
	 * We know we hit our base case here, because if we were writing
	 * a fact function recursively, we would stop at 0, which is where
	 * the loop ends.
	 *
	 * Add more calls to stackPrint around the function if you want to
	 * see the state of the stack at different points during the calculation.
	 */
	printf( "Stack contents at factorial base case:\n" );
	stackPrint( s );

	/*
	 * Pop the frames off of our stack, and perform the operation
	 * that we perform on the way back up from our base case.
	 */
	for( ; i < n; i++ ) {
		frameVars = stackPop( s );
		printf( "popped:\n" );
		fact *= (uintptr_t)frameVars[0];
		free( frameVars );

		stackPrint( s );
		printf( "Intermediate fact value: %d\n\n", fact ); 
	}

	printf( "\nfact %d: %d\n", n, fact );
	
}

int main( void ) {
	showMeFact( 5 );
	printf( "\n\n\n\n--------\n" );
	showMeDepth( 6 );
	return 0;
}


#ifndef STACK_H
#define STACK_H

/*
 * A Frame is a context under which a function call lives.
 * in this simple case, it consists of a list of variables
 * that represent the state of the function.
 */
typedef struct frame Frame;

/*
 * A stack is a structure that contains a list of Frames.
 * Frames can be pushed onto the top of the stack, and
 * popped off the top just as easily, though they cannot
 * be picked up from an arbitrary position.
 * 
 * This simple stack assums that all variables are ints, though
 * the user can treat these ints as pointers if further functionality
 * is needed.
 */
typedef struct stack Stack;

/*
 * A FramePrintFunc is a pointer to a function that knows
 * how to print the contents of a frame represented as a
 * list of void *'s.
 */
typedef void (*FramePrintFunc)( size_t len, void ** args );

/*
 * Returns a new, empty Stack for emulating recursion.
 *
 */
Stack * makeStack( FramePrintFunc f );

/*
 * Returns the number of frames inside of s
 */
size_t stackSize( Stack * s );

/*
 * Pushes a new Frame onto the stack.
 *
 * stackPush takes a Stack s, a length variable n representing
 * the number of variables this frame will contain, and n ints.
 */
void    stackPush( Stack * s, size_t len, ... );

/*
 * Returns a pointer to the list of variables that represent the Frame
 * at the top of the stack.
 *
 * After a call to stackPop, the user takes ownership of the pointer,
 * and is responsible for freeing the space allocated for the variable
 * list.
 */
void ** stackPop( Stack * s );

/*
 * prints the stack, with a simple bracketed format.
 *
 * i.e.
 * -----
 * ( 1 2 3 )
 * ( 1 1 1 )
 * ( 8 91 )
 * ( )
 * -----
 */
void    stackPrint( Stack * s );

/*
 * Frees the frames contained in this stack, as well as their data pointers
 *
 * This function is called in stackFree for convenience, but is available to
 * users in the event that a simple Stack flush is desired.
 */
void    stackFreeFrames( Stack * s );

/*
 * Frees the stack and all of its contents.  Calls stackFreeFrames.
 */
void    stackFree( Stack * s );


#endif

#ifndef WEEK11_H
#define WEEK11_H

typedef struct tree Tree;

/*
 * Runs through a calculation of factorial, displaying the
 * contents of a simple stack throughout the process.
 */
void showMeFact( int n );

/*
 * Creates a random tree of size n, whose nodes contain
 * random numbers between 0 and 100, then calculates the
 * depth of the tree, printing the contents of the stack
 * every time a frame is about to be popped off.
 */
void showMeDepth( int n );

#endif WEEK11_H

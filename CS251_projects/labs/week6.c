#include<stdio.h>
#include<string.h>
#include "week6.h"


static unsigned int intAnd( unsigned int a, unsigned int b ) { return a & b; }
static unsigned int  intOr( unsigned int a, unsigned int b ) { return a | b; }
static unsigned int intXor( unsigned int a, unsigned int b ) { return a ^ b; }

static unsigned int foldfunc( BinaryFunc f, unsigned int *args ) {
	int i = 1;
	unsigned int retInt = args[0];
	for( ; args[i] != -1; i++ ) {
		retInt = (*f)(retInt, args[i]); 
	}

	args[0]=retInt;
	return retInt;
}

/**
 * These functions map their respective operations across an arbitrary
 * number of ints, and return the result
 */
int and(unsigned int* args ) { foldfunc( &intAnd, args ); }
int  or(unsigned int* args ) { foldfunc( &intOr,  args ); }
int xor(unsigned int* args ) { foldfunc( &intXor, args ); }



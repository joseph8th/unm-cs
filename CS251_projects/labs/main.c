#include<stdio.h>
#include "week6.h"

int main( ) {
	int i = 0;
	int ints1[4] = {15,6,12,-1};
	int ints2[5] = {1,2,4,5,-1};
	int ints3[4] = {15,3,8,-1};
	unsigned int longList[7] = {
		0xffffffff, 0x000ff070,
		0x080ff050, 0x0c0ff0f0,
		0x070ff0f0, 0x1a3ff0d0,
		-1
	};

	and( ints1 );
	or( ints2 );
	xor( ints3 );
	
	printf( "result of 'and' over %s: %x\n", "15,6,12", ints1[0] );
	printf( "result of 'or' over %s: %x\n", "1,2,4,5", ints2[0] );
	printf( "result of 'xor' over %s: %x\n", "15,3,8", ints3[0] );

	printf( "result of 'and' over:\n" );
	for( ; i<6; i++ ) {
		printf( "\t%8x\n", longList[i] );
	}
	printf( "\t--------\n" );
	
	and( longList );

	printf( "\t%8x\n", longList[0] );
	
	return 0;
}

Program 1: ncrpt autodetecting encryption/decryption implementation

AUTHOR:
	Joseph Edwards <jee8th@unm.edu>
	CS241 Fall 2011 - University of New Mexico

BUG LIST:
	* Max 32-bit unsigned integer buffer is 65535, at the moment, which gives max 262140 index for 8-bit char array cells for function that gets the next 8 bits from random 32 bit unsigned integer (uint32_t). This could cause a buffer overflow in the right situation, I suppose.
	* No other bugs known at this time.
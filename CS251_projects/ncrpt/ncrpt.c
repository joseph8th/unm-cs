/*
 * ncrpt implementation with autodetection *
 * Author: Joseph Edwards <jee8th@unm.edu> *
 * UNM CS241 Fall 2011                     */

#include <stdlib.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "rand.h"

#define maxBuffer 65535                 /* 1/4 size of char buf */

/* Declare random context */
static randctx randomContext;              /* From randDemo.c */
static void initialize(uint32_t seed) {
  int i;
  for (i = 0; i < RANDSIZ; ++i) 
    randomContext.randrsl[i] = seed;       /* Make seed context */
  randinit(&randomContext, 1);             /* Initialize generator */
}

/* Other static vars and functions */
static char *optionSet[] =                 /* Valid options array */
  { "-e", "--encrypt", "-d", "--decrypt", "-h", "--help" };

void printHelp(void);                      /* Fcn to print help */
void isValidKey(char *);                   /* Fcn to validate hexkey */
int parseOpts(int, char **);               /* Fcn to parse args */
unsigned get8Bits (uint32_t, int);         /* Fcn to get 8 bits */
void nordcrpt(int, uint32_t);              /* Fcn to n- or d-crpt */


int main(int argc, char **argv) {

  int userOpt;                          /* Holds option from *argv[] */
  uint32_t hexKey;                      /* Holds hexkey from *argv[] */

  /* Parse args & return userOpt, or err out */
  userOpt = parseOpts(argc, argv);

  /* Encrypt or decrypt, depending on userOpt */
  hexKey = strtoul(argv[--argc], NULL, 16);
  initialize(hexKey);                   /* Seed the random number generator */
  nordcrpt(userOpt, hexKey);            /* Encrypt or decrypt accordingly */
   
  exit(0);
}


/** Function to ncrpt or dcrpt depending */
void nordcrpt(int userOpt, uint32_t hexKey) {

  int i,j,k;   /* Index vars */
  uint32_t rand32bits, rand8blks[4*maxBuffer];     /* Rand num vars */
  unsigned char ncrptHeader[4] = {0x0e, 0x03, 0x12, 0x00};   /* ncrpt head */
  int charCount = 0;                               /* For chars in and out */
  int charIn[4*maxBuffer];
  unsigned char charOut;

  while (((charIn[charCount] = getchar()) != EOF) && (charCount < 4*maxBuffer)) 
    ++charCount;                                   /* Get & count char buffer */

  for (i=0; i<charCount; ++i) {                    /* Build rand array length of buf */
    if (i % 4 == 0) {                              /* Big If: more rand bytes! */
      rand32bits = rand(&randomContext);           /* Get 32 bits */
      for (j=3; j>=0; --j, ++k)                    /* break 32 bits into 8s */
	rand8blks[i+j] = get8Bits(rand32bits, (k+1)*8);  /* Get next 8 bits */
    }
  }

  if (userOpt == 6) {                              /* If auto-detect... */
    i = 0;
    while (i < 4) {                                /* Try decrypt 1st 4 bytes */
      if (charIn[i] != ncrptHeader[i])             /* Compare to ncrpt header */
	break;   /* Not an ncrpt file */
      ++i;
    }
    if (i == 4)
      userOpt = 2;   /* ncrpt head found, so dcrpt */
    else
      userOpt = 0;   /* no ncrpt head found, so ncrpt */
  }

  if (userOpt == 0)                     /* If encrypt print ncrpt header 1st */
    for (i=0; i<4; ++i)
      putchar(ncrptHeader[i]);

  if (userOpt == 2) {                   /* If decrypt whack header off 1st */
    j = 4;
    charCount -= 4;
  }
  else j = 0;

  for (i=0; i<charCount; ++i, ++j) {
    charOut = charIn[j]^rand8blks[i];    /* ncrpt/dcrpt char! */
    putchar(charOut);
  }
}


/** Function to get next 8 bits from rand32bits */
unsigned get8Bits(uint32_t in32bits, int pos) {
  return ((in32bits >> (pos-8)) & ~(~0 << 8));       /* Isn't it cute? */
}


/** Function to parse args into opts or error out. */
int parseOpts(int argc, char **argv) {

  int i, userOpt, argCount = 1;

  if (argc == 1)                               /* No args, so print help & exit 0 */
    printHelp();

  /* Validate opts & set userOpt */
  if (strncmp(argv[1],"-",1) != 0) {           /* No prefix '-' so maybe hexkey? */
    isValidKey(argv[1]);                       /* AUTO opt if hexkey, else err */
    return 6;                                  /* Attempt auto n-/d-crpt */
  }
  if (strncmp(argv[1],"-",1) == 0) {           /* Does opt string follow? */
    for (i=0; i<6; ++i) {                      /* Check each elt in optionSet */
      if (strcmp(argv[1], optionSet[i]) == 0) {
	userOpt = i;   /* Found one!*/
	if (userOpt % 2 == 1)
	  --userOpt;   /* Change long opt to short */
	break;         /* Stop looking */
      }
      if (i == 5) {    /* Invalid '-[X]' opts escape */
	fprintf(stderr, "Error: option not recognized.\n");
	exit(1);
      }
    }
  }

  /* Help requested, so print it & exit 0 */
  if (userOpt == 4)
    printHelp();

  /* Validate the hexkey arg */
  if ((userOpt == 0) || (userOpt == 2)) {      /* Expect hexkey to follow -e or -d */
    ++argCount;
    if (argCount > argc) {                     /* Err if no argument to -e or -d opts */
      fprintf(stderr, "Error: valid hexkey expected after option -%s.\n", argv[argCount-1]);
      exit(1);
    }
    isValidKey(argv[argCount]);                /* Err if not valid 32 bit hexkey */
  }

  /* Other invalid argument sequences. */
  if (argc > (argCount+1)) {                   /* Too many args not after -h */
    fprintf(stderr, "Error: invalid argument sequence.\n");
    exit(1);
  }
  
  return userOpt;   /* End of the line, so send back whatever's left. */
}


/** Function to validate hexkey */
void isValidKey(char *inStr) {

  int i = 0;
  char tmpChar;

  while ((tmpChar = inStr[i]) != '\0')  {
    if (i > 7) {                           /* Hexkey too long */
      fprintf(stderr, "Error: key %s must be maximum 8 hex digits long.\n", inStr);
      exit(1);
    }
    if (!isxdigit(tmpChar)) {              /* Not valid hex digit */
      fprintf(stderr, "Error: key %s must have only hexidecimal digits.\n", inStr);
      exit(1);
    }
    ++i;
  }
}


/** Function to print out help */
void printHelp(void) {

    printf("\nNCRPT\n\tAutodetecting Cryptography Algorithm\n\tAuthor: Joseph Edwards <jee8th@unm.edu>\n\tUNM CS241 Fall 2011\n");
    printf("\nUSAGE:\n\t$ ncrpt [OPTION [HEXKEY]]\n\nOPTION:\n\t-h\t--help\t\tView this help screen.\n\t-e\t--encrypt\tEncrypt standard input using [HEXKEY], then print to standard out.*\n\t-d\t--decrypt\tDecrypt standard input using [HEXKEY], then print to standard out.*\n\t\t\t\t*[HEXKEY] argument required as final argument.\n\nHEXKEY: Must be 8 digit string of hexidecimal digits (0-F). I.e., f00f00ba. Required as argument for either --encrypt or --decrypt. Required as argument for autodection.\n");
    printf("\nAUTODETECTING: ncrpt will attempt to autodetect whether input is encrypted or plaintext, and decrypt or encrypt respectively. To utilize this feature, provide the hexkey as the only argument. I.e., ncrpt f00f00ba.\n\n");
    exit(0);
}

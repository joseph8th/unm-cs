#include <stdio.h>

int main(void) {

  char ch;

  while ((ch = getchar()) != EOF)
    printf("%x ",ch - 0x20);

  return 0;
}

/* Ackley: A demo program on how to use the ISAAC random number generator
 * 
 * To build and run this 'randDemo' program:
 * (1) Put this file in a directory along with the following 
 *     three files (all available from the project web page):
 *        standard.h
 *        rand.h
 *        rand.c
 * (2) In that directory, compile rand.c as follows:
 *        gcc -Wall -ansi -pedantic -c rand.c
 *     (Note the '-c' argument)
 *     rand.c should compile without warnings or errors.
 * (3) In that directory, compile randDemo.c as follows:
 *        gcc -Wall -ansi -pedantic -c randDemo.c
 *     which should also compile cleanly.  Note: When you
 *     write your own code that works with rand.c, you would 
 *     compile that code in this step, rather than randDemo.c
 * (4) Link the compiled files to make a 'randDemo' program 
 *     as follows:
 *        gcc rand.o randDemo.o -o randDemo
 *     which should complete without error.  
 * (5) Run the program as follows:
 *        ./randDemo
 *     which should print the following output:
 *        2e6d3420 56c39290 ba8d0b85 51c40d48 c5106d94 
 *     and then exit with status 0.
 */

#include <stdlib.h>         /* for exit */
#include <inttypes.h>      /* for uint32_t */
#include <stdio.h>         /* for printf */
#include "rand.h"          /* for randctx, randinit, and rand */

/* The pseudo-random number generation routines need a 'randctx' thing
 * to do their stuff.  So we declare one here.  Then we need to initialize
 * it once we have a seed.
 */

static randctx randomContext;
static void initialize(uint32_t seed) {
  int i;
  for (i = 0; i < RANDSIZ; ++i) 
    randomContext.randrsl[i] = seed;       /* Splat the little seed all through */
  randinit(&randomContext, 1);             /* Initialize the generator */
}

int main() {

  int i;

  uint32_t mySeed = 0xf00f00d0;            /* A random number seed */
  initialize(mySeed);                      /* Seed the generator with it */

  for (i = 0; i < 5; ++i) {           
    uint32_t bits = rand(&randomContext);  /* Each use of rand gives us */
    printf("%08x ", bits);                 /* 32 random bits to use */
  }
  printf("\n");

  exit(0);
}

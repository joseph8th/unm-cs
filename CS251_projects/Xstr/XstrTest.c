#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define XSTREXTRA
#include "Xstr.h"
#include "XstrTest.h"

/* Testing main() for Xstr.c library implementation *
 * By Joseph Edwards for CS241 Fall 2011, UNM       */

int main() {
  printf("XSTR LIBRARY TESTS\n==================\n\n");

  /* TEST XstrMalloc(4), XstrSize(x1), XstrCapacity(x1) */
  x1 = XstrMalloc(4);
  printf("x1(4): size=%d capacity=%d <== Should be (0,16).\n\n", 
	 (unsigned int) XstrSize(x1), (unsigned int) XstrCapacity(x1));

  /* TEST XstrMallocZstring("Blah ") */
  x2 = XstrMallocZstring(blah);
  printf("x2(\"%s\"): size=%d capacity=%d XstrGet(x,size)=", 
	 blah, (int) XstrSize(x2), (int) XstrCapacity(x2));
  /* TEST XstrGet() */
  for (i = 0; i < XstrSize(x2); ++i)
    printf("%c", XstrGet(x2,i));      
  printf("<== Should be (5,16,\"Blah \").\n\n");

  /* TEST XstrReserve() */
  cap = XstrReserve(x2, 24);
  printf("After XstrReserve(x2, 24): size=%d capacity=%d  <== Should be (5,32).\n\n", 
	 (int) XstrSize(x2), (int) XstrCapacity(x2));

  /* TEST XstrAsZstring(), zero-terminated test */
  p = XstrAsZstring(x2);
  printf("p = Zstr(x2) = \"%s\", and 0-term term test: %d  <== Should be (\"Blah \",0).\n\n", p, p[XstrSize(x2)]);  

  /* TEST XstrStore(), XstrAsZstring in printf() */
  x2 = XstrStore(x2, 4, '!');
  printf("p = %s ==> After XstrStore(x2,4,'!'): Zstr = \"%s\"  <== Should be (\"Blah!\").\n\n", p, XstrAsZstring(x2));
  
  /* TEST XstrDup() */
  x3 = XstrDup(x2);
  printf("x3 = XstrDup(x2): Zstr = \"%s\"  <== Should be (\"Blah!\").\n\n", XstrAsZstring(x3)); 

  /* TEST XstrClear() */
  XstrClear(x3);
  printf("x3 after XstrClear(x3): Zstr = \"%s\" XstrGet(x3,0) = %c size = %d  <== Should be (\"\", ?, 0).\n\n", 
	 XstrAsZstring(x3), XstrGet(x3,0), (int) XstrSize(x3)); 

  /* TEST XstrAddChar() */
  x3 = XstrAddChar(x3, '!');
  printf("x3 fter XstrAddChar(x3, '!'): Zstr = \"%s\" size = %d capacity = %d  <== Should be (\"!\", 1, 48).\n\n", 
	 XstrAsZstring(x3), (int) XstrSize(x3), (int) XstrCapacity(x3));

  /* TEST XstrAddZstring() */
  x3 = XstrAddZstring(x3, "Blurp");
  printf("x3 after XstrAddZstring(x3, \"Blurp\"): Zstr = \"%s\" size = %d capacity = %d  <== Should be (\"!Blurp\", 6, 48).\n\n", 
	 XstrAsZstring(x3), (int) XstrSize(x3), (int) XstrCapacity(x3));

  /* TEST XstrAddXstr() */
  x2 = XstrAddXstr(x2, x2);
  printf("x2 after XstrAddXstr(x2, x3): Zstr = \"%s\" size = %d capacity = %d  <== Should be (\"Blah!Blah!\", 10, 32).\n\n", 
	 XstrAsZstring(x2), (int) XstrSize(x2), (int) XstrCapacity(x2));
  x2 = XstrAddXstr(x2, x3);
  printf("x2 after XstrAddXstr(x2, x3): Zstr = \"%s\" size = %d capacity = %d  <== Should be (\"Blah!Blah!!Blurp\", 16, 32)\n\n", 
	 XstrAsZstring(x2), (int) XstrSize(x2), (int) XstrCapacity(x2));

  /* The null-byte stuck in the middle of an Xstr test */
  x3 = XstrAddZstring(XstrAddChar(x3, '\0'), "Poo.");
  printf("x3 after null-byte test: Zstr = \"%s\" size = %d capacity = %d  <== Should be (\"!Blurp\", 11, 48)\n\n", 
	 XstrAsZstring(x3), (int) XstrSize(x3), (int) XstrCapacity(x3));

  /* The 8-bit clean test from the man page */
  x = XstrMalloc(0);
  XstrAddZstring(x,"foo");
  XstrAddChar(x, '\0' );
  XstrAddZstring(x,"bar");
  printf("8-bit clean test: %d %d %c  <== Should be (3 7 b)\n\n",
	 (int) strlen(XstrAsZstring(x)),
	 (int) XstrSize(x),
	 XstrGet(x,4));

  /************************
   * TEST Xstr EXTRA fcns */
  printf("XSTR EXTRA FUNCTION TESTS:\n==========================\n\n");

  /* TEST XstrCmp() */
  XstrClear(x2);
  x1 = XstrAddZstring(x1, "Blah");
  x2 = XstrAddZstring(x2, "Blah!");

  printf("XstrCmp():\n(x1 ? x2): %s ", XstrAsZstring(x1));
  i = XstrCmp(x1,x2);
  if (i < 0) printf("< ");
  if (i > 0) printf("> ");
  if (i == 0) printf("== ");
  printf("%s (%d)  <== Should be (Blah < Blah!)\n", XstrAsZstring(x2), i);

  printf("(x2 ? x1): %s ", XstrAsZstring(x2));
  i = XstrCmp(x2,x1);
  if (i < 0) printf("< ");
  if (i > 0) printf("> ");
  if (i == 0) printf("== ");
  printf("%s (%d)  <== Should be (Blah! > Blah)\n", XstrAsZstring(x1), i);

  printf("(x1 ? x1): %s ", XstrAsZstring(x1));
  i = XstrCmp(x1,x1);
  if (i < 0) printf("< ");
  if (i > 0) printf("> ");
  if (i == 0) printf("== ");
  printf("%s (%d)  <== Should be (Blah == Blah)\n", XstrAsZstring(x1), i);

  printf("\n");

  /* TEST XstrAddInt() */
  x1 = XstrAddInt(x1,20,10);
  printf("x1 after XstrAddInt(x1,20,10): Zstr = \"%s\"  <== Should be (Blah20)\n", XstrAsZstring(x1));
  x1 = XstrAddInt(x1,-20,10);
  printf("x1 after XstrAddInt(x1,-20,10): Zstr = \"%s\"  <== Should be (Blah20-20)\n", XstrAsZstring(x1));
  x1 = XstrAddInt(x1,4,2);
  printf("x1 after XstrAddInt(x1,4,2): Zstr = \"%s\"  <== Should be (Blah20-20100)\n", XstrAsZstring(x1));
  x1 = XstrAddInt(x1,-3840,16);
  printf("x1 after XstrAddInt(x1,-3840,16): Zstr = \"%s\"  <== Should be (Blah20-20100-F00)\n", XstrAsZstring(x1));
  x1 = XstrAddInt(x1,-3840,17);
  printf("x1 after XstrAddInt(x1,-3840,17): Zstr = \"%s\"  <== Should be UNCHANGED, since 17 out of range\n", XstrAsZstring(x1));

  printf("\n");

  /* TEST XstrAddUInt() -- almost straight from the man page */
  XstrClear(x);
  XstrAddZstring(x, "Go");
  XstrAddChar(XstrAddUInt(x, 123, 10), '/'); /* x has "Go123/" */
  XstrAddChar(XstrAddUInt(x, 0XDEADD00D, 16), '/'); /* x has "Go123/deadd00d/" */
  XstrAddChar(XstrAddUInt(x, -1, 8), '/'); /* x has "Go123/deadd00d/37777777777/" */
  printf("x after XstrAddUInt() test from man page: %s  <== Should be (\"Go123/deadd00d/37777777777/\")\n", XstrAsZstring(x));

  printf("\n");

  /* TEST XstrSubstring() -- creates NEW Xstr */
  x5 = XstrSubstring(x, 6, 14);
  printf("x5 new from XstrSubstring(x, 6, 14): %s  <== Should be (deadd00d)\n\n", XstrAsZstring(x5));

  /* TEST XstrFindXstr() */
  XstrClear(x2);
  x2 = XstrAddZstring(x2, "AD0");
  i = XstrFindXstr(x5, x2, 2);
  printf("XstrFindXstr():\nLocation of \"%s\" in x5: %d\n", XstrAsZstring(x2), i);
  XstrStore(x2, 0, 'D');
  i = XstrFindXstr(x5, x2, 2);
  printf("Location of \"%s\" in x5: %d\n\n", XstrAsZstring(x2), i);

  /* TEST XstrReplaceRange() */
  XstrStore(x2,0,'/');
  XstrStore(x2,2,'/');
  x5 = XstrReplaceRange(x5,3,5,x2);
  printf("x5 after XstrReplaceRange(x5,3,5,x2): x2 = \"%s\" in Zstr = %s  <== Should be \"DEA/D/00D\"\n", XstrAsZstring(x2), XstrAsZstring(x5));

  printf("\n");

  /***********************************************
   * XstrFree everything, plus XstrFree() tests */

  printf("x freed:\n");
  XstrFree(x);
  printf("x1 freed:\n");
  XstrFree(x1);
  printf("x2 freed:\n");
  XstrFree(x2);
  printf("x3 freed:\n");
  XstrFree(x3);
  printf("x3 freed again?\n");
  XstrFree(x3);      /* Should print freed already error every time */
  printf("NULL Xstr free-able?\n");
  XstrFree(NULL);    /* Should print NULL error */
  printf("x4 free-able uninitialized?\n");
  XstrFree(x4);      /* Should print NULL error when uninit Xstr fed to XstrFree() */
  printf("x5 freed (after XstrFree errors):\n");
  XstrFree(x5);

  printf("\n");
  return 0;
}

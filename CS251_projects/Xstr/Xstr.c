#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define XSTREXTRA
#include "Xstr.h"

/***************************************************
 * Xstr library by Joseph Edwards <jee8th@unm.edu> *
 * CS241 Fall 2011 - University of New Mexico      *
 ***************************************************/

#define MAX_XSTR 65535  /* Deficient limit on # of free-able Xstrs */

/* Define the struct xstr to which Xstr points */
struct xstr {
  size_t size;               /* Size of string of chars */
  size_t capacity;           /* Capacity of char array  */
  int flag;                  /* uninit = 0, init = 1 */
  unsigned char * chPtr;              /* Index to chPtr in chPtrAry */
};

/* Define an char array for XstrAddInt-type fcns */
static char baseNumChAry[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

/********************************
 * Static fcns to handle errors */

static void defOoMHandler(size_t amount) {
  fprintf(stderr, "Error: Can't allocate %d bytes. Exiting.", (int) amount);
  exit(100);
}
XstrOutOfMemoryHandler defHandler = &defOoMHandler;

static void exitNoMemErr(size_t amount) {
  defHandler(amount);
  /*  fprintf(stderr, "Error: Insufficient memory.\n");
      exit(100); */
}
static void printErr(char *msg) {
  fprintf(stderr, "Error: %s.\n", msg);
  return;
}

/*************************************
 * Static stuff to track freed Xstrs */

static unsigned long freedList[MAX_XSTR];
static size_t freedCount;

/* Fcn to check if an Xstr has been freed by XstrFree() */
static int isFreed(Xstr x) {
  unsigned long xAddress = (unsigned long) x;
  int i;
  if (freedCount == 0)   /* nothing prev. freed, so return 0 */
    return 0;
  for (i = 0; i < freedCount; ++i)
    if (freedList[i] == xAddress )
	return 1;   /* prev. freed, so return 1 for true */
  return 0;   /* never freed */
}

/**********************************************************
 * Static external stuff to manage char arrays & pointers */

/* Static fcn to construct an Xstr */
static Xstr makeXstr(size_t size, size_t capacity) {
  Xstr x;
  if (freedCount == MAX_XSTR)
    exitNoMemErr(capacity);
  capacity += 16 - (capacity % 16);
  x = (Xstr) malloc(sizeof(struct xstr));
  if (x == NULL)
    exitNoMemErr(capacity);
  x->chPtr = (unsigned char *) malloc(capacity * sizeof(unsigned char *));
  if (x->chPtr == NULL)
    exitNoMemErr(capacity);
  x->size = size;
  x->capacity = capacity;
  x->flag = 1;
  return x;
}

/* Fcn to realloc() a char array owned by given Xstr */
static Xstr XstrRealloc(Xstr x, size_t capIn) {    
  unsigned char *chTmp;
  size_t capacity = capIn + 16 - (capIn % 16);   /* cap is div by 16 */
  chTmp = (unsigned char *) realloc( x->chPtr, capacity * sizeof(unsigned char *) );
  if (chTmp == NULL)
    exitNoMemErr(capacity);
  x->capacity = capacity;
  x->chPtr = chTmp;
  return x;
}

/* Fcn to add one Xstr to another */
static Xstr XstrCat(Xstr x, unsigned char *z, size_t zLen) {
  int i;
  if ((zLen + x->size) > x->capacity)         /* then need to realloc the chAry */  
    x = XstrRealloc(x, 2*(zLen + x->size));
  for (i = 0; i < zLen; ++i) {                /* tack the z string onto the end */
    x->chPtr[x->size] = z[i];
    ++x->size;
  }
  return x;
}

/**************************************************
 * Xstr Library non-static functions              *
 **************************************************
 * Fcns for allocation and destruction for Xstr's */

/* Malloc an empty Xstr */
Xstr XstrMalloc(size_t initialCapacity) {
  return makeXstr(0,initialCapacity);
}

/* Malloc an Xstr initialized by a z string */
Xstr XstrMallocZstring(char *z) {
  size_t len = strlen(z);
  size_t capacity = (size_t) 2*len;
  Xstr x = makeXstr(len,capacity); 
  memcpy(x->chPtr, z, x->size);
  return x;
}

/* Free an Xstr */
void XstrFree(Xstr x) {
  if (x == (Xstr) NULL) {
    printErr("Xstr must not be NULL pointer");
    return;
  }
  if (isFreed(x)) {
    printErr("Xstr must not be previously freed");
    return;
  }
  if (x->flag != 1) {
    printErr("Xstr must be made by Xstr library function");
    return;
  }
  /* OK to free x ... */
  freedList[freedCount] = (unsigned long) x;   /* Add address & index to freedList */
  free(x->chPtr);
  x->chPtr = NULL; 
  free(x);
  x = NULL;      /* Makes the flag kind of moot, but J.I.C... */
  ++freedCount;  /* keep track of how many freed */
  return;
}

/**********************************************
 * Accessor fcns to struct xstr member values */

/* Return the size of the Xstr */
size_t XstrSize(Xstr x) {
  return x->size;
}

/* Return the capacity of the Xstr */
size_t XstrCapacity(Xstr x) {
  return x->capacity;
}

/* Return a given element of the Xstr, or -1 */
int XstrGet(Xstr x, size_t index) {
  if (index < x->size) {
    return x->chPtr[index];
  }
  return -1;   /* index out of bounds */
}

/* Return an Xstr as a z string */
char * XstrAsZstring(Xstr x) {
  x->chPtr[x->size] = '\0';      /* don't forget to zero-terminate! */
  return (char *) x->chPtr;
}

/* Return a duplicate of a given Xstr */
Xstr XstrDup(Xstr x) {
  Xstr xOut = makeXstr(x->size, x->capacity);   /* need a whole new Xstr */
  memcpy(xOut->chPtr, x->chPtr, x->size);       /* copy it all into the chAry */
  return xOut;
}

/*********************************
 * Mutator fcns to alter an Xstr */

/* Reserve a minimum capacity for a given Xstr */
size_t XstrReserve(Xstr x, size_t minCapacity) { 
  if (x->capacity < minCapacity) {
    x = XstrRealloc(x, minCapacity);   /* an increase, so that's ok */
  }
  return x->capacity;  /* else no change */
}

/* Clear a given Xstr (setting size to 0) */
void XstrClear(Xstr x) {
  x->size = 0;   /* heheh */
  return;
}

/* Store a given char in a given Xstr at a given index */
Xstr XstrStore(Xstr x, size_t index, unsigned char toStore) {
  if (index > x->size)
    return NULL;                  /* index out of bounds, so return NULL */
  x->chPtr[index] = toStore;   
  return x;    /* else change */
}

/* Add a given char to the end of an Xstr */
Xstr XstrAddChar(Xstr x, unsigned char ch) {
  if (x->size == x->capacity)              /* only need to add 1 char, if == ... */
    x = XstrRealloc(x, 2 * x->capacity);   /* realloc the chAry */
  x->chPtr[x->size] = ch;                  /* tack the char on */
  ++x->size;
  return x;
}

/* Add a given z string to the end of an Xstr */
Xstr XstrAddZstring(Xstr x, char *z) {
  return XstrCat(x, (unsigned char *) z, strlen(z));   /* tack the z string on */
}

/* Add a given Xstr to the end of an Xstr */
Xstr XstrAddXstr(Xstr x, Xstr other) {
  return XstrCat(x, other->chPtr, other->size);        /* tack the Xstr on */
}

/************************
 * Extra Xstr functions */

XstrOutOfMemoryHandler XstrSetOutOfMemoryHandler(XstrOutOfMemoryHandler handler) {
  if (handler != NULL)
    defHandler = handler;
  return defHandler;
}

/* Return <0, ==0, >0 depending if x1 < x2, x1==x2, or x1 > x2 */
int XstrCmp(Xstr x1, Xstr x2) {
  int i, minLen, lenDif;
  lenDif = x1->size - x2->size;
  minLen = (lenDif <= 0) ? x1->size : x2->size;
  for (i = 0; i < minLen; ++i)
    if ((x1->chPtr[i] - x2->chPtr[i]) != 0)        /* not a perfect match */
      return (int) (x1->chPtr[i] - x2->chPtr[i]);  /* return most recent diff */
  if (lenDif < 0)
    return (int) (0 - x2->chPtr[i]);             /* match so far, but x1 shorter */
  if (lenDif > 0)
    return (int) (x1->chPtr[i]);                 /* match so far, but x1 longer */
  return 0;                                        /* perfect match */
}

/* Append an integer formatted in the given base (2-16) */
Xstr XstrAddInt(Xstr x, int number, int base) {
  int i = 0,
    sign = 0,
    numInBaseLen = 0,
    remainder = 0,
    quotient = number;
  char *numZstr;

  if ((base < 2) || (base > 16))
    return x;              /* base out of range, so leave x unchanged */

  while (quotient) {       /* sadly count digits */
    quotient /= base;
    ++numInBaseLen;
  }
  if (number < 0) {         /* if number negative, need room for a leading minus sign */ 
    sign = 1;
    number = abs(number);
  }
  numInBaseLen += sign;                /* 1st make the length of the symbol string long enough */ 
  numZstr = (char *) malloc((numInBaseLen)*sizeof(char *));    /* malloc it! */
  quotient = number;                        /* sadly go through digits again, with a L'il Edian twist */
  for (i = numInBaseLen - 1; i >= sign; --i) {
    remainder = quotient % base;            /* get the int version of remainder */
    numZstr[i] = baseNumChAry[remainder];   /* use remainder as index & assign to next last spot */
    quotient /= base;
  }
  if (sign == 1) numZstr[0] = '-';                            /* this case needs leading minus */
  x = XstrCat(x, (unsigned char *)numZstr, numInBaseLen);     /* tack the numZstr onto x */
  free(numZstr);                                              /* free numZstr */

  return x;
}

/* Append an unsigned integer formatted in the given base (2-16) */
Xstr XstrAddUInt(Xstr x, unsigned int number, int base) {
  int i = 0, 
    sign = 0, 
    numInBaseLen = 0;
  unsigned int remainder = 0, quotient = number;
  unsigned char *numZstr;

  if ((base < 2) || (base > 16))
    return x;              /* base out of range, so leave x unchanged */

  while (quotient) {       /* count digits */
    quotient /= base;
    ++numInBaseLen;
  }
  numZstr = (unsigned char *) malloc(numInBaseLen * sizeof(unsigned char *));    /* malloc it! */
  quotient = number;                        /* sadly go through digits again, with a L'il Edian twist */
  for (i = numInBaseLen - 1; i >= sign; --i) {
    remainder = quotient % base;            /* get the int version of remainder */
    numZstr[i] = baseNumChAry[remainder];   /* use remainder as index & assign to next last spot */
    quotient /= base;
  }
  x = XstrCat(x, (unsigned char *)numZstr, numInBaseLen);     /* tack the numZstr onto x */
  free(numZstr);                                              /* free numZstr */

  return x;
}

/* Return a new Xstr containing a substring of a given Xstr */
Xstr XstrSubstring(Xstr x, int begin, int end) {
  int i, len;
  Xstr xSubStr;

  if (begin < 0) begin = 0;
  if (end >= x->size) end = x->size;
  len = end - begin;
  if (len < 0)
    return makeXstr(0, 1);   /* if end < begin, returns an empty Xstr w/capacity=default */

  xSubStr = makeXstr( (size_t) len, (size_t) 2*len );         /* else... make a new Xstr */
  for (i = 0; i < len; ++i)
    xSubStr->chPtr[i] = x->chPtr[i + begin];                  /* copy over the part we want */

  return xSubStr;
}

/* Find the position of the first/next occurrence of a Xstr inside another */
int XstrFindXstr(Xstr inThis, Xstr findThis, int startingFrom) {
  int i, j = 1, 
    len;                      /* i = search index; j = find index; len = length of string in which to search for 1st char match */

  if (startingFrom < 0) startingFrom = 0;
  len = inThis->size - findThis->size;
  if (len < 0) 
    return -1;

  for (i = startingFrom; i <= len; ++i) {
    if (inThis->chPtr[i] == findThis->chPtr[0]) {
      for (j = 1; j < findThis->size; ++j) {                       /* found 1st char, so see if the next len-# chars match ... */
	if (inThis->chPtr[i + j] != findThis->chPtr[j])
	  break;
	if (j == findThis->size - 1)
	  return i;
      }
    }
  }
  return -1;
}

/* Replace a range of a Xstr with the contents of another Xstr */
Xstr XstrReplaceRange(Xstr inThis, int fromHere, int toHere, Xstr replaceWithThis) {
  int i, newLen, afterLen,
    lenToReplace, aftIndex = 0;
  unsigned char *inThisBefore;

  if (fromHere < 0) fromHere = 0;
  if (toHere >= inThis->size) toHere = inThis->size;
  lenToReplace = toHere - fromHere;
  if (lenToReplace < 0)
    return inThis;                                            /* if indexes backwards, returns inThis unchanged */
  
  newLen = inThis->size + replaceWithThis->size - lenToReplace;
  afterLen = newLen - toHere;
  inThisBefore = (unsigned char *) malloc((2*newLen) * sizeof(unsigned char));

  for (i = 0; i < fromHere; ++i)                         /* replace the spots already in inThis */
    inThisBefore[i] = inThis->chPtr[i];

  for (i = fromHere; i < (fromHere + lenToReplace); ++i, ++aftIndex)
    inThisBefore[i] = replaceWithThis->chPtr[aftIndex];

  for (i = fromHere + lenToReplace; i < (fromHere + replaceWithThis->size); ++i, ++aftIndex)
    inThisBefore[i] = replaceWithThis->chPtr[aftIndex];
 
  aftIndex = toHere;
  for (i = fromHere + replaceWithThis->size; i < newLen; ++i, ++aftIndex)
    inThisBefore[i] = inThis->chPtr[aftIndex];
  
  free(inThis->chPtr);
  inThis->chPtr = inThisBefore;
  inThis->size = newLen;
  inThis->capacity = 2*newLen;

  return inThis;

}

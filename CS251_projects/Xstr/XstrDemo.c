#include <stdio.h>
#include "Xstr.h"

int main() {
  Xstr x1 = XstrMallocZstring("[");  /* make an xstr */
  int i;
  for (i = 0; i < 100; ++i) 
    XstrAddChar(x1,'A'+i%26);        /* stick on some alphabets */
  XstrAddZstring(x1, "]");
  printf("%s", XstrAsZstring(x1));   /* take a look at it */
  XstrFree(x1);                      /* toss it */
  return 0;
}

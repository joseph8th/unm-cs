#ifndef XSTR_H
#define XSTR_H

/*
 * Xstr.h
 * David Ackley for UNM CS241 Fall 2011
 */

#include <stddef.h>

/* This is the header file for the Xstr expandable string library, to 
 * be used for program 2 in UNM CS241 Fall 2011.           
 *                                                           
 * IMPORTANT NOTES:                                                    
 *                                                           
 * (0) This file must be used AS-IS, UNALTERED in any way.  Your code
 * is placed in Xstr.c, which will be compiled and tested against an
 * unaltered Xstr.h; if your Xstr.c doesn't work correctly under those
 * conditions, IT DOESN'T WORK.
 *                                                           
 * (1) See the Xstr man page for complete documentation!  Your Xstr.c
 * implementation MUST be in full accordance with the man page
 * documentation, NOT MERELY the brief comments in this header!
 *
 */

/* The exact definition of 'struct xstr' is implementation-specific --
 * up to you, and defined by you in your Xstr.c.  
 *
 * All external uses operate via a pointer to a struct xstr, so this
 * typedef defines 'Xstr' to be such a pointer.
 *
 * Note that, by this typedef, a 'Xstr' is ALREADY a pointer.  So Xstr
 * users will make a declaration like:
 *
 *   Xstr p = XstrMalloc(10);
 *
 * and the type of p is 'pointer to struct xstr', even though there's
 * no explicit '*' in the declaration.
 */
typedef struct xstr * Xstr;

/* This typedef can be ignored unless the optional functions are implemented */
typedef void (*XstrOutOfMemoryHandler)(size_t);

/* Creation */
extern Xstr XstrMalloc(size_t initialCapacity) ;
extern Xstr XstrMallocZstring(char * z) ;

/* Destruction */
extern void XstrFree(Xstr x) ;

/* Access current length; O(1) in size  */
extern size_t XstrSize(Xstr x) ;

/* Access current capacity; O(1) in size  */
extern size_t XstrCapacity(Xstr x) ;

/* Access a particular char; O(1) in size */
extern int XstrGet(Xstr x, size_t index) ;

/* Update a particular char; O(1) in size */
extern Xstr XstrStore(Xstr x, size_t index, unsigned char toStore ) ;

/* Ensure a given minimum capacity */
extern size_t XstrReserve(Xstr x, size_t minCapacity) ; 

/* Clear; O(1) in size  */
extern void XstrClear(Xstr x) ;

/* Duplication; O(n) in size  */
extern Xstr XstrDup(Xstr x) ;

/* Access as zstring; O(1) in size  */
extern char * XstrAsZstring(Xstr x) ;

/* Append a single char; amortized O(1) in size */
extern Xstr XstrAddChar(Xstr x, unsigned char ch) ;

/* Append a zstring; O(strlen(zstring)) */
extern Xstr XstrAddZstring(Xstr x, char * z) ;

/* Append a copy of an existing Xstr; O(XstrSize(other)) */
extern Xstr XstrAddXstr(Xstr x, Xstr other) ;

#ifdef XSTREXTRA

extern XstrOutOfMemoryHandler XstrSetOutOfMemoryHandler(XstrOutOfMemoryHandler handler) ;

/* Return <0, ==0, >0 depending if x1 < x2, x1==x2, or x1 > x2 */
extern int XstrCmp(Xstr x1, Xstr x2) ;

/* Append an integer formatted in the given base (2-16) */
extern Xstr XstrAddInt(Xstr x, int number, int base) ;

/* Append an unsigned integer formatted in the given base (2-16) */
extern Xstr XstrAddUInt(Xstr x, unsigned int number, int base) ;

/* Return a new Xstr containing a substring of a given Xstr */
extern Xstr XstrSubstring(Xstr x, int begin, int end) ;

/* Find the position of the first/next occurrence of a Xstr inside another */
extern int XstrFindXstr(Xstr inThis, Xstr findThis, int startingFrom) ;

/* Replace a range of a Xstr with the contents of another Xstr */
extern Xstr XstrReplaceRange(Xstr inThis, int fromHere, int toHere, Xstr replaceWithThis) ;

#endif

#endif /* XSTR_H */

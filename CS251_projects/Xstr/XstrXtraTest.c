#include <stdio.h>
#include <stdlib.h>
#include "Xstr.h"
#include "XstrTest.h"

/* My temporary testing main()
 * Compiled against Xstr.c by
 * gcc -g -Wall -ansi -pedantic XstrTest.c Xstr.c -o XstrTest */

int main() {

  /* TEST XstrMalloc(4), XstrSize(x1), XstrCapacity(x1) */
  x1 = XstrMalloc(4);
  printf("x1(4): size=%d capacity=%d\n", (unsigned int) XstrSize(x1), (unsigned int) XstrCapacity(x1));

  /* TEST XstrMallocZstring("Blah ") */
  x2 = XstrMallocZstring(blah);
  printf("x2(\"%s\"): size=%d capacity=%d XstrGet(x,size)=", blah, (int) XstrSize(x2), (int) XstrCapacity(x2));

  /* TEST XstrGet() */
  for (i = 0; i < XstrSize(x2); ++i)
    printf("%c", XstrGet(x2,i));      
  printf("\n");

  /* TEST XstrReserve() */
  cap = XstrReserve(x2, 24);
  printf("After XstrReserve(x2, 24): size=%d capacity=%d\n", (int) XstrSize(x2), (int) XstrCapacity(x2));

  /* TEST XstrAsZstring(), zero-terminated test */
  p = XstrAsZstring(x2);
  printf("p = Zstr(x2) = %s + 0-term term test: %d\n", p, p[XstrSize(x2)]);  

  /* TEST XstrStore(), XstrAsZstring in printf() */
  x2 = XstrStore(x2, 4, '!');
  printf("p = %s ==> After XstrStore(x2,4,'!'): Zstr = \"%s\"\n", p, XstrAsZstring(x2));
  
  /* TEST XstrDup() */
  x3 = XstrDup(x2);
  printf("x3 = XstrDup(x2): Zstr = \"%s\"\n", XstrAsZstring(x3)); 

  /* TEST XstrClear() */
  XstrClear(x3);
  printf("x3 after XstrClear(x3): Zstr = \"%s\" XstrGet(x3,0) = %c size = %d\n", XstrAsZstring(x3), XstrGet(x3,0), (int) XstrSize(x3)); 

  /* TEST XstrAddChar() */
  x3 = XstrAddChar(x3, '!');
  printf("x3 fter XstrAddChar(x3, '!'): Zstr = \"%s\" size = %d capacity = %d\n", XstrAsZstring(x3), (int) XstrSize(x3), (int) XstrCapacity(x3));

  /* TEST XstrAddZstring() */
  x3 = XstrAddZstring(x3, "Blurp");
  printf("x3 after XstrAddZstring(x3, \"Blurp\"): Zstr = \"%s\" size = %d capacity = %d\n", XstrAsZstring(x3), (int) XstrSize(x3), (int) XstrCapacity(x3));

  /* TEST XstrAddXstr() */
  x2 = XstrAddXstr(x2, x2);
  printf("x2 after XstrAddXstr(x2, x3): Zstr = \"%s\" size = %d capacity = %d\n", XstrAsZstring(x2), (int) XstrSize(x2), (int) XstrCapacity(x2));
  x2 = XstrAddXstr(x2, x3);
  printf("x2 after XstrAddXstr(x2, x3): Zstr = \"%s\" size = %d capacity = %d\n", XstrAsZstring(x2), (int) XstrSize(x2), (int) XstrCapacity(x2));

  /* The null-byte test */
  x3 = XstrAddZstring(XstrAddChar(x3, '\0'), "Poo.");
  printf("x3 after null-byte test: Zstr = \"%s\" size = %d capacity = %d\n", XstrAsZstring(x3), (int) XstrSize(x3), (int) XstrCapacity(x3));

  XstrClear(x2);
  x1 = XstrAddZstring(x1, "Blah");
  x2 = XstrAddZstring(x2, "Blah!");
  printf("(x1 ? x2): %s ", XstrAsZstring(x1));
  i = XstrCmp(x1,x2);
  if (i < 0) printf("< ");
  if (i > 0) printf("> ");
  if (i == 0) printf("== ");
  printf("%s\n", XstrAsZstring(x2));


  /* Free stuff left over, plus XstrFree() tests */
  printf("x1 freed.\n");
  XstrFree(x1);
  printf("x2 freed:\n");
  XstrFree(x2);
  printf("x3 freed:\n");
  XstrFree(x3);
  printf("x3 freed again?\n");
  XstrFree(x3);      /* Should print freed already error every time */
  printf("NULL freed?\n");
  XstrFree(NULL);    /* Should print NULL error */
  printf("x4 freed:\n");
  XstrFree(x4);      /* Should print not made by library error when uninit Xstr fed to XstrFree() */

  return 0;
}

#define XSTREXTRA

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Xstr.h"

/***************************************************
 * Xstr library by Joseph Edwards <jee8th@unm.edu> *
 * CS241 Fall 2011 - University of New Mexico      *
 ***************************************************/

#define MAX_XSTR 65535  /* Deficient limit on # of free-able Xstrs */

/* Define the struct xstr to which Xstr points */
struct xstr {
  size_t size;               /* Size of string of chars */
  size_t capacity;           /* Capacity of char array  */
  int flag;                  /* uninit = 0, init = 1 */
  unsigned char * chPtr;              /* Index to chPtr in chPtrAry */
};

/********************************
 * Static fcns to handle errors */

static void exitNoMemErr(void) {
  fprintf(stderr, "Error: Insufficient memory.\n");
  exit(100);
}
static void printErr(char *msg) {
  fprintf(stderr, "Error: %s.\n", msg);
  return;
}

/*************************************
 * Static stuff to track freed Xstrs */

static unsigned long freedList[MAX_XSTR];
static size_t freedCount;

/* Fcn to check if an Xstr has been freed by XstrFree() */
static int isFreed(Xstr x) {
  unsigned long xAddress = (unsigned long) x;
  int i;
  if (freedCount == 0)   /* nothing prev. freed, so return 0 */
    return 0;
  for (i = 0; i < freedCount; ++i)
    if (freedList[i] == xAddress )
	return 1;   /* prev. freed, so return 1 for true */
  return 0;   /* never freed */
}

/**********************************************************
 * Static external stuff to manage char arrays & pointers */

/* Static fcn to construct an Xstr */
static Xstr makeXstr(size_t size, size_t capacity) {
  Xstr x;
  if (freedCount == MAX_XSTR)
    exitNoMemErr();
  capacity += 16 - (capacity % 16);
  x = (Xstr) malloc(sizeof(struct xstr));
  if (x == NULL)
    exitNoMemErr();
  x->chPtr = (unsigned char *) malloc(capacity * sizeof(unsigned char *));
  if (x->chPtr == NULL)
    exitNoMemErr();
  x->size = size;
  x->capacity = capacity;
  x->flag = 1;
  return x;
}

/* Fcn to realloc() a char array owned by given Xstr */
static Xstr XstrRealloc(Xstr x, size_t capIn) {    
  unsigned char *chTmp;
  size_t capacity = capIn + 16 - (capIn % 16);   /* cap is div by 16 */
  chTmp = (unsigned char *) realloc( x->chPtr, capacity * sizeof(unsigned char *) );
  if (chTmp == NULL)
    exitNoMemErr();
  x->capacity = capacity;
  x->chPtr = chTmp;
  return x;
}

/* Fcn to add one Xstr to another */
static Xstr XstrCat(Xstr x, unsigned char *z, size_t zLen) {
  int i;
  if ((zLen + x->size) > x->capacity)         /* then need to realloc the chAry */  
    x = XstrRealloc(x, 2*(zLen + x->size));
  for (i = 0; i < zLen; ++i) {                /* tack the z string onto the end */
    x->chPtr[x->size] = z[i];
    ++x->size;
  }
  return x;
}

/**************************************************
 * Fcns for allocation and destruction for Xstr's */

/* Malloc an empty Xstr */
Xstr XstrMalloc(size_t initialCapacity) {
  return makeXstr(0,initialCapacity);
}

/* Malloc an Xstr initialized by a z string */
Xstr XstrMallocZstring(char *z) {
  size_t len = strlen(z);
  size_t capacity = (size_t) 2*len;
  Xstr x = makeXstr(len,capacity); 
  memcpy(x->chPtr, z, x->size);
  return x;
}

/* Free an Xstr */
void XstrFree(Xstr x) {
  if (x == (Xstr) NULL) {
    printErr("Xstr must not be NULL pointer");
    return;
  }
  if (isFreed(x)) {
    printErr("Xstr must not be previously freed");
    return;
  }
  if (x->flag != 1) {
    printErr("Xstr must be made by Xstr library function");
    return;
  }
  /* OK to free x ... */
  freedList[freedCount] = (unsigned long) x;   /* Add address & index to freedList */
  free(x->chPtr);
  x->chPtr = NULL; 
  free(x);
  x = NULL;      /* Makes the flag kind of moot, but J.I.C... */
  ++freedCount;  /* keep track of how many freed */
  return;
}

/**********************************************
 * Accessor fcns to struct xstr member values */

/* Return the size of the Xstr */
size_t XstrSize(Xstr x) {
  return x->size;
}

/* Return the capacity of the Xstr */
size_t XstrCapacity(Xstr x) {
  return x->capacity;
}

/* Return a given element of the Xstr, or -1 */
int XstrGet(Xstr x, size_t index) {
  if (index < x->size) {
    return x->chPtr[index];
  }
  return -1;   /* index out of bounds */
}

/* Return an Xstr as a z string */
char * XstrAsZstring(Xstr x) {
  x->chPtr[x->size] = '\0';      /* don't forget to zero-terminate! */
  return (char *) x->chPtr;
}

/* Return a duplicate of a given Xstr */
Xstr XstrDup(Xstr x) {
  Xstr xOut = makeXstr(x->size, x->capacity);   /* need a whole new Xstr */
  memcpy(xOut->chPtr, x->chPtr, x->size);       /* copy it all into the chAry */
  return xOut;
}

/*********************************
 * Mutator fcns to alter an Xstr */

/* Reserve a minimum capacity for a given Xstr */
size_t XstrReserve(Xstr x, size_t minCapacity) { 
  if (x->capacity < minCapacity) {
    x = XstrRealloc(x, minCapacity);   /* an increase, so that's ok */
  }
  return x->capacity;  /* else no change */
}

/* Clear a given Xstr (setting size to 0) */
void XstrClear(Xstr x) {
  x->size = 0;   /* heheh */
  return;
}

/* Store a given char in a given Xstr at a given index */
Xstr XstrStore(Xstr x, size_t index, unsigned char toStore) {
  if (index > x->size)
    return x;                  /* index out of bounds, so no change */
  x->chPtr[index] = toStore;   
  return x;    /* else change */
}

/* Add a given char to the end of an Xstr */
Xstr XstrAddChar(Xstr x, unsigned char ch) {
  if (x->size == x->capacity)              /* only need to add 1 char, if == ... */
    x = XstrRealloc(x, 2 * x->capacity);   /* realloc the chAry */
  x->chPtr[x->size] = ch;                  /* tack the char on */
  ++x->size;
  return x;
}

/* Add a given z string to the end of an Xstr */
Xstr XstrAddZstring(Xstr x, char *z) {
  return XstrCat(x, (unsigned char *) z, strlen(z));   /* tack the z string on */
}

/* Add a given Xstr to the end of an Xstr */
Xstr XstrAddXstr(Xstr x, Xstr other) {
  return XstrCat(x, other->chPtr, other->size);        /* tack the Xstr on */
}

/************************
 * Extra Xstr functions */
/*
XstrOutOfMemoryHandler XstrSetOutOfMemoryHandler(XstrOutOfMemoryHandler handler) {
  
}*/

/* Return <0, ==0, >0 depending if x1 < x2, x1==x2, or x1 > x2 */
int XstrCmp(Xstr x1, Xstr x2) {
  int i, minLen, lenDif;
  lenDif = x1->size - x2->size;
  minLen = (lenDif <= 0) ? x1->size : x2->size;
  for (i = 0; i < minLen; ++i)
    if ((x1->chPtr[i] - x2->chPtr[i]) != 0)        /* not a perfect match */
      return (int) (x1->chPtr[i] - x2->chPtr[i]);  /* return most recent diff */
  if (lenDif < 0)
    return (int) (0 - x2->chPtr[++i]);             /* match so far, but x1 shorter */
  if (lenDif > 0)
    return (int) (x1->chPtr[++i]);                 /* match so far, but x2 longer */
  return 0;                                        /* perfect match */
}

/* Append an integer formatted in the given base (2-16) */
Xstr XstrAddInt(Xstr x, int number, int base) {
  int numOut = (int) strtol(sprintf("%d", number), (char**)NULL, base);
  char ch[] = sprintf("%
}

/* Append an unsigned integer formatted in the given base (2-16) */
Xstr XstrAddUInt(Xstr x, unsigned int number, int base) ;

/* Return a new Xstr containing a substring of a given Xstr */
Xstr XstrSubstring(Xstr x, int begin, int end) ;

/* Find the position of the first/next occurrence of a Xstr inside another */
int XstrFindXstr(Xstr inThis, Xstr findThis, int startingFrom) ;

/* Replace a range of a Xstr with the contents of another Xstr */
Xstr XstrReplaceRange(Xstr inThis, int fromHere, int toHere, Xstr replaceWithThis) ;

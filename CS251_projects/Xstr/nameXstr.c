#include <stdio.h>

char name[16];

static char * nameX(unsigned int x) {              /* Makes key-val pair for an Xstr state */
  sprintf(name, "%x", x);    /* String in hex of Xstr's address */
  return name;
}


int main() {
  char word1[] = "foo";
  char *name1 = nameX((unsigned int) &word1);
  printf("%s\n", name1);
  char word2[] = "bar";
  char *name2 = nameX((unsigned int) &word2);
  printf("%s\n", name2);

  return 0;
}

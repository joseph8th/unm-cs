
XSTREXTRA Xstr -- eXtendable string simple library implementation

AUTHOR:	Joseph Edwards <jee8th@unm.edu>
	UNM CS241 Fall 2011

DESCRIPTION: 

XSTREXTRA Xstr is a collection of basic routines providing efficient support for `extendable strings’ -- arbitrary length arrays of chars. It includes the following externally accessible functions:

Allocation and destruction functions:
=====================================
Xstr XstrMalloc(size_t initialCapacity) -- Returns a new Xstr of size = 0, capacity = initialCapacity.

Xstr XstrMallocZstring(char *z) -- Returns a new Xstr of size = strlen(z) and appropriate capacity.

void XstrFree(Xstr x) -- Free's an Xstr and its associated extendable array.

Accessor functions
==================
size_t XstrSize(Xstr x) -- Returns the size of an Xstr.

size_t XstrCapacity(Xstr x) -- Returns the capacity of an Xstr.

int XstrGet(Xstr x, size_t index) -- Returns the index-th element of an Xstr as an integer.

char * XstrAsZstring(Xstr x) -- Returns a pointer to the contents of the Xstr as a normal character array.

Xstr XstrDup(Xstr x) -- Returns a new Xstr whose contents are duplicated from a given Xstr.

Mutator functions
=================
size_t XstrReserve(Xstr x, size_t minCapacity) -- Reserves a minimum capacity for an Xstr, unless the capacity of the Xstr is already greater than the given minCapacity.

void XstrClear(Xstr x) -- Clears an Xstr by setting its size to zero.

Xstr XstrStore(Xstr x, size_t index, unsigned char toStore) -- Stores the character toStore in the index-th position in the Xstr's character array.

Xstr XstrAddChar(Xstr x, unsigned char ch) -- Adds the character ch to the end of the Xstr, possibly increasing it's capacity.

Xstr XstrAddZstring(Xstr x, char *z) -- Adds the string z to the end of the Xstr, possibly increasing it's capacity.

Xstr XstrAddXstr(Xstr x, Xstr other) -- Adds the extendable character array of one Xstr to the end of the array of another.

Extra functions
===============

XstrOutOfMemoryHandler XstrSetOutOfMemoryHandler(XstrOutOfMemoryHandler handler) -- Allows the Xstr library user to define their own out of memory handler.

int XstrCmp(Xstr x1, Xstr x2) -- Compares to Xstrs and returns their integer difference (0 if identical).

Xstr XstrSubstring(Xstr x, int begin, int end) -- Returns a new Xstr containing a substring of a given Xstr.

int XstrFindXstr(Xstr inThis, Xstr findThis, int startingFrom) -- Returns the location of one Xstr in another, or -1 if not found.

Xstr XstrAddInt(Xstr x, int number, int base) -- Adds a signed integer to the Xstr.

Xstr XstrAddUInt(Xstr x, unsigned int number, int base) -- Adds an unsigned integer to the Xstr.

Xstr XstrReplaceRange(Xstr inThis, int fromHere, int toHere, Xstr replaceWithThis) -- Replaces a range of characters in one Xstr with the entire contents of another Xstr.


KNOWN BUGS:

* The number of Xstrs that may be XstrFree'd (and therefore XstrMalloc'd) is limited to 65535 in this implementation. Xstr will exit with an `Out of Memory' error (100) when this limit is reached. :( I should've used a linked list, but then the deadline was here.

* Doesn't recognize that a user-defined Xstr was not made by an Xstr library function. Instead, this implementation treats such an Xstr as a NULL. (But it should recognize a non-NULL `fake' Xstr that fails to set the `library-made' flag in the corresponding struct xstr instance.)

* XstrReplaceRange hasn't been rigorously tested, and might have weird behavior (but I don't think so).

* The XstrOutOfMemoryHandler might be screwy (untested).


static void removeSNode(SarqNode * root, SarqNode snode) {
  
  if ((*root) == NULL) return;

  if ((*root) == snode)
    (*root) = snode->left;
  else {
    SarqNode thisNode;
    for (thisNode = (*root); (thisNode != NULL) && (thisNode->left != snode); thisNode = thisNode->left) {
    if (thisNode == NULL)
      return;
    else
      thisNode->left = snode->left;
    }
  }
  freeSNode(&snode);
}

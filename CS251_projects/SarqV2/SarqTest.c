#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include "SarqTest.h"

static int compareFunc(void * left, void * right) { /* matches SarqCompareFunction */
  return strcmp((char*) left, (char*) right);
}
static void printFunc(FILE * stream, void * item) { /* matches SarqPrintFunction */
  fprintf(stream,"%s", (char *) item);
}
static void freeFunc(void * item) { /* ignore */ } /* matches SarqFreeFunction */

int main(void) {
  uintptr_t i;
  Sarq * s1 = SarqMalloc(NULL);    /* Using default SarqType */
  SarqType sarqInfo = { compareFunc, printFunc, freeFunc };
  Sarq * s2 = SarqMalloc(&sarqInfo);

  /* s1 default SarqType tests */
  SarqInsert(s1, (void *) 20);      /* insert 2 items */
  SarqInsert(s1, (void *) 15);
  printf("size: %d\n", (int) SarqSize(s1));
  SarqPrint(stdout, s1);
  for (i = 0; i < 3; ++i) {        /* remove 3 items */
    printf("\nrm: %ld, ", (uintptr_t) SarqRemove(s1));
    printf("size: %d\n", (int) SarqSize(s1));
    SarqPrint(stdout, s1);         /* prints "rm: 0, size: 0" 3rd time */
  }
  printf("\n");
  for (i = 0; i < 10; ++i)         /* insert 10 items */
    SarqInsert(s1, (void *) i);
  printf("\nsize: %d, ", (int) SarqSize(s1));    /* prints 10 */
  printf("depth: %d\n", (int) SarqDepth(s1));    /* prints 4 or 5 */
  SarqPrint(stdout, s1);
  printf("\n");

  /* s2 custom zstring SarqType tests from man page */
  SarqInsert(s2,(void*) "foo");
  SarqInsert(s2,(void*) "bar");
  SarqInsert(s2,(void*) "foo");
  printf("%s\n", (char *) SarqRemove(s2)); /* prints bar */
  printf("%s\n", (char *) SarqRemove(s2)); /* prints foo */
  printf("%s\n", (char *) SarqRemove(s2)); /* prints foo */
  printf("%p\n", (char *) SarqRemove(s2)); /* prints (nil) */


  SarqFree(s1);
  SarqFree(s2);

  return 0;

}

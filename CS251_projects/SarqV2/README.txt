Sarq - Final Project Part 1

AUTHOR: Joseph Edwards <jee8th@unm.edu>
        UNM CS241 Fall 2011

DESCRIPTION (from man page): 

The Sarq Self-Adjusting Randomized Queue library is a collection of basic routines that provide efficient
support for a priority queue. A priority queue is a data structure that stores ’items’ that are ordered by some
notion of ’priority’, and the priority queue is designed so that it can quickly find and remove whatever is the
current ’highest priority’ item. By inserting n values in an arbitrary order and then removing them all
again, a priority queue can be used to sort the n values based on their priority, but a priority queue is best
optimized for cases when there are mixed sequences of insertions and removals on an ongoing basis.
For flexibility, the ’items’ that a Sarq stores are not predefined to be ints or char *’s or whatever, instead, at
the time of creation each Sarq is given access to a SarqType struct that uses function pointers to describe
how to perform the key operations (comparing two items, printing an item, and freeing an item) for the
particular ’item’s this Sarq will be managing.

The items themselves are handled "at arm’s length" using void *’s, and the Sarq routines generally don’t
know or care what those void pointer values mean to (or whether they are even treated as pointers by) the
supplied SarqType functions. The only exception is that SarqRemove returns 0 when the Sarq is empty,
regardless of the item interpretation. This could create an ambiguity if an item representation uses 0 as a
valid void * value; in such cases the Sarq user must check if the SarqSize is 0 to determine when the Sarq
is empty.

Author note: This implementation utilizes 'SarqNode' objects to contain the item and child pointers, and only
the root SarqNode stored in the Sarq object. Most of the user-accessible library functions listed below handle
SarqNode objects rather than Sarq's.

SarqType typedef is provided for users to define their own compare, print and free functions to handle any
item type. Users must implement the interface:

typedef struct sarqtype {
   SarqCompareFunction compare;
   SarqPrintFunction print;
   SarqFreeFunction free;
} SarqType;

Where users must implement the following function prototypes:

typedef int (*SarqCompareFunction)(void * left, void * right) ;
typedef void (*SarqPrintFunction)(FILE * stream, void * item) ;
typedef void (*SarqFreeFunction)(void * item) ;

User-accessible function prototypes are listed below, from Sarq.h:

/**
 * Create a new empty Sarq designed to hold items that are operated on
 * using the function pointers in the given 'info'.  If 'info' is
 * NULL, a default set of operations is assumed (see the man page).
 * 
 * The SarqType pointed to by info remains the property of the caller
 * of SarqMalloc; the Sarq library does _not_ take ownership of the
 * SarqType.
 */
extern Sarq * SarqMalloc(SarqType * info) ;

/**
 * Deallocate an existing Sarq and reclaim all storage associated with
 * it, including any items still present in the queue, which will be
 * freed using calls to the appropriate SarqFreeFunction. 
 */
extern void SarqFree(Sarq * s) ;

/**
 * Add 'item' to Sarq 's' using the 'self-adjusting randomized queue'
 * algorithm, consequently increasing SarqSize(s) by one.  
 */
extern void SarqInsert(Sarq * s, void * item) ;

/**
 * If SarqSize(s)==0, return 0.  Otherwise, remove and return the
 * highest priority item currently stored in 's', consequently
 * decreasing SarqSize(s) by one.
 */
extern void * SarqRemove(Sarq * s) ;

/**
 * Print a textual representation of 's' to the given 'stream'.  See
 * the man page.
 */
extern void SarqPrint(FILE * stream, Sarq * s) ;

/**
 * Return the current number of items stored in 's'.
 */
extern size_t SarqSize(Sarq * s) ;

/**
 * Return the current depth of 's' -- the number of items on 
 * any longest path from the root to any leaf.
 */
extern size_t SarqDepth(Sarq * s) ;


KNOWN BUGS:

      * I had to start (almost) from scratch this weekend, so 90% of this was written 11/7 & 11/8.
        I apologize for the scant annotation. 
      * Limitations: Unknown behavior when incorrect Sarq objects are passed to library functions.
      * Untested behavior with large Sarqs or large Sarq items.

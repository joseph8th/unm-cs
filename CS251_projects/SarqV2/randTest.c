#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <inttypes.h>

static int bits[31];
static void get31bits() {
  int i;
  uint32_t num = rand();
  for (i = 30; i >= 0; --i) {
    bits[i] = num & 0x1;
    num = num >> 1;
  }
}

int main(void) {
  int i, j;
  for (j = 0; j < 4; ++j) {
    get31bits();
    for (i = 0; i < 31; ++i) {
      printf("%d ", bits[i]);
    }
    printf("\n");
  }
  return 0;
}

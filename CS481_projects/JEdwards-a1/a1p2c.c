#include <sys/types.h>  /* for pid_t type     */
#include <sys/times.h>  /* for times()        */
#include <sys/resource.h>  /* for getrusage() */
#include <time.h>       /* for clock()        */
#include <stdio.h>      /* for printf()       */
#include <stdlib.h>     /* for exit()         */
#include <unistd.h>     /* for fork(),execl() */
#include <sys/wait.h>   /* for wait()         */

/* a1p2c.c: use execl() to execute date(), who() and sleep 5 and show  *
 * voluntary/involuntary context switches, clock time, user time, and  *
 * system time of each process                                         *
 *                                                                     *
 * Author: Joseph Edwards <jee8th@unm.edu>                             *
 * UNM CS481 Fall 2012                                                 */

typedef struct proct_t {
  time_t clkt;
  clock_t usrt;
  clock_t syst;
  clock_t cusrt;
  clock_t csyst;
} ProcT;

ProcT get_times(void) {

  struct tms tbuf;
  ProcT proct;

  times(&tbuf);

  proct.clkt = time(NULL);
  proct.usrt = tbuf.tms_utime;
  proct.syst = tbuf.tms_stime;
  proct.cusrt = tbuf.tms_cutime;
  proct.csyst = tbuf.tms_cstime;

  return proct;
}


void print_times(ProcT startt, ProcT endt) {

  static long clock_ticks = 0;

  if (clock_ticks == 0) {
    clock_ticks = sysconf(_SC_CLK_TCK);
  }

  printf("clock/user/sys: %.2f/%.2f/%.2f\n", 
         difftime(endt.clkt, startt.clkt),
         (double) (endt.usrt - startt.usrt) / clock_ticks,
         (double) (endt.syst - startt.syst) / clock_ticks);
  printf("child user/sys: %.2f/%.2f\n", 
         (double) (endt.cusrt - startt.cusrt) / clock_ticks,
         (double) (endt.csyst - startt.csyst) / clock_ticks);
}


int main(void) {

  ProcT startt[3], endt[3];
  struct rusage usages[3];
  pid_t status;

  /* First fork */
  startt[0] = get_times();
  if (!fork()) {
    printf("\ncommand: date\n");
    execl("/bin/date", "date", NULL);
  }
  else {
    wait(&status);
    getrusage(RUSAGE_SELF, &usages[0]);
    printf("voluntary context switches:\t%i\n", (int) usages[0].ru_nvcsw);
    printf("involuntary context switches:\t%i\n", (int) usages[0].ru_nivcsw);
    endt[0] = get_times();
    print_times(startt[0], endt[0]);

    /* Second fork */
    startt[1] = get_times();
    if (!fork()) {
      printf("\ncommand: who\n");
      execl("/usr/bin/who", "who", NULL);
    }
    else {
      wait(&status);
      getrusage(RUSAGE_SELF, &usages[0]);
      printf("voluntary context switches:\t%i\n", (int) usages[0].ru_nvcsw);
      printf("involuntary context switches:\t%i\n", (int) usages[0].ru_nivcsw);
      endt[1] = get_times();
      print_times(startt[1], endt[1]);

      /* Third fork */
      startt[2] = get_times();
      if (!fork()) {
        printf("\ncommand: sleep 5\n");
        execl("/bin/sleep", "sleep", "5", NULL);
      }
      else {
        wait(&status);
        getrusage(RUSAGE_SELF, &usages[0]);
        printf("voluntary context switches:\t%i\n", (int) usages[0].ru_nvcsw);
        printf("involuntary context switches:\t%i\n", (int) usages[0].ru_nivcsw);
        endt[2] = get_times();
        print_times(startt[2], endt[2]);
      }
    }
  }
  wait(&status);
  exit(0);
}

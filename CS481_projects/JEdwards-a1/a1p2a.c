#include <sys/types.h>  /* for pid_t type */
#include <stdio.h>      /* for printf()   */
#include <stdlib.h>     /* for exit()     */
#include <unistd.h>     /* for fork()     */

/* a1p2a.c: use fork() to create 2 child processes and display PIDs *
 * Author: Joseph Edwards <jee8th@unm.edu>                          *
 * UNM CS481 Fall 2012                                              */

int main(void) {

  pid_t pid1,pid2;

  /* First fork */
  pid1 = fork();
  if (!pid1) printf("\tChild process at fork 1: PID=%i, PPID=%i\n", getpid(), getppid());
  else {
    /* 2nd fork */
    pid2 = fork();
    if (!pid2) printf("\tChild process at fork 2: PID=%i, PPID=%i\n", getpid(), getppid());
    else printf("Process PID=%i, PPID=%i\n\tChild 1 PID=%i\n\tChild 2 PID=%i\n", getpid(), getppid(), pid1, pid2);
  }  
  exit(0);
}

#!/bin/bash
# a1p1b.sh: list processes with PID less than ARG
#
# Author: Joseph Edwards [jee8th@unm.edu]
# University of New Mexico, CS481 online, Fall 2012

# check for arg else default
PIDLTX=10

if [ $# == 1 ]
then
    PIDLTX=$1
fi

# function to list processes with PID less than ARG
ls_proc_pid_lt_x ()
{
    pidls="$PIDLTX"
    let "pidix = $PIDLTX - 1"

    printf "\nProcesses with PIDs less than %s\n\n" "$PIDLTX"

    until [ "$pidix" == 0 ]
    do
        pidls="${pidix},${pidls}"
        let pidix--
    done

    ps --pid $pidls -cjf    
}

ls_proc_pid_lt_x $1

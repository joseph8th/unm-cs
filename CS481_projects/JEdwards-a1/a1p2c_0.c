#include <sys/types.h>  /* for pid_t type     */
#include <sys/times.h>  /* for times()        */
#include <time.h>       /* for clock()        */
#include <stdio.h>      /* for printf()       */
#include <stdlib.h>     /* for exit()         */
#include <unistd.h>     /* for fork(),execl() */
#include <sys/wait.h>   /* for wait()         */

/* a1p2b.c: use execl() to execute date() and who() in child processes *
 * Author: Joseph Edwards <jee8th@unm.edu>                             *
 * UNM CS481 Fall 2012                                                 */

int main(void) {

  struct tms tbuf;
  clock_t startt, endt;
  pid_t pid1,pid2,pid3,status;

  times(&tbuf);

  startt = clock();
  /* First fork */
  pid1 = fork();
  if (!pid1) {
    printf("\ncommand: date\n");
    printf("Child process: PID=%i, PPID=%i\n", getpid(), getppid());
    execl("/bin/date", "date", NULL);
  }
  else {
    wait(&status);
    endt = clock();
    printf("clock/user/sys: %1.2f/%1.2f/%1.2f\n", (double)((endt - startt)/_SC_CLK_TCK),
           (double)tbuf.tms_utime / _SC_CLK_TCK, 
           (double)tbuf.tms_stime / _SC_CLK_TCK);
    printf("child user/sys: %1.2f/%1.2f\n", 
           (double)tbuf.tms_cutime / _SC_CLK_TCK, 
           (double)tbuf.tms_cstime / _SC_CLK_TCK);
    /* Second fork */
    pid2 = fork();
    if (!pid2) {
      printf("\ncommand: who\n");
      printf("Child process: PID=%i, PPID=%i\n", getpid(), getppid());
      execl("/usr/bin/who", "who", NULL);
    }
    else {
      wait(&status);
      startt = clock();
      /* Third fork */
      pid3 = fork();
      if (!pid3) {
        printf("\ncommand: sleep 5\n");
        printf("Child process: PID=%i, PPID=%i\n", getpid(), getppid());
        execl("/bin/sleep", "sleep", "5", NULL);
      }
      else {
        wait(&status);
        endt = clock();
        printf("clock/user/sys: %1.2f/%1.2f/%1.2f\n", (double)((endt - startt)/CLOCKS_PER_SEC),
               (double)tbuf.tms_utime / CLOCKS_PER_SEC, 
               (double)tbuf.tms_stime / CLOCKS_PER_SEC);
        printf("child user/sys: %1.2f/%1.2f\n", 
               (double)tbuf.tms_cutime / CLOCKS_PER_SEC, 
               (double)tbuf.tms_cstime / CLOCKS_PER_SEC);
      }
    }
  }
  wait(&status);
  exit(0);
}

#include <sys/types.h>  /* for pid_t type     */
#include <sys/times.h>  /* for times()    */
#include <time.h>       /* for clock()    */
#include <stdio.h>      /* for printf()   */
#include <stdlib.h>     /* for exit()     */
#include <unistd.h>     /* for fork(),execl() */
#include <sys/wait.h>   /* for wait()         */


int main(void) {

  struct tms tbuf;
  clock_t startt, endt;
  /*  pid_t status; */

  times(&tbuf);

  startt = clock();
  printf("\ncommand: sleep 5\n");
  execl("/bin/sleep", "sleep", "5", NULL);
  /*  wait(&status); */
  endt = clock();
  printf("clock/user/sys: %1.2f/%1.2f/%1.2f\n", (double)(endt - startt)/CLOCKS_PER_SEC,
         (double)tbuf.tms_utime / CLOCKS_PER_SEC, 
         (double)tbuf.tms_stime / CLOCKS_PER_SEC);
  printf("child user/sys: %1.2f/%1.2f\n", 
         (double)tbuf.tms_cutime / CLOCKS_PER_SEC, 
         (double)tbuf.tms_cstime / CLOCKS_PER_SEC);

  exit(0);
}

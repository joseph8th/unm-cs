#include <sys/types.h>  /* for pid_t type */
#include <stdio.h>      /* for printf()   */
#include <stdlib.h>     /* for exit()     */
#include <unistd.h>     /* for fork()     */
#include <sys/wait.h>   /* for wait()     */

/* a1p2b.c: use execl() to execute date() and who() in child processes *
 * Author: Joseph Edwards <jee8th@unm.edu>                             *
 * UNM CS481 Fall 2012                                                 */

int main(void) {

  pid_t pid1,pid2,status;

  /* First fork */
  pid1 = fork();
  if (!pid1) {
    fprintf(stdout,"\ncommand: date\n");
    printf("PID=%i, PPID=%i, Child 1 PID=%i\n", getpid(), getppid(), pid1);
    execl("/bin/date", "/bin/date", NULL);
  }
  wait(&status);

  /* Second fork */
  pid2 = fork();
  if (!pid2) {
    fprintf(stdout,"\ncommand: who\n");
    printf("PID=%i, PPID=%i, Child 2 PID=%i\n", getpid(), getppid(), pid2);
    execl("/usr/bin/who", "/usr/bin/who", NULL);
  }
  wait(&status);

  exit(0);
}

#include <sys/types.h>  /* for pid_t type     */
#include <sys/times.h>  /* for times()        */
#include <time.h>       /* for clock()        */
#include <stdio.h>      /* for printf()       */
#include <stdlib.h>     /* for exit()         */
#include <unistd.h>     /* for fork(),execl() */
#include <sys/wait.h>   /* for wait()         */

/* a1p2b.c: use execl() to execute date() and who() in child processes *
 * Author: Joseph Edwards <jee8th@unm.edu>                             *
 * UNM CS481 Fall 2012                                                 */

void command_fun(char * command, char * arg) {

  pid_t status;

  if (!fork()) {
    printf("\ncommand: %s %s\n", command, arg);
  }
  else {
    wait(&status);
    printf("DONE\n");
  }
}


int main(void) {

  command_fun("date", "");
  command_fun("who", "");
  command_fun("sleep", "5");

  exit(0);
}

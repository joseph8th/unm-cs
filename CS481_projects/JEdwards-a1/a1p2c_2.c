#include <sys/types.h>  /* for pid_t type     */
#include <sys/times.h>  /* for times()        */
#include <time.h>       /* for clock()        */
#include <stdio.h>      /* for printf()       */
#include <stdlib.h>     /* for exit()         */
#include <unistd.h>     /* for fork(),execl() */
#include <sys/wait.h>   /* for wait()         */

/* a1p2b.c: use execl() to execute date() and who() in child processes *
 * Author: Joseph Edwards <jee8th@unm.edu>                             *
 * UNM CS481 Fall 2012                                                 */

void print_times(struct tms tbuf, double tdiff) {
  printf("clock/user/sys: %1.2f/%1.2f/%1.2f\n", tdiff,
         (double)tbuf.tms_utime / _SC_CLK_TCK, 
         (double)tbuf.tms_stime / _SC_CLK_TCK);
  printf("child user/sys: %1.2f/%1.2f\n", 
         (double)tbuf.tms_cutime / _SC_CLK_TCK, 
         (double)tbuf.tms_cstime / _SC_CLK_TCK);
}


int main(void) {

  struct tms tbuffer;
  time_t startt, endt;
  pid_t status;

  times(&tbuffer);

  /* First fork */
  startt = time(NULL);
  if (!fork()) {
    printf("\ncommand: date\n");
    execl("/bin/date", "date", NULL);
  }
  else {
    wait(&status);
    endt = time(NULL);
    print_times(tbuffer, difftime(endt, startt));

    /* Second fork */
    startt = time(NULL);
    if (!fork()) {
      printf("\ncommand: who\n");
      execl("/usr/bin/who", "who", NULL);
    }
    else {
      wait(&status);
      endt = time(NULL);
      print_times(tbuffer, difftime(endt, startt));

      /* Third fork */
      startt = time(NULL);
      if (!fork()) {
        printf("\ncommand: sleep 5\n");
        execl("/bin/sleep", "sleep", "5", NULL);
      }
      else {
        wait(&status);
        endt = time(NULL);
        print_times(tbuffer, difftime(endt, startt));
      }
    }
  }
  wait(&status);
  exit(0);
}

#include <stdio.h>        /* for fprintf() */
#include <stdlib.h>  
#include <sys/types.h>    /* for DIR type */
#include <dirent.h>       /* for *dir() fcns */
#include <errno.h>        /* for error codes */
#include <string.h>       /* for string fcns */

int main(int argc, char * argv[]) {

  DIR *dir;
  struct dirent *entry;
  char *dirstr, *extstr, *dotptr;
  int dlen, found = 0;

  /* make sure enough args */
  if (argc != 3) {
    printf("Usage: filesuffix <SEARCH_DIR> <SUFFIX>\n");
    exit(EXIT_FAILURE);
  }

  /* load up directory and extension by arg order */
  dirstr = argv[1];
  extstr = argv[2];
  dlen = strlen(dirstr)-1;

  /* open directory or exit with error msg */
  if ((dir = opendir(dirstr)) == NULL) {
    perror("Error opening directory");
    exit(EXIT_FAILURE);
  }

  /* loop through the directory entries */
  while ((entry = readdir(dir)) != NULL) {

    /* reverse search filename for '.' char */
    if ((dotptr = strrchr(entry->d_name, '.')) != NULL ) {
      dotptr++;
      if (!strcmp(dotptr, extstr)) {
        if (!found) printf("\nExtension '%s' found:\n", extstr);
        found++;
        /* check for trailing slash at output */
        if (dirstr[dlen] != '/')
          printf("%s/%s\n", dirstr, entry->d_name);
        else
          printf("%s%s\n", dirstr, entry->d_name);
      }
    }
  }

  /* no extension found message */
  if (!found) printf("\nExtension '%s' not found.\n", extstr); 
  
  exit(EXIT_SUCCESS);
}

#include <sys/resource.h>       /* for getrusage()       */
#include <sys/types.h>          /* for pid_t type        */
#include <unistd.h>             /* for fork(),execv(p)() */
#include <stdio.h>              /* for fprintf() */
#include <stdlib.h>             /* for exit codes */
#include <string.h>             /* for string fcns */
#include <limits.h>             /* for system vars */
#include <sys/wait.h>           /* for waitpid()         */
#include <sys/stat.h>           /* for mode_t            */
#include <fcntl.h>              /* for open(), close()   */
#include <errno.h>              /* error messages        */

#define EOL 1                   /* end of line */
#define ARG 2			/* normal argument */
#define AMPERSAND 3             /* & run in background */
#define SEMICOLON 4             /* ; run in background */
#define INREDIR 5               /* < redirect input */
#define OUTREDIR 6              /* > redirect output */

#define NUM_SPECIAL 8
#define MAXARG 32 		/* max. no. command args*/
#define MAXBUF 4096		/* max. length input command */
#define PATH_MAX 256            /* guess at path max */

#define FOREGROUND 0            /* foreground process */
#define BACKGROUND 1            /* background process */

#define MINSH_EXIT 0            /* mini-shell 'exit' command */
#define MINSH_CD 1              /* minsh 'chdir' command */


/************************************************
 * minsh_a.c: 'minsh-J-E' mini-shell program    *
 *----------------------------------------------*
 * VERSION: a4p3a (execv, background, redirect) *
 * AUTHOR: Joseph Edwards <jee8th@unm.edu>      *
 * CS481online, Univ. of New Mexico, Fall 2012  *
 ************************************************/


/* define struct type for easier time-keeping maths */
typedef struct rusgt_t {
  double utime;
  double stime;
} RusgT;

/* program buffers and work pointers */
static char inpbuf[MAXBUF], tokbuf[2*MAXBUF], *ptr=inpbuf, *tok = tokbuf;
static char pthbuf[2*MAXBUF], *pth = pthbuf;

/* special characters */
static char special[]={' ', '\t', '&', ';', '\n', '\0', '<', '>'};

/* minsh special command declarations */
static char *msCmdAry[]={"exit", "cd", "help", "\0"};
static char wrkdirbuf[PATH_MAX];

/* struct type for command run parameters */
typedef struct runparam_t {
  int type;
  char infile[PATH_MAX];
  char outfile[PATH_MAX];
} RunParam;

/* prompt */
char *prompt = "minsh-J-E"; 


/* check if the input character is a special one:
 * one of special[]
 */
int inarg(char c) {
  /* used array index b/c special[] is null-term'd */
  int ix;
  for(ix = 0; ix < NUM_SPECIAL; ix++)
    {
      if(c == special[ix])
        return(0);
    }
  return(1);
}


/* print prompt and read a line 
 */
int userin(char *p) {
  /*initialization for later routines */
  char c = '\0';
  int ix = 0;
  ptr = inpbuf;
  tok = tokbuf;
  /* char *startptr = &inpbuf[0]; */
  
  /* display prompt */
  printf("\n%s: ", p);
  
  while ( (c != EOF) && (ix < MAXBUF) ) {
    c = getchar();
    if (c == EOF) 
      break;
    if (c == '\n') {
      /* *ptr='\0'; */
      inpbuf[ix] = '\0';
      return(EOL);
    }
    /* else add to input buffer */
    inpbuf[ix]=c;
    ix++;
  }
  return(EOF);
}


/* 
 * get token, place into tokbuf 
 */
int gettok(char **outptr)
{
  int type;	
  /* set the outptr string to tok */
  *outptr = tok;
  /* strip white space from the buffer containing the token */
  while ((*ptr == ' ') || (*ptr == '\t')) ptr++;
  /* set the token pointer to the first token in the buffer */	    
  *tok++ = *ptr;

  /* set the type variable depending on the token in the buffer */
  switch (*ptr++) 
    {
    case '\0':
    case '\n':
      type = EOL;
      break;
    case ';':
      type = SEMICOLON;
      break;
    case '&':
      type = AMPERSAND;
      break;
    case '<':
      type = INREDIR;
      break;
    case '>':
      type = OUTREDIR;
      break;
    default:
      type = ARG;
      while (inarg(*ptr))
        *tok++ = *ptr++;
    }
  *tok++ = '\0';
  return type;
}



/* post-execute routines 
 */
int postexec() {

  static int ru1st = 1;
  static RusgT rut;          /* rusage times */
  RusgT thisrut;
  struct rusage ru;          /* for rusage() */

  /* init rut the first time */
  if (ru1st) {
    ru1st = 0;
    rut.utime = 0.0;
    rut.stime = 0.0;
  }

  /* print child exec times for this run */
  if (getrusage(RUSAGE_CHILDREN, &ru) != -1) {

    /* get this run's sum of children exec times */
    thisrut.utime = ((double) ru.ru_utime.tv_sec 
                     + (double) ru.ru_utime.tv_usec / 1000000.0);
    thisrut.stime = ((double) ru.ru_stime.tv_sec 
                     + (double) ru.ru_stime.tv_usec / 1000000.0);
    
    /* subtract off last run's sum of child exec times */
    printf("\n==> CompleteRun: user time %.6f system time %.6f\n", 
           thisrut.utime - rut.utime, 
           thisrut.stime - rut.stime);

    /* set the 'last run times' for the next run */
    rut.utime = thisrut.utime;
    rut.stime = thisrut.stime;
    
    return 1;
  }
  else return 0;
}



/* Parse colon-separated paths into array
 * and return number of elts
 */
int parsepath(char **outptr, char *pathptr) 
{
  int pathc = 0;

  /* look for end of colon-sep path or too many paths */
  while ( (*pathptr != '\0') && (pathc < MAXARG) ) {
    /* set the outptr string to pathptr */
    outptr[pathc] = pth;
    *pth = *pathptr;
    /* skip over path chars */
    while ( (*pathptr != ':') && (*pathptr != '\0') )
      *pth++ = *pathptr++;
    /* null terminate the path & incr numpath */
    *pth++ = '\0';
    pathc++;
    /* if EOL or COLON then act differently */
    if (*pathptr == '\0') break;
    if (*pathptr == ':') pathptr++;
  }
  return pathc;
}


/*
 * find custom mini-shell command,
 * return index to global msCmdAry[]
 */
int findMSCmd(char * cmd) {

  int ix = 0;

  while (*msCmdAry[ix] != '\0') {
    /* if found return index+1 positive */
    if (!strcmp(msCmdAry[ix], cmd)) 
      return (ix+1);
    ix++;
  }
  return 0;
}


/* find a command on system PATH or return false 
 */
int findcommand(char cpath[], char *command) {

  int ix = 0, 
      numpath = 0, 
      found = 0;
  char *pathary[MAXARG];
  char wrkpth[PATH_MAX];

  /* get the system PATH environment variable */
  char *pathptr = getenv("PATH");
  /* check for getenv() errors */
  if ( ( pathptr == NULL ) || (strlen(pathptr) <= 0) ) {
    fprintf(stderr, "Error retrieving system environment PATH.\n");
    return 0;
  }

  /* parse :-separated PATH into array of paths */
  if ((numpath = parsepath(pathary, pathptr)) == 0)
    return 0;

  /* search each path in the array */
  while (ix < numpath) {
    sprintf(wrkpth, "%s/%s", pathary[ix], command);
    /* found or not */
    if ( access(wrkpth, X_OK) == 0 ) {
      found = 1;
      break;
    }
    else
      ix++;
  }

  /* if not on PATH, look for custom minsh command */
  if (!found) {
    *wrkpth = '\0';
    found = findMSCmd(command);
  }

  /* found so copy out wrkpath & return true */
  if (found) {
    strcpy(cpath, wrkpth);
    return found;
  }

  /* else not found, return false */
  return 0;
}


/*
 * execute a mini-shell command in parent only
 * (future version could use callback fcns in procs or thrds)
 */
int runMinshCmd(char **cline, int argno) {

  int ix, runerr;

  /* pre-exec info */
  printf("\n==> MinshToRun: %s ", cline[0]);
  for (ix=1; ix<argno; ix++) 
    printf(": %s ", cline[ix]);
  printf("\nWorking dir: %s \n\n", wrkdirbuf);

  /* get index of minsh command */
  ix = findMSCmd(cline[0]) - 1;

  /* act accordingly */
  switch (ix) {
  case MINSH_EXIT:
    printf("Exiting %s...\n", prompt);
    return -1;
  case MINSH_CD:
    if (argno == 2) {
      printf("Changing directory to %s\n", cline[1]);
      runerr = chdir(cline[1]);
      return runerr;
    }
    else
      printf("Error in arguments to %s.\n", cline[0]);
    break;
  default:
    printf("%s Help:\n\texit: exit mini-shell (^C)\n\tcd: change directory\n", prompt);
    return 0;
  }

  return 1;
}


/* 
 * execute a command with optional wait 
 */
int runcommand(char **cline, int argno, RunParam run) {

  pid_t pid, status;           /* process info */
  int ix,
    runerr,                    /* runMinshCmd err code */
    fidin, fidout;             /* file ids for I/O redirects */
  char cpath[PATH_MAX];        /* command path */
  char *command = cline[0];    /* command elt of array */

  /* get current working directory */
  getcwd(wrkdirbuf, PATH_MAX);
  
  /* try to find the command executable on system PATH */
  if ( !findcommand(cpath, command) ) {
    fprintf(stderr, "Command not found: %s\n", command);
    return 0;
  }

  /* else found: if cpath[0] is NULL then minsh execs */
  if (*cpath == '\0') {
    runerr = runMinshCmd(cline, argno);
    return runerr;
  }
 
  /* pre-exec info */
  printf("\n==> SysToRun: %s : %s ", cpath, command);
  for (ix=1; ix<argno; ix++) 
    printf(": %s ", cline[ix]);
  if (*run.infile != '\0')
    printf("< %s ", run.infile);
  if (*run.outfile != '\0')
    printf("> %s ", run.outfile);
  printf("\nWorking dir: %s \n\n", wrkdirbuf);

  /* Different cases according to pid(parent or child),
   * take different actions */
  switch (pid = fork()) {
  case -1:
    fprintf(stderr, "Error forking process.\n");
    return -1;
  case 0:
    /*** CHILD PROCESS ***/

    /* redirect input to file? */
    if (*run.infile != '\0') {      
      if ( (fidin = open(run.infile, O_RDONLY, 0644)) == -1) {
        fprintf(stderr, "Input Redirect Error at open(%s): %s\n", 
                run.infile, strerror(errno) );
        return 0;
      }
      /* close stdin and dup(fidin) */
      if ( dup2(fidin, STDIN_FILENO) == -1 ) {
        perror("Input Redirect Error at dup2()");
        return 0;
      }
      close(fidin);
    }

    /* redirect output to file? */
    if (*run.outfile != '\0') {     
      if ( (fidout = open(run.outfile, O_WRONLY|O_CREAT|O_TRUNC, 0644)) == -1 ) {
        fprintf(stderr, "Output Redirect Error at open(%s): %s\n", 
                run.outfile, strerror(errno) );
        return 0;
      }
      /* close stdout and dup(fidout) */
      if ( dup2(fidout, STDOUT_FILENO) == -1 ) {
        perror("Output Redirect Error at dup2()");
        return 0;
      }
      close(fidout);
    }

    /*** EXECV command in child process ***/
    if (execv(cpath, cline) == -1)
      fprintf(stderr, "Error executing %s: %s\n", *cline, strerror(errno));
    exit(1);
    
  } /* end child proc */
  
  /*** PARENT PROCESS ***/

  if (run.type == FOREGROUND) {
    /* parent wait */
    if (waitpid(pid, &status, 0) == -1) {
      fprintf(stderr, "Error waiting for PID %d: %s\n", pid, strerror(errno));
      return -1;
    }
    /* post-exec info */
    if (!postexec()) {
      fprintf(stderr, "Error in post-exec function.");
      return -1;
    }
    /* we waited so return status */
    return status;
  }

  /* return default if BACKGROUND */
  return 0;
}


/* process input line
 * ALL FOREGROUND version 
 */
int procline(void)
{
  char *arg[MAXARG + 1];   /* pointer array for runcommand */
  int toktype;		  /* type of token in command     */
  int narg = 0;           /* number of arguments so far   */
  int ran;                /* error code for runcommand()  */

  /* init for redirect subroutines     */
  RunParam run = {FOREGROUND, "\0", "\0"};

  for(;;) {
    /* take action according to token type */
    switch(toktype = gettok(&arg[narg])) {
    case ARG:
      if (narg<MAXARG) narg++;
      break;
    case EOL:
      if (narg != 0) {
        arg[narg] = '\0';
        ran = runcommand(arg, narg, run);
      }
      return ran;
    case SEMICOLON:
    case AMPERSAND:
      /* change to BACKGROUND and add routine
       * to runcommand() to add run in background
       */
      if (narg != 0) {
        arg[narg] = '\0';
        if (toktype == AMPERSAND) run.type = BACKGROUND;
        ran = runcommand(arg, narg, run);
        if (ran == -1) return ran;
      }
      narg = 0;
      run.type = FOREGROUND;
      *run.infile = *run.outfile = '\0';
      break;
    case INREDIR:
    case OUTREDIR:
      if (narg != 0) {
        arg[narg] = '\0';
        if (gettok(&arg[narg+1]) == ARG) {
          if (toktype == INREDIR)
            strcpy(run.infile, arg[narg+1]);
          if (toktype == OUTREDIR)
            strcpy(run.outfile, arg[narg+1]);
        }
      }
      break;
    } /* end switch */
  } /* end for */
}


/*
 * Main function 
 */
int main()
{
  /* print prompt and read a line*/
  while ((userin(prompt)) != EOF) {
    /* process the input, execute the command and output the results */	  
    if (procline() == -1) break;
  }
  printf("\n\nTerminating mini-shell [%s].\n", prompt);
  exit(EXIT_SUCCESS);
}
  

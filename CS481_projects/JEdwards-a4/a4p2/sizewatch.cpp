#include <iostream>    /* for i/ostream */
#include <fstream>     /* for i/ofstream */
#include <sstream>     /* for stringstream */
#include <stdlib.h>    /* for exit() */
#include <unistd.h>    /* for sleep() */
#include <time.h>      /* for time() */

/* 10 min * 60 sec */
#define MAX_WATCH_SECONDS 30

using namespace std;

/*
 * main function - commandline interface
 */
int main( int argc, char * argv[] ) {

  int fsize, lastsize = 0;
  int isfirst = 1;
  time_t startt, t;
  pid_t pid, sid;

  /* check for commandline arg */
  if (argc != 2) {
    cerr << "\nUsage: " << argv[0] << " <PATH_TO_FILE>" << endl;
    exit(EXIT_FAILURE);
  }

  /* fork() from the parent */
  pid = fork();
  /* error forking */
  if (pid < 0 ) {
    cerr << "\nUnable to create child process" << endl;
    exit(EXIT_FAILURE);
  }
  /* control returns to parent here */
  if (pid > 0) {
    exit(EXIT_SUCCESS);
  }

  /* create file stream to watch */
  ifstream watchfile ( argv[1], ifstream::in );
  ofstream logfile( "sizewatch.log", ios::app );

  /* check if stream open */
  if (!watchfile.is_open()) {
    cerr << "\nError: file not found: " << argv[1] << endl;
    exit(EXIT_FAILURE);
  }

  /* set new SID for child */
  if ((sid = setsid()) < 0) {
    cerr << "\nError: unable to set SID for child process" << endl;
    exit(EXIT_FAILURE);
  }

  /* close std file descriptors to daemonize */
  close(STDIN_FILENO);
  close(STDOUT_FILENO);
  close(STDERR_FILENO);

  /* get start time */
  time(&startt);
  time(&t);

  /* start watching */
  while (difftime(t,startt) < MAX_WATCH_SECONDS) {

    /* get file size */
    watchfile.seekg(0,ios::end);
    fsize = watchfile.tellg();
    watchfile.seekg(0, ios::beg);

    /* build string and write */
    time(&t);
    if (isfirst) {
      logfile << "Opening size " << fsize << " at " << ctime(&t);
      isfirst = 0;
    }
    else
      logfile << "Changed size " << fsize - lastsize << " at " << ctime(&t);

    /* remember last size */
    lastsize = fsize;

    /* sleep for 10 seconds */
    sleep(10);
  }

  /* close streams */
  watchfile.close();
  logfile.close();

  exit(EXIT_SUCCESS);
}

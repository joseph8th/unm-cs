/*
 * waitline.h - Class header for 'WaitLine' queue class
 * in JEdwards-a3/a3p2/a3p2.cpp theme park simulation.
 *
 * Author: Joseph Edwards VIII
 *         CS481 online, UNM Fall 2012
 *         JEdwards-a3/a3p2/waitline.h
 */

#ifndef WAITLINE_H
#define WAITLINE_H

#include <pthread.h>
#include <stdlib.h>
#include "visitor.h"
#include <queue>

#define MAXWAITPEOPLE 800

typedef struct {
  int rejected;
  int waiting;
} ArvN;

namespace notroot {

  class WaitLine {
  private:
    int m_tot;
    std::queue<Visitor> Q;
    pthread_mutex_t * mutex;
    pthread_cond_t * notEmpty;
    pthread_cond_t * notFull;

  public:
    WaitLine();
    ~WaitLine();
    void ArriveN(int n, int t_mins, ArvN * arvN);
    void EmbarkN(int n, std::queue<Visitor> & Qout);
  };

}

#endif  /* WAITLINE_H */

/*
 * visitor.cpp - Implement 'Visitor' data class
 * in JEdwards-a3/a3p2/a3p2.cpp theme park simulation.
 *
 * Author: Joseph Edwards VIII
 *         CS481 online, UNM Fall 2012
 *         JEdwards-a3/a3p2/visitor.cpp
 */

#include "visitor.h"

namespace notroot {

  Visitor :: Visitor(int arrivalTime) {
    myArrivalTime = arrivalTime;
  }
  /* Getters & setters */
  int Visitor :: GetArrival() { return myArrivalTime; }

  void Visitor :: SetEmbark(int embarkTime) { myEmbarkTime = embarkTime; }

  int Visitor :: GetEmbark() { return myEmbarkTime; }
}

/*
 * waitline.cpp - Implement 'WaitLine' queue class
 * in JEdwards-a3/a3p2/a3p2.cpp theme park simulation.
 *
 * Author: Joseph Edwards VIII
 *         CS481 online, UNM Fall 2012
 *         JEdwards-a3/a3p2/waitline.cpp
 */

#include "visitor.h"
#include "waitline.h"

namespace notroot {

  WaitLine :: WaitLine() {
    m_tot = 0;
    mutex = (pthread_mutex_t *) malloc(sizeof(pthread_mutex_t));
    pthread_mutex_init(mutex, NULL);
    notEmpty = (pthread_cond_t *) malloc(sizeof(pthread_cond_t));
    pthread_cond_init(notEmpty, NULL);
    notFull = (pthread_cond_t *) malloc(sizeof(pthread_cond_t));
    pthread_cond_init(notFull, NULL);
  }

  void WaitLine :: ArriveN(int n, int t_mins, ArvN * arvN) {

    int i = 0;
    int qlen, N, reject = 0;

    pthread_mutex_lock(mutex);

    m_tot = t_mins;
    while (Q.size() == MAXWAITPEOPLE)
      pthread_cond_wait(notFull, mutex);

    qlen = Q.size();
    if (qlen + n < MAXWAITPEOPLE) N = n;
    else {
      reject = (qlen + n)%MAXWAITPEOPLE;
      N = n - reject;
    }

    while (i < N) {
      Q.push(Visitor(m_tot));
      i++;
    }

    arvN->rejected = reject;
    arvN->waiting = qlen + N;

    pthread_mutex_unlock(mutex);
    pthread_cond_broadcast(notEmpty);
  }

  void WaitLine :: EmbarkN(int n, std::queue<Visitor> & Qout) {

    int N, qlen, i = 0;

    pthread_mutex_lock(mutex);

    while (Q.empty())
      pthread_cond_wait(notEmpty, mutex);

    qlen = Q.size();
    if (qlen - n >= 0) N = n;
    else N = qlen;

    while (i < N) {
      Visitor tmpVis = Q.front();
      tmpVis.SetEmbark(m_tot);
      Qout.push(tmpVis);
      Q.pop();
      i++;
    }

    pthread_mutex_unlock(mutex);
    pthread_cond_broadcast(notFull);
  }

  WaitLine :: ~WaitLine() {
    pthread_mutex_destroy(mutex);
    free(mutex);
    pthread_cond_destroy(notEmpty);
    free(notEmpty);
    pthread_cond_destroy(notFull);
    free(notFull);
  }

}

/*
 * parksim.cpp - simulate waiting line for theme park
 *
 * Author: Joseph Edwards VIII
 *         CS481 online, UNM Fall 2012
 *         JEdwards-a3/a3p2/parksim.cpp
 */

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <queue>

#include "random437.h"
#include "visitor.h"
#include "waitline.h"

#define ARRIVAL_DELAY 60000
#define LOAD_DELAY 7000
#define EMBARK_DELAY 53000

using namespace notroot;


// simulate arrival periods & rates
struct arv_t {          
  int startTime;
  int meanRate;
} arv[] = {
  {9,25}, {11,45}, {14,35}, {16,25}, {19,0}
};

struct totals_t {
  int arrivals;
  int riders;
  int rejects;
  int wait_times;
  int max_line;
  int max_line_at;
} totals = {0, 0, 0, 0, 0, 0};

// some global variables & objects
int CARNUM, MAXPERCAR;
WaitLine myWaitLine;
int fin = 0;

/*
 * wait line thread handler fcn (producer)
 */
void * waitline_fcn(void * arg) {

  ArvN arvN;
  int h, m_tot = 0, m = 0;
  Random myRand(false);                /* Random object */
  int arrive, reject;               /* per min log vars */
  int per = 0;                         /* rate period index */

  // loop thru arrival rate periods - end when rate == 0 
  while (arv[per].meanRate != 0) {
    /*    printf("\nArrival Period %d: Start Time %d Mean Rate %d\n", 
          per, arv[per].startTime, arv[per].meanRate); */

    // loop the minutes per period
    m = 0;
    while (arv[per].startTime*60 + m < arv[per+1].startTime*60) {

      // per hour subroutine
      if (m%60 == 0)
        h = (arv[per].startTime*60 + m)/60;

      // produce arrivals per minute here...
      arrive = myRand.poisson((double)arv[per].meanRate);
      
      // ...send new arrivals to produce function - old time: m_tot
      myWaitLine.ArriveN(arrive, m_tot, &arvN);
      usleep(ARRIVAL_DELAY);

      totals.arrivals += arrive;
      totals.rejects += arvN.rejected;
      if (arvN.waiting > totals.max_line) {
        totals.max_line = arvN.waiting;
        totals.max_line_at = m_tot;
      }
  
      printf("%03d\t%03d arrive\t%03d reject\t%03d waitline\t%02d:%02d:00\n",
             m_tot, arrive, arvN.rejected, arvN.waiting, h, m%60 );

      m++;      /* end of period 'minute' */
      m_tot++;  /* 'minute' counter */
    }
    per++;      /* end of rate period */
  }
}


/*
 * explorer thread handler fcn (consumer)
 */
void * explorer_fcn(void * arg) {

  int id = *((int *)arg);

  std::queue<Visitor> load;
  int w, m;
  int per = 0;       /* arrival period index */

  while (!fin) {
    // consume method
    usleep(LOAD_DELAY);
    myWaitLine.EmbarkN(MAXPERCAR, load);
    usleep(EMBARK_DELAY);
    
    totals.riders += load.size();
    
    while (!load.empty()) {
      w = (load.front()).GetEmbark() - (load.front()).GetArrival();
      /*      printf("%d embark time %d arrive time %d wait time\n", 
              (load.front()).GetEmbark(), (load.front()).GetArrival(), w); */
      
      totals.wait_times += w;
      load.pop();
    }
  }

}


/* 
 * main function 
 */
int main(int argc, char *argv[]) {

  int ix;
  pthread_t waitThread, carThread[CARNUM];   /* threads */

  /* parse args */
  if (argc < 3) {
    fprintf(stderr, "\nUsage: %s CARNUM MAXPERCAR >> filename\n", argv[0]);
    exit(EXIT_SUCCESS);
  }
  CARNUM = atoi(argv[1]);
  MAXPERCAR = atoi(argv[2]);

  // create 1 producer thread
  if ( pthread_create(&waitThread, NULL, waitline_fcn, NULL) )
    exit(EXIT_FAILURE);

  // create CARNUM consumer threads
  for (ix=0; ix<CARNUM; ++ix) {
    if ( pthread_create(&carThread[ix], NULL, explorer_fcn, (void *)&ix) )
      exit(EXIT_FAILURE);
  }

  // join producer thread
  pthread_join(waitThread, NULL);
  fin = 1;

  printf("\n%d arrived\t%d ride\t%d go away\t%d avg wait\t%d line 1st max at %d\n",
         totals.arrivals, totals.riders, totals.rejects, 
         totals.wait_times/totals.riders, totals.max_line, totals.max_line_at);

  return 0;
}

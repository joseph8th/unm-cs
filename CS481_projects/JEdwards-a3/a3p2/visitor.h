/*
 * visitor.h - Class header for 'Visitor' model class
 * in JEdwards-a3/a3p2/a3p2.cpp theme park simulation.
 *
 * Author: Joseph Edwards VIII
 *         CS481 online, UNM Fall 2012
 *         JEdwards-a3/a3p2/visitor.h
 */

#ifndef VISITOR_H
#define VISITOR_H

namespace notroot {

  class Visitor {
  private:
    int myArrivalTime;
    int myEmbarkTime;
    int myDepartTime;

  public:
    /* Constructor */
    Visitor(int arrivalTime);
    /* Getters & setters */
    int GetArrival();
    void SetEmbark(int embarkTime);
    int GetEmbark();
  };  /* class */

}

#endif  /* VISITOR_H */

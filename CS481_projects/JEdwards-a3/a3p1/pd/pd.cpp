#include "../pc/pc.h"       /* for Semaphore() */
#include <unistd.h>       /* for fork()   */
#include <stdio.h>
#include <stdlib.h>       /* for EXIT_*   */
#include <errno.h>        /* for perror() */
#include <sys/types.h>
#include <pthread.h>

#include "../shm437.h"       /* for Shm437() */

using namespace notroot;

/* create semaphore instance */
Semaphore sem(false);

struct BANK {
  int balance[2];
  BANK() {
    balance[0] = balance[1] = 0;
  }
};

Shm437 *pShmBank = new Shm437(1,sizeof(BANK));
BANK *ptrBank = (BANK*) pShmBank->ShmAlloc();

/*
 * function to run as threads
 */
void * startThread(void* arg) {

  int tmp1, tmp2, rint;
  double dummy;

  int acctno = *((int*)arg);

  for (int i=0; i < 100; i++) {
    ::sem.wait();
    tmp1 = ptrBank->balance[0]; 
    tmp2 = ptrBank->balance[1];
    rint = (rand()%20) - 10;
    if ( (tmp1 + rint) >= 0 && (tmp2 - rint) >=0 ) {

      if (acctno == 0) {
        ptrBank->balance[0] = tmp1 + rint;
      }

      for (int j=0; j < rint*100; j++) {
        dummy = 2.345*8.765/1.234;
      }
      usleep(1);

      if (acctno == 1) {
        ptrBank->balance[1] = tmp2 - rint;
      }

    }
    ::sem.post();
  }

}

/*
 * main() function
 */
int main(int argc, char **argv) {

  pthread_t thr1, thr2;
  int thr1_id, thr2_id;

  int acct[]={0, 1};

  ptrBank->balance[0] = ptrBank->balance[1] = 100;
  srandom(getpid());

  printf("Init balances 0:%d + 1:%d ==> %d!\n",
         ptrBank->balance[0], ptrBank->balance[1],
         ptrBank->balance[0] + ptrBank->balance[1]);

  thr1_id = pthread_create(&thr1, NULL, &startThread, (void*)&acct[0]);
  thr2_id = pthread_create(&thr2, NULL, &startThread, (void*)&acct[1]);

  ::sem.wait();
  printf("Let's check the balances 0:%d + 1:%d ==> %d ?= 200\n",
         ptrBank->balance[0], ptrBank->balance[1],
         ptrBank->balance[0] + ptrBank->balance[1]);
  ::sem.post();

  exit(EXIT_SUCCESS);
}

// EECE437 sample file to deal with UNIX shared memory
#ifndef shm437_h
#define shm437_h
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

class Shm437 {
  public:
    // constructor
      Shm437(int seqnum, unsigned int size) {
        key_t key; 
        if ((key=ftok(getenv("HOME"), seqnum))==(key_t)-1) {
          perror("ERROR: ftok"); 
          }
        else if ((shmid=shmget(key,size,0600|IPC_CREAT))==-1) {
          perror("ERROR: shmget");
          }
        ssize = size;
	}
      void *ShmAlloc() {
	void *ptr;
        if ((ptr=shmat(shmid,(void*)NULL,0600))==(void*)-1) {
          perror("ERROR: shmalloc");
          }
        memset((char*)ptr,0,ssize);
        return ptr;
        };
  private:
    // data members
      int shmid, ssize;
  };
#endif /* shm437_h */

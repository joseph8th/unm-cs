#include <semaphore.h>    /* for POSIX semaphores */
#include <unistd.h>       /* for fork()   */
#include <stdio.h>
#include <stdlib.h>       /* for EXIT_*   */
#include <errno.h>        /* for perror() */
#include <sys/types.h>
#include <sys/wait.h>

#include "../shm437.h"       /* for Shm437() */

struct BANK {
  int balance[2];
  BANK() {
    balance[0] = balance[1] = 0;
  }
};

int main(int argc, char **argv) {

  sem_t mutex;

  pid_t pid; 
  int tmp1, tmp2, rint; 
  double dummy;
  Shm437 *pShmBank = new Shm437(1,sizeof(BANK));
  BANK *ptrBank = (BANK*) pShmBank->ShmAlloc();

  ptrBank->balance[0] = ptrBank->balance[1] = 100;
  srandom(getpid());

  printf("Init balances 0:%d + 1:%d ==> %d!\n",
         ptrBank->balance[0], ptrBank->balance[1],
         ptrBank->balance[0] + ptrBank->balance[1]);

  /* init semaphore */
  if ( sem_init(&mutex, 1, 1) < 0) {
    perror("Error: sem_init() failed.");
    exit(EXIT_FAILURE);
  }

  pid = fork();

  if (pid==0) {
    for (int i=0; i < 100; i++) {
      tmp1 = ptrBank->balance[0]; 
      tmp2 = ptrBank->balance[1];
      rint = (rand()%20) - 10;
      if ( (tmp1 + rint) >= 0 && (tmp2 - rint) >=0 ) {

        sem_wait(&mutex);
        ptrBank->balance[0] = tmp1 + rint;
        sem_post(&mutex);

        for (int j=0; j < rint*100; j++) {
          dummy = 2.345*8.765/1.234;
        }
        usleep(1);

        sem_wait(&mutex);
        ptrBank->balance[1] = tmp2 - rint;
        sem_post(&mutex);
      }
    }
  }

  if (pid!=0) {
    /*    wait(NULL); */

    sem_wait(&mutex);
    printf("Let's check the balances 0:%d + 1:%d ==> %d ?= 200\n",
           ptrBank->balance[0], ptrBank->balance[1],
           ptrBank->balance[0] + ptrBank->balance[1]);
    sem_post(&mutex);
  }
  exit(EXIT_SUCCESS);
}

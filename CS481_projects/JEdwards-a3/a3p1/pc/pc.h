/*
 * pc.h - A binary semaphore class header.
 *
 * Author: Joseph Edwards VIII
 *         CS481 online, UNM Fall 2012
 *         JEdwards-a3/a3p1/pc/pc.h
 */

#ifndef PC_H
#define PC_H
#include <pthread.h>

namespace notroot {
  class Semaphore {
  private:
    bool isWaiting;
    pthread_cond_t threadCond;
    pthread_mutex_t threadMutex;

    void lock() { pthread_mutex_lock(&threadMutex); }
    void unlock() { pthread_mutex_unlock(&threadMutex); }

  public:
    Semaphore(bool waiting);
    void wait();
    void post();
  };
}

#endif /* PC_H */

/*
 * pc.cpp - implement notroot::Semphore interface 
 * for binary semaphores in pc.h
 *
 * Author: Joseph Edwards VIII
 *         CS481 online, UNM Fall 2012
 *         JEdwards-a3/a3p1/pc/pc.cpp
 */

#include <sys/types.h>
#include <fcntl.h>
#include "pc.h"

namespace notroot {

  Semaphore::Semaphore(bool waiting) {
    /* constructor */
    isWaiting = waiting;
    pthread_cond_init(&threadCond, NULL);
    pthread_mutex_init(&threadMutex, NULL);
  }

  void Semaphore::wait() {
    /* wait method */
    lock();
    while (isWaiting)
      pthread_cond_wait(&threadCond, &threadMutex);
    isWaiting = true;
    unlock();
  }

  void Semaphore::post() {
    /* post method */
    bool was_waiting;
    lock();
    was_waiting = isWaiting;
    isWaiting = false;
    unlock();
    if (was_waiting)
      pthread_cond_signal(&threadCond);
  }

}

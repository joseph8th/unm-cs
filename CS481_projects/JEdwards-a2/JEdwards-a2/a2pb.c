#include <sys/resource.h>       /* for getrusage()       */
#include <sys/types.h>          /* for pid_t type        */
#include <unistd.h>             /* for fork(),execv(p)() */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <sys/wait.h>           /* for waitpid()         */

#define EOL 1                   /* end of line */
#define ARG 2			/* normal argument */
#define AMPERSAND 3
#define SEMICOLON 4
#define MAXARG 32 		/* max. no. command args*/
#define MAXBUF 4096		/* max. length input command */

#define FOREGROUND 0
#define BACKGROUND 1

/* define struct type for easier time-keeping maths */
typedef struct rusgt_t {
  double utime;
  double stime;
} RusgT;

/* program buffers and work pointers */
static char inpbuf[MAXBUF], tokbuf[2*MAXBUF], *ptr=inpbuf, *tok = tokbuf;
static char pthbuf[2*MAXBUF], *pth = pthbuf;

/* special characters */
static char special[]={' ', '\t', '&', ';', '\n', '\0'};
/* prompt */
char *prompt = "minsh-J-E"; 



/* check if the input character is a special one:
 * one of ', '\t', '&', ';', '\n', '\0'
 */
int inarg(char c) {
  /* used array index b/c special[] is null-term'd */
  int ix;
  for(ix = 0; ix < 6; ix++)
    {
      if(c == special[ix])
        return(0);
    }
  return(1);
}



/* print prompt and read a line 
 */
int userin(char *p) {
  /*initialization for later routines */
  char c = '\0';
  int ix = 0;
  ptr = inpbuf;
  tok = tokbuf;
  /* char *startptr = &inpbuf[0]; */
  
  /* display prompt */
  printf("\n%s: ", p);
  
  while ( (c != EOF) && (ix < MAXBUF) ) {
    c = getchar();
    if (c == EOF) 
      break;
    if (c == '\n') {
      /* *ptr='\0'; */
      inpbuf[ix] = '\0';
      return(EOL);
    }
    /* else add to input buffer */
    inpbuf[ix]=c;
    ix++;
  }
  return(EOF);
}



/* get token, place into tokbuf 
 */
int gettok(char **outptr)
{
  int type;	
  /* set the outptr string to tok */
  *outptr = tok;
  /* strip white space from the buffer containing the token */
  while ((*ptr == ' ') || (*ptr == '\t')) ptr++;
  /* set the token pointer to the first token in the buffer */	    
  *tok++ = *ptr;

  /* set the type variable depending on the token in the buffer */
  switch (*ptr++) 
    {
    case '\0':
    case '\n':
      type = EOL;
      break;
    case ';':
      type = SEMICOLON;
      break;
    case '&':
      type = AMPERSAND;
      break;
    default:
      type = ARG;
      while (inarg(*ptr))
        *tok++ = *ptr++;
    }
  *tok++ = '\0';
  return type;
}



/* post-execute routines 
 */
int postexec() {

  static int ru1st = 1;
  static RusgT rut;          /* rusage times */
  RusgT thisrut;
  struct rusage ru;          /* for rusage() */

  /* init rut the first time */
  if (ru1st) {
    ru1st = 0;
    rut.utime = 0.0;
    rut.stime = 0.0;
  }

  /* print child exec times for this run */
  if (getrusage(RUSAGE_CHILDREN, &ru) != -1) {

    /* get this run's sum of children exec times */
    thisrut.utime = ((double) ru.ru_utime.tv_sec 
                     + (double) ru.ru_utime.tv_usec / 1000000.0);
    thisrut.stime = ((double) ru.ru_stime.tv_sec 
                     + (double) ru.ru_stime.tv_usec / 1000000.0);
    
    /* subtract off last run's sum of child exec times */
    printf("\n==> CompleteRun: user time %.6f system time %.6f\n", 
           thisrut.utime - rut.utime, 
           thisrut.stime - rut.stime);

    /* set the 'last run times' for the next run */
    rut.utime = thisrut.utime;
    rut.stime = thisrut.stime;
    
    return 1;
  }
  else return 0;
}



/* Parse colon-separated paths into array
 * and return number of elts
 */
int parsepath(char **outptr, char *pathptr) 
{
  int pathc = 0;

  /* look for end of colon-sep path or too many paths */
  while ( (*pathptr != '\0') && (pathc < MAXARG) ) {
    /* set the outptr string to pathptr */
    outptr[pathc] = pth;
    *pth = *pathptr;
    /* skip over path chars */
    while ( (*pathptr != ':') && (*pathptr != '\0') )
      *pth++ = *pathptr++;
    /* null terminate the path & incr numpath */
    *pth++ = '\0';
    pathc++;
    /* if EOL or COLON then act differently */
    if (*pathptr == '\0') break;
    if (*pathptr == ':') pathptr++;
  }
  return pathc;
}



/* find a command on system PATH or return false 
 */
int findcommand(char cpath[], char *command) {

  int ix = 0, 
      numpath = 0, 
      found = 0;
  char *pathary[MAXARG];

  char wrkpth[MAXBUF];
  char *pathptr = getenv("PATH");

  /* check for getenv() errors */
  if ( ( pathptr == NULL ) || (strlen(pathptr) <= 0) ) {
    fprintf(stderr, "Error retrieving system environment PATH.\n");
    return 0;
  }
  /* parse env PATH into array and seach for command on paths */
  if ((numpath = parsepath(pathary, pathptr)) == 0)
    return 0;

  /* search each path in the array */
  while (ix < numpath) {
    sprintf(wrkpth, "%s/%s", pathary[ix], command);
    /* found or not */
    if ( access(wrkpth, X_OK) == 0 ) {
      found = 1;
      break;
    }
    else
      ix++;
  }

  /* path found, return complete */
  if (found) {
    strcpy(cpath, wrkpth);
    return 1;
  }

  return 0;
}



/* execute a command with optional wait 
 * ALL FOREGROUND version
 */
int runcommand(char **cline, int argno, int ground) {

  pid_t pid, status;         /* process info */

  int ix;

  char cpath[MAXBUF];    /* command path */
  char *command = cline[0];
  
  /* try to find the command executable on system PATH */
  if ( !(findcommand(cpath, command)) ) {
    fprintf(stderr, "Executable %s not found on system PATH.", command);
    return 0;
  }

  /* pre-exec info */
  printf("\n==> ToRun: %s : %s ", cpath, command);
  for (ix=1; ix<argno; ix++) printf(": %s ", cline[ix]);
  printf("\n");

  /* Different cases according to pid(parent or child),
   * take different actions */
  switch (pid = fork()) {
  case -1:
    fprintf(stderr, "Error forking process.\n");
    return -1;
  case 0:
    /*
     * ** execute command in child process ** 
     */
    if (execv(cpath, cline) == -1)
      fprintf(stderr, "Error executing: %s\n", *cline);
    exit(1);
  }

  /* parent wait */
  if (waitpid(pid, &status, 0) == -1) {
    fprintf(stderr, "Error waiting for PID: %d\n", pid);
    return -1;
  }
  else {
    /* post-exec info */
    if (!postexec()) {
      fprintf(stderr, "Error in post-exec function.");
      return -1;
    }
    return status;
  }
}


/* process input line
 * ALL FOREGROUND version 
 */
int procline(void)
{
  char *arg[MAXARG + 1];   /* pointer array for runcommand */
  int toktype;		  /* type of token in command     */
  int narg = 0;           /* number of arguments so far   */
  int ran;                /* error code for runcommand()  */

  for(;;) {
    /* take action according to token type */
    switch(toktype = gettok(&arg[narg])) {
    case ARG:
      if (narg<MAXARG) narg++;
      break;
    case EOL:
      if (narg != 0) {
        arg[narg] = '\0';
        ran = runcommand(arg, narg, FOREGROUND);
      }
      return ran;
    case SEMICOLON:
    case AMPERSAND:
      /* change to BACKGROUND and add routine
       * to runcommand() to add run in background
       */
      if (narg != 0) {
        arg[narg] = '\0';
        ran = runcommand(arg, narg, FOREGROUND);
        if (ran == -1) return ran;
      }
      narg = 0;
      break;
    }
  }
}



/* a2pa.c: 'minsh-J-E' mini shell main function
 * CS481online, Univ. of New Mexico, Fall 2012 
 *
 * VERSION: b(0.1) (execv, no background)
 * AUTHOR: Joseph Edwards <jee8th@unm.edu>
 */
int main()
{
  /* print prompt and read a line*/
  while ((userin(prompt)) != EOF) {
    /* process the input, execute the command and output the results */	  
    if (procline() == -1) break;
  }
  printf("\n\nTerminating mini-shell [%s].\n", prompt);
  exit(EXIT_SUCCESS);
}
  

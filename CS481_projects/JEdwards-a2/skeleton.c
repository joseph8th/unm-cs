#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/wait.h>

#define EOL 1			/* end of line */
#define ARG 2			/* normal argument */
#define AMPERSAND 3
#define SEMICOLON 4
#define MAXARG 32 		/* max. no. command args*/
#define MAXBUF 4096		/* max. length input command */

#define FOREGROUND 0
#define BACKGROUND 1

/* program buffers and work pointers */
static char inpbuf[MAXBUF], tokbuf[2*MAXBUF], *ptr=inpbuf, *tok = tokbuf;
/* special characters */
static char special[]={' ', '\t', '&', ';', '\n', '\0'};
/* prompt. 
 * change X and Y to your corresponding ones*/
char *prompt = "minsh-X-Y: "; 


/* check if the input character is a special one:
 * one of ', '\t', '&', ';', '\n', '\0'
 * feel free to call or not call it
 */
int inarg(char c)
{
	char *wrk;
	for(wrk = special; *wrk; wrk++)
	{
		if(c == *wrk)
		    return(0);
	}
	return(1);
}



/* print prompt and read a line*/
int userin(char *p)
{
  /*initialization for later routines */
  /* display prompt */
  
  while (1)
  {
    if((c=getchar()) == EOF)
      return(EOF);
  /* other if statement if you necessary    
   ... 
   ...
   ... 
   ... 
   ... 
  */   
  }
}



/* get token, place into tokbuf */
int gettok(char **outptr)
{
	int type;	
	/* in this function,
	 * set the outptr string to tok 
	 * strip white space from the buffer containing the token
	 * set the token pointer to the first token in the buffer	    
	 * set the type variable depending on the token in the buffer */

	/* a small "switch, case" statement is listed below,
	 * feel free to expand if necessary
	 */
	switch(*ptr++){
	case '\n':
	           type= EOL;
	           break;
	/* other cases and default part if necessary */
	}

	return type;
}	           



/* execute a command with optional wait */
int runcommand(char **cline, int argno)
{
	pid_t pid;
	
	switch(pid = fork()){
		/* Different cases according to pid(parent or child),
		 * take different actions
		 */
		case:
		case:
	}
	
	/* code for parent */

}



/* process input line */
int procline(void)
{
	char *arg[MAXARG +1]; /* pointer array for runcommand */
	int toktype;		  /* type of token in command */
	int narg;             /* number of arguments so far */
	
	for(;;)
	{
		/* take action according to token type */
		switch(toktype = gettok(&arg[narg])){
			/*different cases according after calling gettok*/
			case: 
			case /* in certain case */:
				runcommand(arg, narg);
				break;
		}
	}
}



int main()
{
  /* print prompt and read a line*/
  while(userin(prompt) !=EOF){
  /* process the input, execute the command and output the results */	  
    procline();
  }
}
  

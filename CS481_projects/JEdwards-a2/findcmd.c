#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <sys/types.h>

#define EOL 1                   /* end of line     */
#define ARG 2			/* normal argument */
#define COLON 5                 /* path separator  */
#define MAXARG 32
#define MAXBUF 4096

static char pthbuf[2*MAXBUF], *pth = pthbuf;


/* Parse colon-separated paths into array
 * and return number of elts
 */
int parsepath(char **outptr, char *pathptr) 
{
  int pathc = 0;

  /* look for end of colon-sep path or too many paths */
  while ( (*pathptr != '\0') && (pathc < MAXARG) ) {

    /* set the outptr string to pathptr */
    outptr[pathc] = pth;
    *pth = *pathptr;
    /* skip over path chars */
    while ( (*pathptr != ':') && (*pathptr != '\0') )
      *pth++ = *pathptr++;
    /* null terminate the path & incr numpath */
    *pth++ = '\0';
    pathc++;
    /* if EOL or COLON then act differently */
    if (*pathptr == '\0') break;
    if (*pathptr == ':') pathptr++;
  }
  return pathc;
}


/* find a command on system PATH or return false 
 */
int findcommand(char *command) {

  int ix, numpath, found = 0;
  char *pathary[MAXARG + 1];
  char *pathptr = getenv("PATH");
  char wrkpth[MAXBUF];

  /* check for getenv() errors */
  if ( ( pathptr == NULL ) || (strlen(pathptr) <= 0) ) {
    fprintf(stderr, "Error retrieving system environment PATH.\n");
    return -1;
  }
  /* parse env PATH into array and seach for command on paths */
  if ((numpath = parsepath(pathary, pathptr)) != 0) {

    /* search each path in the array */
    while (ix < numpath) {
      sprintf(wrkpth, "%s/%s", pathary[ix], command);
      /* found or not */
      if ( access(wrkpth, X_OK) == 0 ) {
        found = 1;
        break;
      }
      else
        ix++;
    }

    if (!found)
      fprintf(stderr, "Command %s not found on system path.\n", command);
    else
      printf("Command %s found on: %s\n", command, pathary[ix]);
  }
  return found;
}



int main(int argc, char *argv[]) {

  if (argc == 2)
    findcommand(argv[1]);

  return 0;
}

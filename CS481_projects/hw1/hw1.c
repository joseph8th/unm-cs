#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{

  int pid1,pid2,pid3, status;

  pid1 = fork();
  if (!pid1) {
    printf("PID=%i, PPID=%i, Child 1 PID=%i\n", getpid(), getppid(), pid1);
    execl("/bin/sleep", "sleep", "45", NULL);
  }
  wait(&status);

  pid2 = fork();
  if (!pid2) {
    printf("PID=%i, PPID=%i, Child 1 PID=%i\n", getpid(), getppid(), pid2);
    execl("/bin/sleep", "sleep", "45", NULL);
  }
  wait(&status);

  pid3 = fork();
  if (!pid3) {
    printf("PID=%i, PPID=%i, Child 1 PID=%i\n", getpid(), getppid(), pid3);
    execl("/bin/sleep", "sleep", "45", NULL);
  }
  wait(&status);

  printf("PID=%i, PPID=%i, Child 1 PID=%i\n", getpid(), getppid(), pid1);


  return 0;
}

#include <windows.h>    /* for win32 API */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>     /* for strlen() */

/* Pretty exit() on error function. */
static int quitnice(char *msg) {
    fprintf(stderr, "\n%s\n", msg);
    exit(EXIT_FAILURE);
}

int execcmd(char *cmdline) {

    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    
    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));

    if(!CreateProcess(NULL, cmdline, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi))
        return GetLastError();
  
    /* Wait for child process to exit. */
    WaitForSingleObject(pi.hProcess, INFINITE);
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
    return 0;
}

int main(int argc, char *argv[]) {
    
    int arglen, errs;
    long int val;
    char *eptr, *arg, *cmdline, errmsg[64];
    char *command = "FiboChild.exe";
    
    /* Need one and only one argument. */
    if (argc != 2) {
        ++errs;
        sprintf(errmsg, "Usage: %s [number > 0].", argv[0]);
        quitnice(errmsg);
    }
    else {
        arg = argv[1];
        arglen = strlen(arg);
        val = strtol(arg, &eptr, 10);
        
        /* Use strol() to check for valid number. */
        if ((arg == eptr) || (eptr-arg != arglen)) {
            sprintf(errmsg, "Error: %s is not a number.", arg);
            quitnice(errmsg);
        }
        /* Must be positive and at least one. */
        if (val < 1) {
            sprintf(errmsg, "Error: %d is a negative number.", val);
            quitnice(errmsg);
        }
        
        /* Argument checks out. Try to execute [command arg]. */
        printf("\nCommand line: %s %s\n", command, arg);
        sprintf(cmdline, "%s %s", command, arg);
        
        /* Try to catch execution failure */
        if (execcmd(cmdline)) {
           sprintf(errmsg, "Error: \'%s %s\' failed (%d).", command, arg, 1);
           quitnice(errmsg);       
        }
    }
    exit(EXIT_SUCCESS);
}

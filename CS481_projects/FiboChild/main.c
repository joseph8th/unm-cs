#include <stdio.h>
#include <stdlib.h>
#include <string.h>     /* for strtol() */
#include <limits.h>     /* for ULLONG_MAX */

/* Fibonacci recursive function. */
static void fibofun(unsigned long long int lastnum, 
                    unsigned long long int thisnum, 
                    long int numnums) 
{
    unsigned long long int sum = lastnum + thisnum;
    if ((sum < ULLONG_MAX) && (sum > 0)) {            
        if (numnums > 0) {
           printf("%llu ", thisnum);
           lastnum = thisnum;
           thisnum = sum;
           fibofun(lastnum, thisnum, numnums-1);
        }
    }
    else return;
}

/* Print out fibonacci series. */
int main(int argc, char *argv[]) {
    
    char *eptr;
    long int numnums = strtol(argv[1], &eptr, 10);

    printf("\nULLONG_MAX=%llu\n", ULLONG_MAX);
    printf("\nFirst %d numbers of Fibonacci series:\n", numnums);
    fibofun(0, 1, numnums);
    printf("\n\n");
    
    exit(EXIT_SUCCESS);
}

#include <stdio.h>
#include <sys/times.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
 
int main(int argc, char * argv[]) {

  static long clock_ticks = 0;

  long i, j, k, T1, T2;
  double a = 0.0, b = 0.0, c = 0.0;
  struct tms start_time, stop_time;

  if (clock_ticks == 0) {
    clock_ticks = sysconf(_SC_CLK_TCK);
  }

  if (argc == 2) 
    k = atoi(argv[1]);
  else
    k = 1000;

  printf("\nTimeTester: Looping %ld times...\n", k);

  T1 = times(&start_time);
   
  for(i=0;i<k;i++){
    for(j=0;j<k;j++){
      a += 1.0/c;
      b = pow(0.22,2.5);
      b = pow(0.22,2.5);
      c += b;
    }
  }
             
  T2 = times(&stop_time);
   
  printf ("Execute Time for Loop: \n");
  printf("%.3f seconds\n", 
         (double)(stop_time.tms_utime - start_time.tms_utime)/clock_ticks );
 
  return 0;       
}
